/**
 * Tasks file. 
 */
var express = require('express');
var router = express.Router();
var dbconn = require('../assets/scripts/dbconn');
var funcs = require("../assets/scripts/util");
var ObjectId = require("mongodb").ObjectID;
var gameLists = require("../assets/scripts/game-lists");

var deleteAnagram = function(res, arr, i) 
{
    dbconn.delete("anagrams", {
        title: arr[i].title,
        result: arr[i].result
    }, del => {
        if ( i+1 < arr.length )
            deleteAnagram(res, arr, i+1);
        else
        {
            res.status(200).json({deleted: true});
        };
    });   
}

// Lists all anagrams.
router.post("/list-anagrams", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var arr = [];
        var i = 0;
        dbconn.query("anagrams", {}, (anagrams, len) => {
            var response = {
                anagrams: arr
            };
            for ( var i=0; i<len; i++ )
            {
                arr[i] = {
                    position: i+1,
                    title: anagrams[i].title,
                    result: anagrams[i].result
                };
            };
            res.status(200).json(response);
        });
    };
});

// Add new anagram.
router.post("/add-new-anagram", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            exists: true
        };  
        dbconn.query("anagrams", {
            title: data.title,
            result: data.result
        }, (anagrams, len) => {
            // Do insert operation.
            if ( len == 0 )
            {
                response.exists = false;
                dbconn.insert("anagrams", {
                    title: data.title,
                    result: data.result
                }, (ins) => {                    
                    res.status(200).json(response);
                });
            } else
                res.status(200).json(response);
        });             
    };
});

// Removes anagrams.
router.post("/remove-anagrams", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            deleted: false
        };     
        var anagrams = data.anagrams;
        deleteAnagram(res, anagrams, 0);        
    };
});

// Get all 5x5 games.
router.post("/get-5x5-games", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            found: false,
            games: []
        };     
        
        dbconn.query("5x5", {}, (games, len) => {
            if ( len == 0 )
                response.games = null;
            else
            {
                response.found = true;
                for (var i=0; i<len; i++)
                {
                    response.games[i] = games[i];

                };
            };
            res.status(200).json(response);
        });
    };
});

// Get all 5x5 games.
router.post("/remove-5x5-games", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {};     
        var ids = [];

        
        for ( var i=0; i<data.games.length; i++ )
            ids[i] = ObjectId( data.games[i] );

        dbconn.delete("5x5", {
            "_id": {
                $in: ids
            }
        }, del => {
            res.status(200).json(response);
        });
    };
});

// Creates new 5x5 game.
router.post("/add-5x5-game", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            added: false
        };     

        dbconn.insert("5x5", data.game, (ins) => {
            response.added = ins.insertedCount > 0;
            res.status(200).json(response);
        });
    };
});

// Gets trophies.
router.post("/get-trophies", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {};     

        dbconn.query("trophies", {}, (trophies, num) => {
            response.gameCount = num;
            response.games = trophies;            
            res.status(200).json(response);
        });
    };
});

// Gets trophies.
router.post("/delete-trophie-game", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {};     

        dbconn.delete("trophies", { _id: ObjectId(data.id) }, (del) => {
            res.status(200).json(response);
        });       
    };
});

// Checks if given trophie exists within the database.
router.post("/check-if-trophie-exists", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {}; 
        
        dbconn.query("trophies", {
            name: data.name
        }, (trophies, num) => {
            response.found = num > 0;
            res.status(200).json(response);
        });       
    };
});

// Add new trophie.
router.post("/add-new-trophie-game", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {}; 

        dbconn.insert("trophies", data, (ins) => {
            response.added = ins.insertedCount > 0;
            res.status(200).json(response);
        });
    };
});

// Checks nat-geo words.
router.post("/check-nat-geo-words", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {}; 

        dbconn.query("words", {}, (words, num) => {
            response.count = num;
            response.words = words;
            res.status(200).json(response);
        })
    };
});

// Checks nat-geo words.
router.post("/delete-nat-geo-words", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {}; 
        var arr = data.by;

        for (var i=0; i<arr.length; i++)
            arr[i] = ObjectId(arr[i]);

        dbconn.delete("words", {
            _id: {
                $in: arr
            }
        }, (del) => {
            response.deleted = del.deletedCount > 0;
            res.status(200).json(response);
        });
    };
});

// Adds new nat-geo word.
router.post("/add-new-term", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {}; 
        var doc = {
            word: {        
                $regex: new RegExp("^" + data.term.toLowerCase(), "i")            
            },
            category: data.category,
            letter: data.letter.toUpperCase()
        };

        dbconn.query("words", doc, (words, len) => {
            response.exists = len > 0;
            response.added = false;
            doc.word = data.term;
            if ( !response.exists )
            {
                dbconn.insert("words", doc, ins => {
                    response.added = ins.insertedCount > 0;
                    res.status(200).json(response);
                });
            } else        
                res.status(200).json(response);            
        });
    };
});

router.post("/can-open-supervise-section", (req, res) => {
    if ( !funcs.isUserSupervisor(req) )
        res.status(401).end();
    else
    {
        var response = {
            singleplayer: {
                hasActiveQuiz: false
            },
            multiplayer: {
                hasActiveQuiz: false
            }
        };

        // Go through the all singleplayer games.
        gameLists.singleplayerGames.forEach((el) => {
            if ( el.supervisor == req.session.username )
            {
                response.singleplayer.hasActiveQuiz = true;
                return false;
            };
        });

        res.status(200).json(response);
    };
});

module.exports = router;