/**
 * User route paths.
 */
/**
 * Tasks file. 
 */
var express = require('express');
var router = express.Router();
var dbconn = require('../assets/scripts/dbconn');
var server = require("../server");
var multer = require('multer');
var path = require("path");
var sha256 = require('js-sha256').sha256;
var gameLists = require("../assets/scripts/game-lists");
var socketLists = require("../assets/scripts/socket-list");
var util = require("../assets/scripts/util");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, server.production ? './dist/application/assets/upload' : './src/assets/upload')
    },
    filename: function (req, file, cb) {
        var ext = path.extname(file.originalname);
        var filename = file.originalname.replace(ext,"");
        cb(null, filename + "-" + Date.now() + ext);
    }
})
const upload = multer({
    storage: storage
});
var waitingMultiplayerList = gameLists.waitingMultiplayerGames;

// Check if user exists in the database.
router.post('/exists-with-credentials', function(req, res, next) {
    var data = req.body;
    if ( !data.username || !data.password )
    {
        res.status(400);
        res.json({'error': "Username or password not provided."});
        return;
    };
    
    dbconn.query('users', { username: data.username, password: data.password }, function(users, num) {
        var resp = {exists: num > 0, isValid: num>0 ? users[0].isValid : false, sess: req.session.user ? req.session.user : null};
        // Log in user.
        if ( num > 0 )
        {
            var singleplayerGames = gameLists.singleplayerGames;
            singleplayerGames.deleteAllHosts(data.username);

            if ( !req.session.user )
            {
                req.session.username = data.username;
                req.session.accountType = users[0].accType;
                resp.sess = req.session.username;                
            };
        };
        //console.log(req.session);
        //console.log("....................");
        res.json(resp);
    });
});

// Check if user exists in the database.
router.post('/exists', function(req, res, next) {
    var data = req.body;
    if ( !data.username )
    {
        res.status(400);
        res.json({'error': "Username not provided."});
        return;
    };
    dbconn.query('users', { username: data.username }, function(user, num) {
        setTimeout(function() {
            res.json({exists: num > 0});
        }, 200);        
    });
});

// Uploads image.
router.post('/upload-image', upload.single('file'), function(req, res) {
    const filename = req.file.filename;
    const path = req.file.path;
    res.json({uploaded: true, path: filename});
});

// Registers user.
router.post('/register-user', upload.single('file'), function(req, res) {
   var obj = req.body;
   dbconn.insert('users', req.body, function(resp) {
    res.json({registered: true});
   });
});

// Change password for user.
router.post('/change-password', function(req, res) {
    var data = req.body;
    var resp = {
        changed: false,
        userExists: false,
        cred: false
    };
    dbconn.query('users', { username: data.username }, function(users, num) {
        resp.userExists = num > 0;
        if ( resp.userExists )
        {
            dbconn.query('users', { username: data.username, password: data.oldpwd }, function(myuser, num) {                
                resp.cred = num > 0;
                if ( resp.cred )
                {
                    var user = myuser[0];
                    var change = {
                        $set: { password: data.newpwd }
                    };
                    dbconn.update('users', { username: data.username }, change, function(err, updatedNum, status) {
                        resp.changed = updatedNum.matchedCount > 0;
                        res.json(resp);
                    });
                } else
                    res.json(resp);
            });
        } else
            res.json(resp);
    });
 });

// Recover account part 1 - Asks if user exists.
router.post('/recover-account-1', function(req, res) {
    var obj = req.body;
    var resp = { exists: false };
    dbconn.query('users', { username: obj.username, jmbg: obj.jmbg }, function(users, num) {
        if ( num > 0 )
        {
            resp.exists = true;
            var myself = users[0];            
            resp.question = myself.secq;
        };
        res.json(resp);
    });
 });

 // Recover account part 2 - Checks security Q&A.
router.post('/recover-account-2', function(req, res) {
    var obj = req.body;
    var resp = { exists: false };
    dbconn.query('users', { username: obj.username }, function(users, num) {
        var resp = {
            confirmed: false
        };
        var user = users[0];
        var shaANS = sha256(user.seca);
        if ( shaANS == obj.answer )
            resp.confirmed = true;
        res.json(resp);
    });
 });

// Recover account part 3 - Change password for the user.
router.post('/recover-account-3', function(req, res) {
    var obj = req.body;
    try {
        dbconn.query('users', { username: obj.username }, function(users, num) {
            var resp = {
                changed: false
            };
            var changing = {
                $set: { password: obj.password }
            };
            var user = users[0];
            dbconn.update('users', { username: user.username }, changing, function(err, retobj, status) {
                resp.changed = retobj.matchedCount > 0;
                res.json(resp);    
            });            
        });
    } catch(e) {
        res.status(400).json(e);
    }
 });

 // Checks if user's session is active.
 router.post('/is-active-session', function(req, res) {
     var data = req.body;
     var resp = {
         isLoggedIn: false,
         user: null,
         addit: {},
         expiry: null
     };
     //console.log(req.session);
     resp.isLoggedIn = req.session ? req.session.username != undefined : false;
     resp.expiry = resp.isLoggedIn ? req.session.cookie.expires : null;

     if ( resp.isLoggedIn )
     {
        dbconn.query('users', { username: req.session.username }, function(users, num) {
            resp.user = users[0];
            delete resp.user.jmbg;
            delete resp.user.seca;
            delete resp.user.secq;
            delete resp.user.isValid;
            delete resp.user.password;            

            // Check if supervisor is in the game.
            if ( resp.user.accType == 1 )
            {
                var list = gameLists.singleplayerGames;
                resp.addit.inSupervision = false;
                resp.addit.isMultiplayer = false;
                list.forEach((el) => {
                    if ( el.supervisor != null && el.supervisor == req.session.username)
                    {
                        console.log("hereeeeeee");
                        resp.addit.inSupervision = true;                        
                        return false;
                    };
                    return true;
                });
            };
            res.status(200).json(resp);
        });
     } else
        res.status(200).json(resp);
 });

// Logs out.
router.post('/logout', function(req, res) {

    // Delete waiting game.
    var username = req.session.username;
    var list = gameLists.singleplayerGames;
    list.deleteAllHosts(username);

    // Delete user from socket list.
    socketLists.list.delete(username);

    // Notify all supervisors.
    if ( req.session.accountType == 0 )
    {
        var elem;        
        for ( list.i_start(); list.i_isNotNull(); list.i_next() )
        {
            elem = list.i_get();
            // Notify!
            if ( elem.accountType == 1 )
            {
                try {
                    elem.socket.emit("ctrl-sp-user-declined", username);
                } catch(e) {}
            }
        };
    }

    req.session.destroy();
    res.status(200).end();
});

// Checks for available multiplayer matches..
router.post('/get-avail-multiplayers', function(req, res) {

    // Check unauthorized access.
    if ( !(req.session && req.session.username != null) )
    {
        res.status(401).end();
        return;
    };

    var resp = {
        users: null,
        num: 0
    };
    
    var allUsers = [];
    var allUsersData = [];
    var i = 0;
    var wo;
    for ( waitingMultiplayerList.i_start(); waitingMultiplayerList.i_isNotNull(); waitingMultiplayerList.i_next() )
    {
        wo = waitingMultiplayerList.i_get();
        if ( wo.UserBlue == req.session.username )
            continue;
        allUsers[i] = wo.UserBlue;
        allUsersData[i] = wo;
        i++;
    };

    dbconn.query( "users", { username: {
        $in: allUsers
    } }, function( users, numUsers ) {
        resp.users = [];
        resp.num = numUsers;
        for (var i=0; i<numUsers; i++)
        {
            var started = null;
            for ( var j=0; j<allUsersData.length; j++ )
            {
                if ( allUsersData[j].UserBlue == users[i].username )
                    started = allUsersData[j].BlueStarted
            };
            resp.users[i] = {
                name: users[i].name,
                surname: users[i].surname,
                image: "assets/upload/" + users[i].image,
                fromBeginTime: started,
                username: users[i].username
            };
        };
        res.json(resp);
    });
});

// Checks for available multiplayer matches..
router.post('/active-games-check', function(req, res) {

    // Check unauthorized access.
    if ( !(req.session && req.session.username != null) )
    {
        res.status(401).end();
        return;
    };

    var resp = {
        multiplayer: {
            hasActiveWaitingGame: false,
            isActivelyWaiting: false
        },
        singleplayer: {
            hasActiveWaitingGame: false,
            activeGame: false, 
            data: null
        }, 
        gameOfTheDay: {
            found: false,
            data: null,
            played: false, 
            results: null
        }
    };
    
    var allUsers = [];
    var allUsersData = [];
    var i = 0;
    var found = false;
    var found1 = false;

    // Check multiplayer games.
    for ( waitingMultiplayerList.i_start(); waitingMultiplayerList.i_isNotNull(); waitingMultiplayerList.i_next() )
    {
        var wo = waitingMultiplayerList.i_get();
        if ( wo.UserBlue == req.session.username )
        {
            found = true;
        };

        if ( wo.UserRed == req.session.username )
        {
            found1 = true;
        };
    };

    resp.multiplayer.hasActiveWaitingGame = found;
    resp.multiplayer.isActivelyWaiting = found1;

    // Check singleplayer games.
    var lst = gameLists.singleplayerGames;

    resp.singleplayer.hasActiveWaitingGame = lst.isWaitingHost(req.session.username);
    resp.singleplayer.activeGame = lst.isActiveHost(req.session.username);
    lst.forEach( (el) => {
        if ( el.username == req.session.username )
        {   
            resp.singleplayer.data = {
                supervisor: el.supervisor,
                currentGame: el.currentGame,
                game: el.currentGame > -1 ? el.games[el.currentGame] : null
            };                            
            return false;
        }
    } );

    var t = new Date();
    var p = new Promise((resolve) => {
        dbconn.query("gameOfTheDay", {
            date: {
                $eq: new Date(t.getFullYear(), t.getMonth(), t.getDate(), 0, 0, 0, 0)
            }
        }, (games, len) => {
            resp.gameOfTheDay.found = len == 1;
            if ( len == 1 )
            {            
                try {
                    delete games[0].date;
                    resp.gameOfTheDay.data = games[0];
                } catch(e) {
                    resp.gameOfTheDay.data = null;
                }
            };
            resolve();
        });
    });

    var f = new Promise((resolve) => {
        dbconn.query("games", {
            date: {
                $gte: new Date(t.getFullYear(), t.getMonth(), t.getDate(), 0, 0, 0, 0)
            },
            userID: req.session.username
        }, (games, len) => {
            var game = games[0];
            resp.gameOfTheDay.played = len > 0;
            if ( len > 0 )
            {
                var game = games[0]; 
                resp.gameOfTheDay.results = {
                    anagram: game.points_anagram,
                    number: game.points_number,
                    fxf: game.points_fxf,
                    natgeo: game.points_natgeo,
                    trophie: game.points_trophie
                };
            };
            resolve();
        });
    });

    Promise.all([p,f]).then(values => {
        res.status(200).json(resp);
    });

    /**
     * dbconn.query("users", {username: el.supervisor}, (users, num) => {
                    var sup = users[0];
                    resp.singleplayer.data.supervisor = {
                        name: sup.name,
                        surname: sup.surname,
                        image: sup.image,
                        username: sup.username                        
                    };                   
                });
     */
});

// Gets rang list.
router.post("/get-rang-list", (req, res) => {
    if ( !util.isUser(req) )
    {
        res.status(400).end();
    } else {
        var t = new Date();
        var de = new Date();
        var ds = new Date();
        ds.setDate( de.getDate() - 20 );
        var games = dbconn.getCollection("games");
        var doc = games.aggregate([
            {
                $match: {
                    date: { $gte: ds, $lte: de }
                }
            },
            { 
                $project: { 
                    total: { 
                        $sum: [ "$points_anagram", "$points_number", "$points_fxf", "$points_natgeo", "$points_trophie" ] 
                    },
                    username: "$userID",
                    date: "$date"
                } 
            },
            {
                $group: {
                    _id: { month: { $month: "$date" }, day: { $dayOfMonth: "$date" }, year: { $year: "$date" }, total: { $max: "$total" }, username: "$username" },
                    total: { $max: "$total" },
                    username: { $first: "$username" },
                    date: { $first: "$date" }
                }
            },
            {
                $sort: {
                    "_id.year": -1, 
                    "_id.month": -1, 
                    "_id.day": -1, 
                    total: -1
                }
            },
            {
                $group: {
                    _id: { year: "$_id.year", month: "$_id.month", day: "$_id.day" },
                    total: { $max: "$_id.total" },
                    username: { $first: "$_id.username" },
                    date: { $first: "$date" }
                }
            },
            {
                $sort: {
                    "_id.year": -1, 
                    "_id.month": -1, 
                    "_id.day": -1, 
                    total: -1
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "username",
                    foreignField: "username",
                    as: "userData"
                }
            }
        ]).toArray();

        doc.then((values) => {
            var ranglist = [];
            for ( var i=0; i<values.length; i++ )
            {
                ranglist[i] = {
                    rang: i+1,
                    username: values[i].userData[0].username,
                    fullname: values[i].userData[0].name + " " + values[i].userData[0].surname,
                    image: values[i].userData[0].image,
                    points: values[i].total,
                    date: util.formatDate( new Date( values[i].date ) )
                };
            };

            res.status(200).json({
                ranglist: ranglist,
                length: ranglist.length
            });
        });       
    };
});

// Checks if user can sign-out from current match.
router.post("/can-sign-out", (req, res) => {
    if ( !req.session || !req.session.username )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            multiplayer: {
                canSignOut: false,
                empty: false
            }, 
            singleplayer: {
                canSignOut: false,
                empty: false
            }           
        };

        if ( waitingMultiplayerList.size() == 0 )
        {
            response.multiplayer.canSignOut = true;
            response.multiplayer.empty = true;
        } else
        {
            // Go thourgh the waiting game list. If found => can sign out!
            var foundRed = false;
            var wo;            
            for ( waitingMultiplayerList.i_start(); waitingMultiplayerList.i_isNotNull(); waitingMultiplayerList.i_next() )
            {
                wo = waitingMultiplayerList.i_get();
                if ( wo.UserRed == req.session.username )
                    foundRed = true;
                if ( wo.UserBlue == req.session.username || wo.UserRed == req.session.username )
                {
                    response.canSignOut = true;
                };
            };
            if ( !foundRed )
                response.canSignOut = true;
        };

        // Check singleplayer games.
        gameLists.singleplayerGames.forEach((el) => {
            if ( el.username == req.session.username )
            {
                response.singleplayer.empty = el.started;
                response.singleplayer.canSignOut = el.started;
                return false;
            }
        });

        res.status(200).json(response);        
    };
});

// Adds new multiplayer match.
router.post("/set-multiplayer-match", (req, res) => {
    if ( !req.session || !req.session.username )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            created: false
        };
        dbconn.insert("waiting", {
            UserBlue: data.blueUser,
            UserRed: "",
            Supervisor: "",
            BlueStarted: new Date().toISOString()
        }, function(task) {
            response.created = task.insertedCount > 0;
            res.status(200).json(response);
        })
    };
});

// Searches for red contestant.
router.post('/search-red', (req, res) => {
    if ( !req.session || !req.session.username )
        res.status(401).end();
    else
    {
        var f = false;
        var output = {
            found: false,
            user: {
                name: null,
                surname: null,
                username: null
            }
        };
        var inProcess = false;        
        var int = setInterval(() => {
            if ( inProcess )
                return;
            inProcess = true; 
            dbconn.query("waiting", 
            {                                     
                UserRed: {
                    $ne: ""
                },
                UserBlue: {
                    $eq: req.session.username
                }             
            }, (rows, num) => {                               
                // User is found. Send response to user.
                if ( num > 0 )
                {
                    var row = rows[0];
                    var red = row.UserRed;
                    output.found = true;

                    dbconn.query("users", { username: {
                        $eq: red
                    } }, function(users, len) {
                        if ( len > 0 )
                        {
                            var redDetailed = users[0];
                            output.user.username = redDetailed.username;
                            output.user.name = redDetailed.name;
                            output.user.surname = redDetailed.surname;
                        };                       
                        clearInterval(int);
                        res.status(200).json(output);     
                    });
                } else
                {
                    inProcess = false;
                };
            });
        }, 1000);
    };
});

// Searches for red contestant.
router.post('/search-supervisor', (req, res) => {
    if ( !req.session || !req.session.username )
        res.status(401).end();
    else
    {
        var f = false;
        var output = {
            found: false,
            supervisor: {
                name: null,
                surname: null, 
                username: null
            }
        };
        var inProcess = false;        
        var int = setInterval(() => {
            if ( inProcess )
                return;
            inProcess = true; 
            dbconn.query("waiting", 
            {                                     
                Supervisor: {
                    $ne: ""
                },
                UserBlue: {
                    $eq: req.session.username
                }             
            }, (rows, num) => {                               
                // User is found. Send response to user.
                if ( num > 0 )
                {
                    var row = rows[0];
                    var supervisor = row.Supervisor;
                    output.found = true;

                    dbconn.query("users", { username: {
                        $eq: supervisor
                    } }, function(users, len) {
                        if ( len > 0 )
                        {
                            var supDetailed = users[0];
                            output.supervisor.username = supDetailed.username;
                            output.supervisor.name = supDetailed.name;
                            output.supervisor.surname = supDetailed.surname;
                        };
                        clearInterval(int);
                        res.status(200).json(output);                        
                    });
                } else
                {
                    inProcess = false;
                };
            });
        }, 5000);
    };
});

// Join match.
router.post("/join-match", function(req, res) {
    var data = req.body;
    var output = {
        joined: false
    };
    dbconn.update("waiting", {
        UserBlue: data.username
    }, {
        $set: { UserRed: req.session.username }
    }, (error, updatedNum, status) => {
        output.joined = updatedNum.result.updatedNum > 0;
        res.status(200).json(output);
    });
});

// Checks if user has active quiz.
router.post("/has-active-quiz", function(req, res) {
    if ( !req.session || !req.session.username )
        res.status(401).end();
    else
    {
        var resp = {
            hasquiz: false,
            inGame: -1
        };
        var wo;
        for ( waitingMultiplayerList.i_start(); waitingMultiplayerList.i_isNotNull(); waitingMultiplayerList.i_next() )
        {
            wo = waitingMultiplayerList.i_get();
            if ( wo.UserBlue == req.session.username || wo.UserRed == req.session.username )
            {
                resp.hasquiz = true;
                resp.inGame = 1;
                break;
            };
        };

        // Add check in active matches.
        // ...

        // Check singleplayer matches
        gameLists.singleplayerGames.forEach((el) => {
            if ( el.username == req.session.username && el.started )
            {
                resp.hasquiz = true;  
                resp.inGame = 0;              
                return false;
            };
        });

        res.status(200).json(resp);
    };
});

// Starts quiz. Adding data into database.
router.post("/start-quiz", function(req, res) {
    if ( !req.session || !req.session.username )
        res.status(401).end();
    else
    {
        var resp = {
            ok: false
        };
        var data = req.body;
        var match = {
            pointsBlue: 0,
            pointsRed: 0,
            score: -1,
            date: new Date().toISOString(),
            userBlue: req.session.username,
            userRed: data.redID,
            supervisor: data.supID
        };

        // Delete waiting record.
        waitingList.deleteAllHosts(req.session.username);

        // Insert into database new match.
        dbconn.insert("matches", match, (ins) => {
            resp.ok = ins.insertedCount === 1;
            res.status(200).json(resp);
        });
    };
});

module.exports = router;