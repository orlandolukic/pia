/**
 * Handles ordinary requests.
 */
var express = require('express');
var util = require("../assets/scripts/util");
var dbconn = require("../assets/scripts/dbconn");
var router = express.Router();

router.post("/get-results", (req, res) => {
    var endDate = new Date();
    var startDate = new Date();
    startDate.setDate( endDate.getDate() - 20 );
    var games = dbconn.getCollection("games");
    var doc = games.aggregate([
        { 
            $project: { 
                total: { 
                    $sum: [ "$points_anagram", "$points_number", "$points_fxf", "$points_natgeo", "$points_trophie" ] 
                },
                username: "$userID",
                date: "$date"
            } 
        },
        {
            $group: {
                _id: { month: { $month: "$date" }, day: { $dayOfMonth: "$date" }, year: { $year: "$date" }, total: { $max: "$total" }, username: "$username" },
                total: { $max: "$total" },
                username: { $first: "$username" },
                date: { $first: "$date" }
            }
        },
        {
            $sort: {
                "_id.year": -1, 
                "_id.month": -1, 
                "_id.day": -1, 
                total: -1
            }
        },
        {
            $group: {
                _id: { year: "$_id.year", month: "$_id.month", day: "$_id.day" },
                total: { $max: "$_id.total" },
                username: { $first: "$_id.username" },
                date: { $first: "$date" }
            }
        },
        {
            $sort: {
                "_id.year": -1, 
                "_id.month": -1, 
                "_id.day": -1, 
                total: -1
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "username",
                foreignField: "username",
                as: "userData"
            }
        }
    ]).toArray();

    var ds = new Date();
    ds.setDate(1);
    var de = new Date();
    var m = games.aggregate([
        {
            $match: {
                date: { $gte: ds, $lte: de }
            }
        },
        { 
            $project: { 
                total: { 
                    $sum: [ "$points_anagram", "$points_number", "$points_fxf", "$points_natgeo", "$points_trophie" ] 
                },
                username: "$userID",
                date: "$date"
            } 
        },
        {
            $group: {
                _id: { username: "$username", total: "$total" },
                total: { $max: "$total" },
                username: { $first: "$username" },
                date: { $first: "$date" }
            }
        },
        {
            $sort: {
                total: -1
            }
        },
        {
            $group: {
                _id: { username: "$_id.username" },
                total: { $max: "$total" },
                date: { $first: "$date" },
                username: { $first: "$username" }
            }
        },
        {
            $sort: {
                total: -1
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "username",
                foreignField: "username",
                as: "userData"
            }
        }
    ]).toArray();

    Promise.all([doc, m]).then((values) => {

        var arr1 = values[0];
        for (var i=0; i<arr1.length; i++)
        {
            delete arr1[i].userData[0].password;
            delete arr1[i].userData[0].secq;
            delete arr1[i].userData[0].seca;
            arr1[i].rang = i+1;
        };

        arr2 = values[1];
        for (var i=0; i<arr2.length; i++)
        {
            delete arr2[i].userData[0].password;
            delete arr2[i].userData[0].secq;
            delete arr2[i].userData[0].seca;
            arr2[i].rang = i+1;
        };

        res.json({
            last20: arr1,
            month: arr2
        });
    });
});

module.exports = router;