/**
 * Administrator handling requests.
 */
var express = require('express');
var router = express.Router();
var dbconn = require('../assets/scripts/dbconn');
var funcs = require("../assets/scripts/util");
var ObjectId = require("mongodb").ObjectID;
//var ISODate = require("mongodb").ISODate;

// Fetches all pending registration requests.
router.post("/fetch-pending-requests", (req, res) => {
    if ( !funcs.isAdministrator(req) )
        res.status(401).end();
    else
    {
        var response = {};
        dbconn.query("users", {
            accType: 0,
            isValid: false
        }, (users, len) => {
            response.count = len;
            var user;
            for ( var i=0; i<len; i++ )
            {
                user = users[i];
                delete user.password;
                delete user.secq;
                delete user.seca;
                delete user.accType;
                delete user.isValid;                
            };
            response.requests = users;
            res.status(200).json(response);
        }); 
    };
});

// Processes selected pending request.
router.post("/process-pending-request", (req, res) => {
    if ( !funcs.isAdministrator(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {};
        
        // Accept user
        if ( data.type == 1 )
        {
            dbconn.update("users", { _id: ObjectId(data.id) }, {
                $set: {
                    isValid: true
                }
            }, (err, updatedNum, status) => {
                response.performedAction = updatedNum.matchedCount > 0;
                res.status(200).json(response);
            });
        } else if ( data.type == 0 )    // Delete user.
        {
            dbconn.delete("users", { _id: ObjectId(data.id) }, del => {
                response.performedAction = del.deletedCount > 0;
                res.status(200).json(response);
            });
        } else
        {
            response.performedAction = false;
            res.status(200).json(response);
        };
    };
});

// Lists all anagrams & trophies.
router.post("/fetch-all-games", (req, res) => {
    if ( !funcs.isAdministrator(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            game5x5: {
                found: false,
                games: []
            },
            gameTrophie: {
                found: false,
                games: []
            },
            gameAnagram: {
                found: false,
                games: []
            },
            gameOfTheDay: {
                found: false,
                games: []
            }                      
        };     
        
        dbconn.query("5x5", {}, (games, len) => {
            response.game5x5.found = len > 0;                        
            for (var i=0; i<len; i++)
                response.game5x5.games[i] = games[i];           

            dbconn.query("trophies", {}, (trophies, num) => {
                response.gameTrophie.found = num > 0;
                response.gameTrophie.games = trophies;            

                dbconn.query("anagrams", {}, (anagrams, num) => {
                    response.gameAnagram.found = num > 0;
                    response.gameAnagram.games = anagrams;   

                    var today = new Date();                    
                    dbconn.query("gameOfTheDay", {
                        date: { 
                            "$gte": new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0)
                        }
                    }, (g, num) => {                        
                        response.gameOfTheDay.found = num > 0;
                        response.gameOfTheDay.games = g;                                                 
                        
                        res.status(200).json(response);
                    });                             
                });                  
            });            
        });
    };
});

// Manages game of the day by admin.
router.post("/manage-game-of-the-day", (req, res) => {
    if ( !funcs.isAdministrator(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            inserted: false,
            changed: false,
            inUse: false
        };
        
        // Add new game of the day.
        if ( data.action == "add" )
        {
            var doc = {
                anagramID: data.anagram,
                fxfID: data.fxf,
                trophieID: data.trophie,
                date: new Date(data.date)
            };
            dbconn.insert("gameOfTheDay", doc, (ins) => {
                response.inserted = ins.insertedCount > 0;       
                res.status(200).json(response);         
            });
        } else  // change action.
        {
            // Do security check before.
            dbconn.query("games", {
                godID: ObjectId(data.id)
            }, (games, len) => {
                response.inUse = len > 0;
                if ( len == 0 )
                {
                    var doc = {
                        $set: {
                            anagramID: data.anagram,
                            fxfID: data.fxf,
                            trophieID: data.trophie
                        }
                    };
                    dbconn.update("gameOfTheDay", {
                        _id: ObjectId(data.id)                
                    }, doc, (err, upd, status) => {
                        response.changed = upd.updatedCount > 0;
                        res.status(200).json(response);          
                    }); 
                } else
                    res.status(200).json(response);
            });                  
        }
    };
});

// Fetches all pending registration requests.
router.post("/remove-game-of-the-day", (req, res) => {
    if ( !funcs.isAdministrator(req) )
        res.status(401).end();
    else
    {
        var data = req.body;
        var response = {
            deleted: false,
            error: null
        };

        dbconn.query("games", { godID: ObjectId(data.id) }, (games, num) => {            
            if ( num > 0 )
            {
                response.error = "*** Postoje takmicari koji igraju kviz.";
                res.status(200).json(response);
            } else
            {
                dbconn.delete("gameOfTheDay", {
                    _id: ObjectId(data.id)
                }, (del) => {
                    response.deleted = del.deletedCount > 0;
                    res.status(200).json(response);                    
                }); 
            };           
        });    
    };
});

module.exports = router;