/**
 * Tasks file. 
 */
var express = require('express');
var router = express.Router();
var dbconn = require('../assets/scripts/dbconn');

// Get all tasks.
router.get('/tasks', function(req, res, next) {
    dbconn.getAll("tasks", function(coll, num) {
        res.json(coll);
    });
});

// Get task.
router.get('/task/:id', function(req, res, next) {
    dbconn.query("tasks", {mid: parseInt(req.params.id)}, function(coll, num) {
        res.json(coll);
    });
});

// Temporary save task.
router.get('/save/task/:mid/:name', function(req, res, next) {
    dbconn.insert("tasks", {mid: parseInt(req.params.mid), name: req.params.name}, function(task) {
        //console.log(task);
        res.send('ok');
    });
});

// Save task.
router.post('/task', function(req, res, next) {
    var task = req.body;
    if ( !task.title || (task.isDone + '') ) 
    {
        res.status(400);
        res.json({err: "bad request"});
    } else
    {
        
    };
});

// Delete task.
router.delete('/delete/task/:id', function(req, res, next) {
    dbconn.delete( "tasks", "mid", parseInt(req.params.id), function(task) {
        res.send('deleted');
    });
});

// Update task.
router.put('/update/task/:id', function(req, res, next) {
    dbconn.update( "tasks", "mid", parseInt(req.params.id), function(task) {
        res.send('updated!');
    });
});

module.exports = router;