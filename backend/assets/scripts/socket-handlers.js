/**
 * Socket handlers.
 */
var dbconn = require("./dbconn");
var list = require("./socket-list").list;
var socketList = require("./socket-list").list;
var gameLists = require("./game-lists");
var waitingList = gameLists.waitingMultiplayerGames;
var singleplayerList = gameLists.singleplayerGames;
var ObjectId = require("mongodb").ObjectId;
var utils = require("./util");

var socketMutualHandlers = {

    /**
     * When users connects to the socket.
     * 
     * @param {*} socket Socket object.
     * @param {*} username Username of the user.
     */
    hello: function (socket, username, accountType) {
        if ( list.add(username, accountType, socket) )
            console.log("user registered!");
    },

    /**
     * Gets data about supervision after page reload.
     * 
     * @param {*} socket Supervisor's socket.
     * @param {*} data Data sent to server.
     */
    getDataAboutSupervision: function(socket, data) {
        var response = {            
            singleplayer: {
                active: false,
                user: null
            },
            multiplayer: {
                active: false
            }
        };   
        var u;     
        gameLists.singleplayerGames.forEach((el) => {
            if ( el.supervisor == data.sender )
            {
                response.singleplayer.active = true;
                response.singleplayer.user = el.username;                
                return false;
            };
        });

        /*
        var pr = new Promise((resolve) => {
            dbconn.query("users", {username: u}, (users, num) => {
                var u = users[0];
                response.singleplayer.user = {
                    username: 
                };
                resolve();
            });            
        });
        pr.then( () => { 
            
        });
        */
        // Send data to the supervisor.
        try {
            socket.emit( "ctrl-supervisor-get-data-about-supervision", response );
        } catch(e) {}
    },

    /**
     * Gets current game status.
     * 
     * @param {*} socket 
     * @param {*} data 
     */
    getGameStatusForSupervisor: function(socket, data)
    {
        gameLists.singleplayerGames.forEach((el) => {
            if ( el.supervisor == data.sender )
            {
                //console.log(el);
                try {
                    socket.emit("ctrl-get-game-status", {
                        godID: el.godID,
                        started: el.started,
                        currentGame: el.currentGame,
                        supervision: el.supervision
                    });
                } catch(e) {}
                return false;
            }
        });
    },

    /**
     * Gets points for user game.
     * 
     * @param {*} socket 
     * @param {*} obj 
     */
    reviewPoints: function(socket, obj)
    {
        gameLists.singleplayerGames.forEach((el) => {
            if ( el.username == obj.sender )
            {
                var games = el.games;
                try {
                    socket.emit("ctrl-sp-points-review", {
                        points: {
                            anagram: games[0].getPoints(),
                            number: games[1].getPoints(),
                            fxf: games[2].getPoints(),
                            natgeo: games[3].getPoints(),
                            trophie: games[4].getPoints()
                        }
                    });
                } catch(e) {}
                return false;
            }
        });
    },

    /**
     * Terminates active connection with server.
     * 
     * @param {socket} socket Connection socket.
     */
    terminateGameForMe: function(socket) {
        var wo;
        var username = list.getUsernameBySocket(socket);
        var deleteu = false;
        //var pos = -1;
        var host = null;
        for ( waitingList.i_start(); waitingList.i_isNotNull(); waitingList.i_next() )
        {
            wo = waitingList.i_get();
            if ( wo.UserRed == username )
            {
                //pos = wo.UserBlue == username ? 0 : 1;
                deleteu = true;  
                host = wo.UserBlue;
                break;   
            };
        };
        if ( deleteu )
        {
            //pos == 0 ? waitingList.removeRedContestantForHost(host) : null;
            var otherUser = list.getSocket(host);            
            try {
                otherUser.emit("not-ready-to-go", { red: true, supervisor: false });
            } catch(e) {}
            waitingList.removeRedContestantForHost(host);
        };

        if ( list.deleteBySocketId(socket.id) )
            console.log("user deregistered!");
    },

    /**
     * Signs in red contestant into the waiting game.
     */
    signInRedContestant: function(client, data) {
        console.log("signs in red contestant!");
        console.log("list.getUsernameBySocket(client) = " + list.getUsernameBySocket(client));
        console.log("blueUser = " + data.blue);
        waitingList.addRedContestantForHost( list.getUsernameBySocket(client), data.blue );
    },


    /**
     * Making new game.
     * 
     * @param {socket} client Client's socket.
     * @param {*} username Username for which new game is made.
     */
    createGameFor: function(client, username) {
        waitingList.createWaitingGame(username);
    },

    /**
     * Deletes waiting game.
     * 
     * @param {socket} client Client's socket.
     * @param {string} username Username for which waiting games is deleting.
     */
    declineGameFor: function(client, username) {
        var otherUser = list.getSocket( waitingList.getOpponent(username) );
        try {
            otherUser.emit("on-game-termination", true);
        } catch(e) {}
        waitingList.deleteAllHosts(username);
    },

    /**
     * Checks if red contestant can sign-out.
     * 
     * @param {socket} client Client's socket.
     * @param {any} username Client's username
     */
    canSignOut: function(client, username) {
        // Check active games list.
        client.emit("can-sign-out", true);
    },


    /**
     * Passes data to end terminating user.
     * 
     * @param {*} client Client which initiated connection.
     * @param {*} obj Send instructions.
     */
    passDataToDestination: function(client, obj) {
        var socket = socketList.getSocket(obj.dest);
        try {
            socket.emit( "ctrl-" + obj.name, obj.data );
        } catch(e) {}
    },

    /**
     * Gets data about user.
     * 
     * @param {*} client 
     * @param {*} obj 
     */
    getDataAboutUser: function(client, obj)
    {
        var user = obj.data;
        var resp = {
            found: false,
            user: null
        };
        dbconn.query("users", {username: user}, (users, num) => {
            var user = users[0];
            resp.found = num > 0;
            try {
                resp.user = {
                    username: user.username,
                    name: user.name,
                    surname: user.surname,
                    image: user.image,
                    profession: user.profession,
                    email: user.email
                };
                
                client.emit("ctrl-get-data-about-user", resp);
            } catch(e) {}
        });
    }

};

/**
 * SINGLEPLAYER MANAGEMENT OF THE GAME.
 */
var socketSingleplayerHandlers = {

    /**
     * Creates game for current user.
     * 
     * @param {*} client 
     * @param {*} data 
     */
    createGame: function(client, data) {        
        singleplayerList.createWaitingGame(data);   
        // Notify all supervisors.
        var elem, user;        
        for ( list.i_start(); list.i_isNotNull(); list.i_next() )
        {
            elem = list.i_get();
            // Notify!
            if ( elem.accountType == 1 )
            {
                try {
                    elem.socket.emit("ctrl-sp-notify-supervisor", data);
                } catch(e) {}
            }
        }            
    },

    /**
     * Deletes game in the list.
     * 
     * @param {*} client 
     * @param {*} data 
     */
    declineGame: function(client, data) {
        singleplayerList.deleteAllHosts(data.sender);

        // Notify all supervisors.
        var elem, user;        
        for ( list.i_start(); list.i_isNotNull(); list.i_next() )
        {
            elem = list.i_get();
            // Notify!
            if ( elem.accountType == 1 )
            {
                try {
                    elem.socket.emit("ctrl-sp-user-declined", data.sender);
                } catch(e) {}
            }
        }
    },

    /**
     * Gets data for user.
     * 
     * @param {*} client 
     * @param {*} username 
     */
    getDataForUser: function(client, username)
    {
        dbconn.query("users", {username: username}, (users, len) => {
            var user = users[0];
            client.emit("ctrl-sp-data-about-user", {
                username: user.username,
                name: user.name,
                surname: user.surname,
                image: user.image,
                profession: user.profession,
                email: user.email
            });
        });
    },

    /**
     * Registers supervisor
     * 
     * @param {*} client 
     * @param {*} obj 
     */
    registerSupervisor: function(client, obj)
    {              
        singleplayerList.forEach((el) => {
            if ( el.username == obj.data.for )
            {
                el.supervisor = obj.sender;                
                return false;
            };
            return true;
        });
    },

    /**
     * Sets supervision to finished state.
     * 
     * @param {*} socket Supervisor's socket.
     * @param {*} data Data sent to server.
     */
    finishSupervision: function(socket, obj) {   
        var u, su, ref, response = {}, points = 0, arr = obj.data;   

        gameLists.singleplayerGames.forEach((el) => {
            if ( el.supervisor == obj.sender )
            {
                el.supervision.finished = true;
                el.supervision.results = arr;
                u = el.username;  
                ref = el;       
                return false;
            };
        });
        
        for (var i=0; i<arr.length; i++)
        {
            if ( arr[i].checked )
                points += 4;
        };
        response.terms = obj.data;
        response.points = points;
        response.supervision = ref.supervision;
        ref.games[ref.currentGame].addPoints(points);
        ref.games[ref.currentGame].setStatus(-1);
        response.status = -1;

        // Gets user socket.
        su = socketList.getSocket(u);

        // Send data to the supervisor.
        try {
            socket.emit( "ctrl-supervisor-sp-finished", {} );
        } catch(e) {}

        // Notify user who's waiting.
        try {
            su.emit( "ctrl-sp-supervisor-finished", response );
        } catch(e) {}

        // Add all terms in the database.
        utils.appendTermsInTheDatabse(obj.data);
    },

    /**
     * Begins real game for singleplayer.
     * 
     * @param {*} client 
     * @param {*} data 
     */
    beginGame: function(client, obj)
    {   
        var socketSupervisor = socketList.getSocket(obj.data.supervisor);
        try {            
            var list = gameLists.singleplayerGames;
            list.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    el.started = true;
                    el.startedDate = new Date();
                    //el.currentGame = 0;
                    var date = new Date();
                    var outputDate = new Date(date);
                    outputDate.setMinutes( date.getMinutes() + 2 );
                    outputDate.setSeconds( date.getSeconds() + 0 );
                    el.timeout = outputDate;
                    return false;
                };
            });
            
            socketSupervisor.emit("ctrl-sp-begin-game", {});
        } catch(e) {}
    },

    /**
     * Terminates current singleplayer game.
     * 
     * @param {*} client 
     * @param {*} obj 
     */
    terminateGame: function(client, obj)
    {
        gameLists.singleplayerGames.deleteAllHosts(obj.sender);
        try {
            client.emit("ctrl-sp-terminate-game", {});
        } catch(e) {}
        try {
            socketList.getSocket(el.supervisor).emit("ctrl-supervisor-sp-game-terminated", {});
        } catch(e) {}

    },

    /**
     * Inits game with data.
     * 
     * @param {*} client 
     * @param {*} obj 
     */
    initGameInfo: function(client, obj) 
    {
        var retval = {};
        var points = null;
        var ref = null;
        var anagram,
            fxf, 
            trophie;

        // Load all games.
        var getGameOfTheDay = new Promise((resolve) => {
            var date = new Date();
            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();
            dbconn.query("gameOfTheDay", {
                date: {
                    $gte: new Date(year, month, day, 0, 0, 0)
                }
            }, (games, len) => {
                var game = games[0];
                resolve(game);
            });
        });

        getGameOfTheDay.then((game) => {
            anagram = game.anagramID;
            fxf = game.fxfID;
            trophie = game.trophieID;

            var s1, s2, s3;
            s1 = new Promise((resolve) => {
                dbconn.query("anagrams", { _id: ObjectId(anagram) }, (a,l) => {
                    resolve(a[0]);
                });
            });

            s2 = new Promise((resolve) => {
                dbconn.query("5x5", { _id: ObjectId(fxf) }, (f,l) => {
                    resolve(f[0]);
                });
            });

            s3 = new Promise((resolve) => {
                dbconn.query("trophies", { _id: ObjectId(trophie) }, (d,l) => {
                    resolve(d[0]);
                });
            });

            var prBlue = new Promise((resolve) => {
                dbconn.query("users", {username: obj.sender}, (users, num) => {
                    var s = users[0];
                    retval.blue = {
                        username: s.username,
                        name: s.name,
                        surname: s.surname,
                        image: s.image,
                        email: s.email,
                        profession: s.profession,
                        loaded: true
                    };
                    resolve();
                });
            });

            // Attaches handler when all promises are resolved.
            Promise.all([s1, s2, s3]).then((values) => {
                gameLists.singleplayerGames.forEach((el) => {
                    if ( obj.sender == el.username )
                    {
                        points = 0;
                        for (var i=0; i<el.games.length; i++)
                            points += el.games[i].getPoints();
                        // Fill in games.                        
                        this.fillGames(el.games, values[0], values[1], values[2]);                       
                        retval.currentGame = el.currentGame;                                    
                        retval.godID = game.godID;
                        retval.finished = el.finished;

                        prSup = new Promise((resolve) => {
                            dbconn.query("users", {username: el.supervisor}, (users, num) => {
                                try {
                                    var s = users[0];
                                    retval.supervisor = {
                                        username: s.username,
                                        name: s.name,
                                        surname: s.surname,
                                        image: s.image,
                                        email: s.email,
                                        profession: s.profession,
                                        loaded: true
                                    };
                                } catch(e) {}
                                resolve();
                            });
                        });                       
                        return false;
                    };
                });
                try {
                    Promise.all([prSup, prBlue]).then((values) => {
                        retval.blue.points = points;
                        client.emit("ctrl-get-data-about-game", retval);    
                    });                         
                } catch(e) {}
            });
        });        
    },

    fillGames: function(games, s1, s2, s3)
    {
        // Set anagram options.
        var game = games[0];
        game.setQuestion(s1.title);
        game.setAnswer(s1.result);

        // Set 5x5 game.
        var w;
        game = games[2];
        for (var i=0; i<5; i++)
        {
            w = s2["w" + (i+1)];
            for (var j=0; j<5; j++)
                game.setLetterInMatrix(i, j, w[j]);
        };

        // Set letter for natgeo game.
        game = games[3];
        if ( !game.isFilled() )
        {
            var ind = 0;
            var arr = utils.genAlphabetArray('A', 'Z', false);
            var len = arr.length;
            ind = Math.floor( Math.random() * (len-1) );
            game.setLetter(arr[ind]);
        };

        game = games[4];
        for (var i=0; i<13; i++)
        {
            game.setQuestion(i, s3["q" + (i+1)]);
            game.setAnswer(i, s3["a" + (i+1)]);
        };
    },

    // GAMES SUBOBJECTS.
    anagram: {

        /**
         * Gets game info.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        init: function(client, obj) 
        {
            var retval = {};
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var g = el.games[el.currentGame];                    
                    retval.game = g.asObject();

                    // Can set timeout for anagram game!
                    g.setTimeout();
                    retval.timeout = g.getTimeout(); 
                    retval.timeoutInterval = g.getTimeoutInterval();
                };
            });

            try {
                client.emit("ctrl-sp-anagram-game-info", retval);
            } catch(e) {}
        },

        /**
         * User finished anagram game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        finished: function(client, obj) 
        {
            var data = obj.data;
            var retval = {
                points: 0
            };
            var sup = null, ind = 0;
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    sup = el.supervisor;
                    var game = el.games[el.currentGame];
                    console.log(el);
                    if ( game.checkResult(data.answer) )
                    {
                        retval.points = 10;
                    };
                    el.currentGame++;
                    ind = el.currentGame;
                    return false;
                };
            });
            try {
                client.emit("ctrl-sp-anagram-finished", retval);
            } catch(e) {}
            try {
                var supervisorSocket = socketList.getSocket(sup);
                supervisorSocket.emit("ctrl-game-change", {
                    gameIndex: ind
                });
            } catch(e) {}
        }
    },

    /**
     * NUMBER GAME.
     */
    number: {

        /**
         * Gets basic info about the game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        init: function(client, obj) 
        {
            var retval = {};
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    console.log("NUMBER INIT", el);
                    var game = el.games[el.currentGame];
                    retval.game = game.asObject();
                    retval.status = game.getStatus();
                    retval.timeout = game.getTimeout();
                    return false;  
                };
            });
            try {
                client.emit("ctrl-sp-number-game-info", retval);
            } catch(e) {}
        },

        /**
         * Stops spinning numbers for the user.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        stopSpinning: function(client, obj)
        {
            var retval = {};
            var data = obj.data;
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];
                    game.startGame(data);
                    retval.timeoutInterval = game.getTimeoutInterval();                    
                    return false;
                };
            });
            try {
                client.emit("ctrl-sp-number-stop-spinning", retval);
            } catch(e) {}
        },

        /**
         * User finished number game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        finished: function(client, obj) 
        {
            var data = obj.data;
            var retval = {
                points: 0
            };
            var ind = 0, sup = null;
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    sup = el.supervisor;
                    var game = el.games[el.currentGame];
                    game.checkResult(data);
                    retval.points = game.getPoints();
                    el.currentGame++;
                    ind = el.currentGame;
                    return false;
                };
            });
            try {
                client.emit("ctrl-sp-number-finish-game", retval);
            } catch(e) {}
            try {
                var supervisorSocket = socketList.getSocket(sup);
                supervisorSocket.emit("ctrl-game-change", {
                    gameIndex: ind
                });
            } catch(e) {}
        }
    },

    /**
     * 5x5 GAME
     */
    fxf : {
        /**
         * Gets basic info about the game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        init: function(client, obj) 
        {
            var retval = {};
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];
                    game.startGame(); 
                    retval.game = game.asObject();                                      
                    retval.timeoutInterval = game.getTimeoutInterval();
                    retval.timeout = game.getTimeout();
                    return false;  
                };
            });
            try {
                client.emit("ctrl-sp-fxf-game-info", retval);
            } catch(e) {}
        },

        /**
         * Fires when user presses letter.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        letterPressed: function(client, obj) 
        {
            var data = obj.data;
            var retval = {
                letter: data.letter
            };
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];
                    retval.letterDoesNotExist = !game.checkIfLetterExists(data); 
                    if ( !retval.letterDoesNotExist )
                        game.addPoints(1);
                    game.snapshotBefore();
                    game.setLetterPressed(data.letter);                  
                    game.snapshotAfter();
                    retval.positions = game.getPositionsForLetter(data.letter);
                    retval.points = game.calculatePoints();
                    if ( !retval.letterDoesNotExist )
                        retval.points++;
                    retval.isFinished = game.isFinished();
                    return false;  
                };
            });
            try {
                client.emit("ctrl-sp-fxf-letter-pressed", retval);
            } catch(e) {}
        },

        /**
         * Releases all table data.
         * 
         * @param {*} client Client's socket.
         * @param {*} obj Data.
         */
        releaseAllTable: function(client, obj)
        {
            var retval = {};
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];                    
                    retval.table = game.getAllData();                    
                    return false;  
                };
            });
            try {
                client.emit("ctrl-sp-fxf-release-all-table", retval);
            } catch(e) {}
        },

        /**
         * User finished 5x5 game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        finished: function(client, obj) 
        {
            var data = obj.data;
            var retval = {};
            var sup = null;
            var ind = 0;
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {                    
                    sup = el.supervisor;
                    el.currentGame++;
                    ind = el.currentGame;
                    return false;
                };
            });
            try {
                client.emit("ctrl-sp-fxf-finish-game", retval);
            } catch(e) {}
            try {
                var supervisorSocket = socketList.getSocket(sup);
                supervisorSocket.emit("ctrl-game-change", {
                    gameIndex: ind
                });
            } catch(e) {}
        }
    },

    /**
     * NATGEO GAME.
     */
    natgeo: {
        /**
         * Gets basic info about the game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        init: function(client, obj) 
        {
            var retval = {};
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];
                    game.startGame(); 
                    retval.game = game.asObject();                                      
                    retval.timeoutInterval = game.getTimeoutInterval();
                    retval.timeout = game.getTimeout();
                    retval.supervision = el.supervision;
                    retval.status = game.getStatus();
                    return false;  
                };
            });
            try {
                client.emit("ctrl-sp-natgeo-game-info", retval);
            } catch(e) {}
        },

        /**
         * Saves current input value.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        save: function(client, obj) 
        {
            var data = obj.data;            
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];
                    game.setProperty(data.field, data.value);
                    return false;  
                };
            }); 
            try {
                client.emit("ctrl-sp-natgeo-save-input", {});
            } catch(e) {}          
        },

        /**
         * Supervise submitted terms.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        superviseTerms: function(client, obj) 
        {
            var data = obj.data;   
            var game = null;         
            var promises = [];
            var supervisor = null;
            var ref = null;

            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    ref = el;
                    game = el.games[el.currentGame];  
                    supervisor = el.supervisor;                  
                    return false;  
                };
            }); 

            // Update game fields && make query requests.
            for ( var key in data )
            {
                if ( data[key] != "" )
                    game.setProperty(key, data[key]);

                var term = game.getProperty(key);
                if ( term == "" )
                    continue;
                var o = new Object();
                o.term = new String( term.toLowerCase() );
                promises.push(
                    new Promise((resolve) => {
                        console.log(o.term);
                        var k = key;
                        var doc = {
                            word: {
                                $regex: new RegExp("^" + o.term, "i"),
                            },                  
                            category: key,
                        };
                        dbconn.query("words", doc, (words, len) => {
                            if ( len == 0 )
                                resolve({
                                    key: k,
                                    value: o.term,
                                    resolved: false
                                });
                            else
                                resolve({
                                    key: k,
                                    value: o.term,
                                    resolved: true
                                });
                        });
                    })
                );
            };

            // Prepare words for supervisor.
            Promise.all(promises).then((values) => {
                
                var retval = {
                    points: 0 ,
                    status: -1,
                    approved: []                   
                };
                var forSupervision = [];
                for (var i=0; i<values.length; i++)
                {
                    // Word is not resolved!
                    if ( !values[i].resolved )
                    {
                        forSupervision.push({
                            key: values[i].key,
                            value: game.getProperty(values[i].key)
                        });
                    } else
                    {
                        ref.supervision.dbChecked.push({
                            category: values[i].key
                        });
                        game.addPoints(2);
                        retval.points += 2;
                        retval.approved.push({
                            key: values[i].key,
                            value: values[i].value
                        });
                    };
                };
                retval.status = forSupervision.length > 0 ? 2 : 3;
                ref.supervision.superviseTerms = forSupervision;
                ref.supervision.inSupervision = true;

                try {
                    client.emit("ctrl-sp-natgeo-supervise-terms", retval);
                } catch(e) {}    

                var supervisorSocket = socketList.getSocket(supervisor);
                try {
                    supervisorSocket.emit("ctrl-sp-terms-for-supervision", forSupervision);
                } catch(e) {}
            });                 
        },

        /**
         * User finished natgeo game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        finished: function(client, obj) 
        {
            var data = obj.data;
            var retval = {};
            var sup = null;
            var ind = 0;
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {                    
                    sup = el.supervisor;
                    el.supervision.finished = true;
                    el.currentGame++;
                    ind = el.currentGame;
                    return false;
                };
            });
            try {
                client.emit("ctrl-sp-natgeo-finish-game", retval);
            } catch(e) {}
            try {
                var supervisorSocket = socketList.getSocket(sup);
                supervisorSocket.emit("ctrl-game-change", {
                    gameIndex: ind
                });
            } catch(e) {}
        }
    },

    /**
     * TROPHIE GAME.
     */
    trophie: {
        /**
         * Gets basic info about the game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        init: function(client, obj) 
        {
            var retval = {};
            var sup;
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];
                    sup = el.supervisor;
                    retval.started = game.startGame(); 
                    retval.game = game.asObject();                                      
                    retval.timeoutInterval = game.getTimeoutInterval();
                    retval.timeout = game.getTimeout();                  
                    //retval.status = game.getStatus();
                    return false;  
                };
            });
            try {
                client.emit("ctrl-sp-trophie-game-info", retval);
            } catch(e) {}
            try {
                socketList.getSocket(sup).emit("ctrl-game-change", {
                    gameIndex: 5
                });
            } catch(e) {}
        },

        /**
         * Gets basic info about the game.
         * 
         * @param {*} client 
         * @param {*} obj 
         */
        submitAnswer: function(client, obj) 
        {
            var retval = {};
            var f = new Promise((resolve) => { resolve(); });
            gameLists.singleplayerGames.forEach((el) => {
                if ( el.username == obj.sender )
                {
                    var game = el.games[el.currentGame];
                    retval.correct = game.testAnswer(obj.data); 
                    retval.answer = game.getAnswer(game.getCurrentQuestion());  
                    game.moveToNextQuestion();
                    retval.timeout = game.getTimeout();
                    retval.timeoutInterval = game.getTimeoutInterval();
                    retval.points = retval.correct ? 5 : 0;
                    retval.finished = game.asObject().finished;
                    if ( retval.finished )
                    {
                        el.finished = true;
                        // Add data in the database.
                        f = new Promise((resolve) => {
                            var doc = {
                                date: el.startedDate,
                                userID: el.username,
                                points_anagram: el.games[0].getPoints(),
                                points_number: el.games[1].getPoints(),
                                points_fxf: el.games[2].getPoints(),
                                points_natgeo: el.games[3].getPoints(),
                                points_trophie: el.games[4].getPoints(),
                                supervisorID: el.supervisor
                            };
                            dbconn.insert("games", doc, (ins) => {
                                try {
                                    client.emit("ctrl-sp-points-review", {
                                        points: {
                                            anagram: doc.points_anagram,
                                            number: doc.points_number,
                                            fxf: doc.points_fxf,
                                            natgeo: doc.points_natgeo,
                                            trophie: doc.points_trophie
                                        }
                                    });
                                } catch(e) {}
                                try {
                                    socketList.getSocket(el.supervisor).emit("ctrl-supervisor-sp-game-terminated", {});
                                } catch(e) {}
                                resolve();
                            });
                        });
                    }
                    return false;  
                };
            });
            f.then(() => {
                try {
                    client.emit("ctrl-sp-trophie-submit-answer", retval);
                } catch(e) {}
            });
        },
    }

};

var socketMultiplayerHandlers = {};

// Export all handlers.
module.exports = {
    mutual: socketMutualHandlers,
    singleplayer: socketSingleplayerHandlers,
    multiplayer: socketMultiplayerHandlers
};