/**
 * All lists for games.
 */
var games = require("./games");

 var WaitingMultiplayerGames = function() {
    var first = null;
    var last = null;
    var temp = null;
    var num = 0;

    this.i_start = function() { temp = first; }
    this.i_isNotNull = function() { return temp != null;  }
    this.i_get = function() { return temp; }
    this.i_next = function() { temp = temp.next; }

    /**
     * Creates host for the game.
     */
    this.createWaitingGame = function(host) 
    {
        this.deleteAllHosts(host);

        var obj = {
            UserBlue: host,
            UserRed: null,
            Supervisor: null,
            BlueStarted: new Date(),

            notifiedBlueAboutRed: false,
            notifiedBlueAboutSupervisor: false,
            notifiedRedAboutOK: false,

            next: null
        };
        if ( first == null )
            first = last = obj;
        else
            last = last.next = obj;
        num++;
        return true;
    };

    /**
     * Gets red user based on blue one.
     */
    this.getRedUser = function(blue) 
    {
        var g = first;
        while( g )
        {
            if ( g.UserBlue == blue )
                return g.UserRed;                
            g = g.next;
        }
        return null; 
    }


    /**
     * Gets blue user based on red one.
     */
    this.getBlueUser = function(red) 
    {
        var g = first;
        while( g )
        {
            if ( g.UserRed == red )
                return g.UserBlue;                
            g = g.next;
        }
        return null; 
    }

    /**
     * Gets opponent's username.
     */
    this.getOpponent = function(user) 
    {
        var g = first;
        while( g )
        {
            if ( g.UserRed == user )
                return g.UserBlue;
            else if ( g.UserBlue == user )
                return g.UserRed;                
            g = g.next;
        }
        return null; 
    }

    /**
     * Adds red contestant.
     */
    this.addRedContestantForHost = function(redUser, host)
    {
        var g = first;
        while( g )
        {
            if ( g.UserBlue == host )
            {
                g.UserRed = redUser;
                return true;
            };
            g = g.next;
        }
        return false;
    };

    /**
     * Adds supervisor.
     */
    this.addSupervisorForHost = function(supervisor, host)
    {
        var g = first;
        while( g )
        {
            if ( g.UserBlue == host )
            {
                g.Supervisor = supervisor;
                return true;
            };
            g = g.next;
        }
        return false;
    };

    /**
     * Removes red contestant.
     */
    this.removeRedContestantForHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.UserBlue == host )
            {
                g.UserRed = null;
                g.notifiedBlueAboutRed = false;
                return true;
            };
            g = g.next;
        }
        return false;
    };

    /**
     * Removes supervisor.
     */
    this.removeSupervisorForHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.UserBlue == host )
            {
                g.Supervisor = null;
                return true;
            };
            g = g.next;
        }
        return false;
    };

    /**
     * Checks if host is ready to play the game.
     */
    this.isReadyHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.UserBlue == host )
                return g.UserRed != null && g.Supervisor != null;
            g = g.next;
        };
        return false;
    }

    /**
     * Checks if user has active socket connection.
     */
    this.existsHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.UserBlue == host )
                return true;
            g = g.next;
        }
        return false;
    }

    /**
     * Deletes all hosts in the system.
     */
    this.deleteAllHosts = function(host) 
    {
        var curr = first, p = null;
        while( curr )
        {
            if ( curr.UserBlue == host )
            {
                // We delete form the beggining of the list.
                if ( p == null )
                {
                    first = curr.next;
                    if ( first == null )
                        last = null;
                } else
                {
                    p.next = curr.next;                    
                    if ( last == curr )
                        last = p;
                }; 
                num--;               
            };
            p = curr;
            curr = curr.next;
        }
    }

    /**
     * Gets number of elements inside waiting list.
     */
    this.size = function() 
    {
        return num;
    }
 };

 // ====================================================================================================
//      Waiting/playing singleplayer games.
 // ====================================================================================================

 var singleplayerGames = function() {
    var first = null;
    var last = null;
    var temp = null;
    var num = 0;

    this.i_start = function() { temp = first; }
    this.i_isNotNull = function() { return temp != null;  }
    this.i_get = function() { return temp; }
    this.i_next = function() { temp = temp.next; }
    

    /**
     * Creates host for the game.
     */
    this.createWaitingGame = function(host) 
    {
        this.deleteAllHosts(host);

        var obj = {
            username: host,
            godID: null,
            startedDate: null,
            started: false,
            finished: false,
            supervisor: null,
            currentGame: 0,
            timeout: null,
            supervision: {
                inSupervision: false,
                superviseTerms: [],
                finished: false,
                results: [],
                dbChecked: []
            },            
            points: 0,
            games: [
                new games.gameAnagram(),
                new games.gameNumber(),
                new games.gameFxF(),
                new games.gameNatGeo(),
                new games.gameTrophie()
            ],            
            /*
            notifiedBlueAboutRed: false,
            notifiedBlueAboutSupervisor: false,
            notifiedRedAboutOK: false,            
            */
            next: null
        };
        if ( first == null )
            first = last = obj;
        else
            last = last.next = obj;
        num++;
        return true;
    };

    /**
     * Executes callback for each element inside the list.
     */
    this.forEach = function(callback)
    {
        var g = first;
        var n, retval;
        while( g )
        {
            n = g.next;
            g.next = null;
            retval = callback(g);                
            g.next = n;
            if ( typeof retval == typeof true && !retval ) 
                break;
            g = g.next;
        };
    }

    /**
     * Adds supervisor.
     */
    this.addSupervisorForHost = function(supervisor, host)
    {
        var g = first;
        while( g )
        {
            if ( g.username == host )
            {
                g.supervisor = supervisor;
                return true;
            };
            g = g.next;
        }
        return false;
    };

    /**
     * Removes supervisor.
     */
    this.removeSupervisorForHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.username == host )
            {
                g.supervisor = null;
                return true;
            };
            g = g.next;
        }
        return false;
    };

    /**
     * Checks if host is ready to play the game.
     */
    this.isReadyHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.username == host )
                return g.supervisor != null;
            g = g.next;
        };
        return false;
    }

    /**
     * Checks if host is waiting.
     */
    this.isWaitingHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.username == host && !g.started )
                return true;
            g = g.next;
        };
        return false;
    }

    /**
     * Checks if host is already playing game.
     */
    this.isActiveHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.username == host && g.started )
                return true;
            g = g.next;
        };
        return false;
    }

    /**
     * Checks if user has active socket connection.
     */
    this.existsHost = function(host)
    {
        var g = first;
        while( g )
        {
            if ( g.username == host )
                return true;
            g = g.next;
        }
        return false;
    }

    /**
     * Deletes all hosts in the system.
     */
    this.deleteAllHosts = function(host) 
    {
        var curr = first, p = null;
        while( curr )
        {
            if ( curr.username == host )
            {
                // We delete form the beggining of the list.
                if ( p == null )
                {
                    first = curr.next;
                    if ( first == null )
                        last = null;
                } else
                {
                    p.next = curr.next;                    
                    if ( last == curr )
                        last = p;
                }; 
                num--;               
            };
            p = curr;
            curr = curr.next;
        }
    }

    /**
     * Gets number of elements inside waiting list.
     */
    this.size = function() 
    {
        return num;
    }
 };

 module.exports = {
    waitingMultiplayerGames: new WaitingMultiplayerGames(),
    singleplayerGames: new singleplayerGames()
 };