/**
 * Package for all games.
 */

 // Anagram game.
 var gameAnagram = function() {
    var points = 0;
    var timeout;
    var isSetTimeout = false;
    var question;
    var answer;
    var timeoutInterval = 60;

    this.getPoints = function() {
        return points;
    };

    this.addPoints = function(amount) {
        if ( amount > 0 )
            points += amount;
    }

    this.setQuestion = function(q) {
        question = q;
    }

    this.setAnswer = function(a) {
        answer = a;
    }

    this.setTimeout = function() {
        if ( !isSetTimeout )
        {
            var date = new Date();
            date.setSeconds( date.getSeconds() + timeoutInterval );
            timeout = date;
            isSetTimeout = true;
        };
    }

    this.isSetTimeout = function() {
        return isSetTimeout;
    }

    this.isValid = function() {
        if ( !isSetTimeout )
            return true;
        else
        {
            var d = new Date();
            if ( d.getTime() - timeout.getTime() >= 0 )
                return false;
        };
    }

    this.getTimeout = function() {        
        return timeout;
    }

    this.getTimeoutInterval = function() {
        return timeoutInterval;
    }

    this.asObject = function() {
        return {
            question: question
        };
    }

    this.checkResult = function(res) {
        var regexp = new RegExp("^" + answer + "$", "i");
        if ( regexp.test(res) )
        {
            points = 10;
            return true;
        } else
            return false;
    }

 };

 // My Number game.
 var gameNumber = function() {
    var points = 0;
    var timeout = null;
    var status = 0;
    var timeoutInterval = 60;

    // Game options.
    var number;
    var partialNumbers = [];
    var expression;

    this.getPoints = function() {
        return points;
    };

    this.addPoints = function(amount) {
        if ( amount > 0 )
            points += amount;
    }

    this.setExpression = function(exp) {
        expression = exp;
    }

    this.getExpression = function(exp) {
        return expression;
    }

    this.evalExpression = function() {
        try {
            return eval(expression);
        } catch(e) {
            return false;
        }
    }

    this.setTimeout = function(time) {
        timeout = time;
    }

    this.getTimeout = function() {
        return timeout;
    }

    this.getWantedNumber = function() {
        return number;
    }

    this.setWantedNumber = function(num) {
        number = num;
    }

    this.getPartialNumbers = function() {
        return partialNumbers;
    }

    this.setPartialNumbers = function(nums) {
        partialNumbers = nums;
    }

    this.asObject = function() {
        var obj = {};
        obj.wantedNumber = this.getWantedNumber();
        obj.partialNumbers = this.getPartialNumbers();
        obj.expression = expression;
        return obj;
    }

    this.getStatus = function() {
        return status;
    }

    this.getTimeoutInterval = function() {
        return timeoutInterval;
    }

    this.startGame = function(data) {
        status = 1;
        partialNumbers = data.numbers;
        number = data.wantedNumber;
        var date = new Date();
        date.setSeconds( date.getSeconds() + timeoutInterval );
        timeout = date;
    }

    this.checkResult = function(data) {
        if ( data.isValidResult && data.result == number )
        {
            points = 10;
        };
    }
 };

 // 5x5 game.
 var gameFxF = function() {
    var points = 0;
    var timeout = null;
    var timeoutInterval = 30;
    var started = false;

    var genCharArray = function(charA, charZ)
    {
        var a = {}, i = charA.charCodeAt(0), j = charZ.charCodeAt(0), v;
        for (; i <= j; ++i) 
        {
          v = String.fromCharCode(i);
          switch( v )
          {
          case "W": 
          case "X":
          case "Y":
            break;
  
          default:
              a[v] = false;
              break;
          };          
        };
        
            a['Ć'] = false;
            a['Š'] = false;
            a['Đ'] = false;
            a['Ž'] = false;
            a['Č'] = false;       
        return a;
    };

    // Game options.
    var matrix = [];
    var checked = [];
    var letters = genCharArray('A', 'Z');
    var snapshotBefore = [];
    var snapshotAfter = [];
    var snapshotStatus = 0;
    for ( var i=0; i<5; i++ )
    {
        matrix[i] = [];
        checked[i] = [];
        for (var j=0; j<5; j++)
        {
            matrix[i][j] = null;
            checked[i][j] = false;
        };
    };

    this.getPoints = function() {
        return points;
    };

    this.addPoints = function(amount) {
        if ( amount > 0 )
            points += amount;
    }

    this.setTimeout = function(time) {
        timeout = time;
    }

    this.getTimeout = function() {
        return timeout;
    }

    this.getMatrix = function() {
        return matrix;
    }

    this.setLetterInMatrix = function(i, j, letter) {
        if ( i<0 || j<0 || i>=5 || j>=5 )
            return;

        matrix[i][j] = letter;
    }

    this.flushMatrix = function() {
        for ( var i=0; i<5; i++ )        
            for (var j=0; j<5; j++)
                matrix[i][j] = null;
    }

    this.asObject = function() {
        var obj = {};
        obj.checked = [];
        obj.checkedLetters = [];
        for( var key in letters )
        {
            if ( letters[key] )
                obj.checkedLetters.push(key);
        };        
        var m = 0;
        for (var i=0; i<5; i++)
        {            
            for ( var j=0; j<5; j++ )
            {
                if ( checked[i][j] )
                    obj.checked[m++] = {
                        i: i,
                        j: j,
                        letter: matrix[i][j]
                    };
            };
        };
        return obj;        
    }

    this.setLetterPressed = function(l) {
        letters[l] = true;        
        for (var i=0; i<5; i++)        
            for ( var j=0; j<5; j++ )
                if ( matrix[i][j] != null && matrix[i][j].toUpperCase() == l )
                    checked[i][j] = true;
    }

    this.checkIfLetterExists = function(data) {
        var letter = data.letter;
        for (var i=0; i<5; i++)        
            for ( var j=0; j<5; j++ )
                if ( matrix[i][j] != null && matrix[i][j].toUpperCase() == letter )
                    return true;
        return false;       
    }

    this.getPositionsForLetter = function(l)
    {
        var positions = [];
        for (var i=0; i<5; i++)        
            for ( var j=0; j<5; j++ )
                if ( matrix[i][j] != null && matrix[i][j].toUpperCase() == l )
                {
                    positions.push({
                        i: i,
                        j: j                        
                    });                   
                };
        return positions;
    }

    this.startGame = function() {
        if ( started )
            return;
        var date = new Date();
        date.setSeconds( date.getSeconds() + timeoutInterval );
        timeout = date;
        started = true;
    }

    this.getAllData = function() {
        var m = [];
        for (var i=0; i<5; i++)
        {
            m[i] = [];
            for (var j=0; j<5; j++)
                m[i][j] = matrix[i][j];
        };
        return m;
    }

    this.getTimeoutInterval = function() {
        return timeoutInterval;
    }

    this.calculatePoints = function() {        

        if ( snapshotStatus < 2 )
            return 0;

        var addition = 0;
        for (var i=0; i<5; i++)        
        {
            if ( snapshotBefore[i].horiznotal != snapshotAfter[i].horiznotal )
            {
                points += 2;
                addition += 2;
            }; 
            
            if ( snapshotBefore[i].vertical != snapshotAfter[i].vertical )
            {
                points += 2;
                addition += 2;
            };
        };
        snapshotStatus = 0;

        return addition;
    }

    this.snapshotBefore = function() {
        var hor = true, ver = true;
        for (var i=0; i<5; i++)        
        {
            hor = true;
            ver = true;
            for ( var j=0; j<5; j++ )
            {
                hor = hor && checked[i][j];
                ver = ver && checked[j][i];
            };
            snapshotBefore[i] = {
                horiznotal: hor,
                vertical: ver
            };
        };
        snapshotStatus = 1;
                
    }

    this.snapshotAfter = function() {
        var hor = true, ver = true;
        for (var i=0; i<5; i++)        
        {
            hor = true;
            ver = true;
            for ( var j=0; j<5; j++ )
            {
                hor = hor && checked[i][j];
                ver = ver && checked[j][i];
            };
            snapshotAfter[i] = {
                horiznotal: hor,
                vertical: ver
            };
        };
        snapshotStatus = 2;
    }

    this.isFinished = function() {
        var f = true;
        for ( var i=0; i<5; i++ )
        {
            for ( var j=0; j<5; j++ )
                f = f && checked[i][j];
        };
        return f;
    }
 };

 // National Geography game.
var gameNatGeo = function() {
    var points = 0;
    var timeout = null;
    var timeoutInterval = 120;
    var started = false;
    var filled = false;
    var status = 0;

    // Game options.
    var letter = "";
    var state = "";
    var city = "";
    var lake = "";
    var mountain = "";
    var river = "";
    var animal = "";
    var plant = "";
    var band = "";

    this.getPoints = function() {
        return points;
    };

    this.addPoints = function(amount) {
        if ( amount > 0 )
            points += amount;
    }

    this.setTimeout = function(time) {
        timeout = time;
    }

    this.getTimeout = function() {
        return timeout;
    }

    this.setLetter = function(l) {
        if ( filled )
            return;
        letter = l;
        filled = true;
    }

    this.isFilled = function() {
        return filled;
    }

    this.getLetter = function() {
        return letter;
    }

    this.setStatus = function(s) {
        status = s;
    }

    this.getStatus = function() {
        return status;
    }

    this.getProperty = function(name) {
        switch(name) 
        {
        case "state": return state;
        case "city": return city;
        case "lake": return lake;
        case "mountain": return mountain;
        case "river": return river;
        case "animal": return animal;
        case "plant": return plant;
        case "band": return band;
        }
        return null;
    }

    this.setProperty = function(name, value) {
        switch(name) 
        {
        case "state": state = value; break;
        case "city": city = value; break;
        case "lake": lake = value; break;
        case "mountain": mountain = value; break;
        case "river": river = value; break;
        case "animal": animal = value; break;
        case "plant": plant = value; break;
        case "band": band = value; break;
        }        
    }   

    this.asObject = function() {
        return {
            letter: letter,
            state: state,
            city: city,
            lake: lake,
            mountain:mountain,
            river: river,
            animal: animal,
            plant: plant,
            band: band,
            status: status
        };
    }

    this.getTimeoutInterval = function() {
        return timeoutInterval;
    }

    this.startGame = function() {
        if ( started )
            return;

        var date = new Date();
        date.setSeconds( date.getSeconds() + timeoutInterval );
        timeout = date;
        started = true;
    }
 };

 // Trophie game.
var gameTrophie = function() {
    var points = 0;
    var timeout;
    var started = false;
    var timeoutInterval = 30;
    var currentQuestion = 0;

    // Game options.
    var questions = []; 
    var answers = [];
    var correct = [];
    var length = [
        9, 8, 7, 6, 5, 4, 3, 4, 5, 6, 7, 8, 9
    ];
    var finished = false;

    for ( var i=0; i<13; i++ )
        correct[i] = false;

    this.getPoints = function() {
        return points;
    };

    this.addPoints = function(amount) {
        if ( amount > 0 )
            points += amount;
    }

    this.setTimeout = function(time) {
        timeout = time;
    }

    this.getTimeout = function() {
        return timeout;
    }

    this.getQuestion = function(ind) {
        if ( ind<0 || ind>=13 )
            return null;
        return questions[ind];
    }

    this.getAnswer = function(ind) {
        if ( ind<0 || ind>=13 )
            return null;
        return answers[ind];
    }

    this.setQuestion = function(ind, q) {
        if ( ind<0 || ind>=13 )
            return null;
        questions[ind] = q;
    }

    this.setAnswer = function(ind, a) {
        if ( ind<0 || ind>=13 )
            return null;
        answers[ind] = a;
    }

    this.asObject = function() {
        var r = {
            questions: [],
            answers: [],
            correct: correct,
            length: length,
            currentQuestion: currentQuestion,
            finished: finished
        };
        for (var i=0; i<13; i++)
        {
            r.questions[i] = questions[i];
        };
        for (var i=0; i<currentQuestion; i++)
            r.answers[i] = answers[i];
        return r;
    }

    this.startGame = function() {
        if ( started )
            return true;
        var date = new Date();
        date.setSeconds( date.getSeconds() + timeoutInterval );
        timeout = date;
        started = true;
        return false;
    }

    this.getTimeoutInterval = function() {
        return timeoutInterval;
    }

    this.getCurrentQuestion = function() {
        return currentQuestion;
    }

    this.moveToNextQuestion = function() {
        currentQuestion++;
        if ( currentQuestion == 13 )
        {
            finished = true;
        } else
        {
            var date = new Date();
            date.setSeconds( date.getSeconds() + timeoutInterval );
            timeout = date;
        }
    }

    this.testAnswer = function(a) {
        if ( new RegExp("^" + answers[currentQuestion], "i").test(a) )
        {
            points += 5;
            correct[currentQuestion] = true;
            return true;
        };
        correct[currentQuestion] = 4 > 5;
        return false;
    }
 };

 module.exports = {
     gameAnagram: gameAnagram,
     gameNumber: gameNumber,
     gameFxF: gameFxF,
     gameNatGeo: gameNatGeo,
     gameTrophie: gameTrophie
 };