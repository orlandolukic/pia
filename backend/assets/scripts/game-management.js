/**
 * All functionalities for game management.
 */
/**
 * Game management.
 */
var gameLists = require("./game-lists");
var waitingMultiplayerList = require("./game-lists").waitingMultiplayerGames;
var socketList = require("./socket-list").list;
var dbconn = require("./dbconn");

 // Interval in which computer should check if waiting game is ready to proceed.
 var waitingGameInterval = setInterval(function() {
    var obj, blue, red, supervisor;
    for ( waitingMultiplayerList.i_start(); waitingMultiplayerList.i_isNotNull(); waitingMultiplayerList.i_next() )
    {  
        obj = waitingMultiplayerList.i_get();

        try {
            var socketBlue = socketList.getSocket(obj.UserBlue);                

            if ( obj.UserRed != null )
            {
                if ( !obj.notifiedBlueAboutRed )
                {
                    var output = {
                        name: null,
                        surname: null,
                        username: null                  
                    };

                    dbconn.query("users", { username: {
                        $eq: obj.UserRed
                    } }, function(users, len) {
                        if ( len > 0 )
                        {
                            var redDetailed = users[0];
                            output.username = redDetailed.username;
                            output.name = redDetailed.name;
                            output.surname = redDetailed.surname;
                        };  
                        try {                    
                            socketBlue.emit("ready-to-go", { red: true, supervisor: false, redData: output, supervisorData: null });                                              
                        } catch(e) {}
                    }); 
                    obj.notifiedBlueAboutRed = true;                  
                };
            }

            if ( obj.Supervisor != null )
            {                  
                if ( !obj.notifiedBlueAboutSupervisor )
                {
                    socketBlue.emit("ready-to-go", { red: false, supervisor: true });
                    obj.notifiedBlueAboutSupervisor = true;
                };                    
            }
        } catch(e) {
            // Send error signal to red user.                
            //waitingList.deleteAllHosts(obj.UserBlue);
            return;
        };        
        
        // Try to notify red user.
        try {
            if ( !obj.notifiedRedAboutOK && obj.notifiedBlueAboutSupervisor && obj.notifiedBlueAboutRed )
            {
                var socketRed = socketList.getSocket(obj.UserRed);
                socketRed.emit("ready-to-begin", true);
                obj.notifiedRedAboutOK = true;
            };
        } catch(e) {
            // Red user is not responding...
            console.log("Red user not responding...");
        }        
    };

    // Go through the list with waiting singleplayer games and send notify request to supervisor.
    var lst = gameLists.singleplayerGames, elem;
    for ( lst.i_start(); lst.i_isNotNull(); lst.i_next() )
    {
        elem = lst.i_get();
        if ( !elem.started && !elem.supervisor )
        {                 
            try {                               
                socketList.forEach((el) => {
                    // Notify supervisor.                    
                    if ( el.accountType == 1 )
                    {                        
                        el.socket.emit("ctrl-sp-notify-supervisor", elem.username);                        
                    };                                  
                });                
            } catch(e) {
                console.log(e);
            }
        };
    }
    //console.log("==============================================");
 }, 5000);