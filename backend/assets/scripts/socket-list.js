/**
 * List of all sockets in the system.
 * 
 * LstElement
 * {
 *      username: @username
 *      socket:   @socket
 *      next:     @LstElement
 * }
 */
var SocketList = function() {
    var first = null;
    var last = null;
    var temp = null;

    this.i_start = function() { temp = first; }
    this.i_isNotNull = function() { return temp != null;  }
    this.i_get = function() { return temp; }
    this.i_next = function() { temp = temp.next; }

    /**
     * Adds socket in the list for given user.
     */
    this.add = function(username, accountType, socket) 
    {
        var g;
        if ( g = this.find(username) )
        {
            g.socket = socket;
            return false;
        };

        var obj = {
            username: username,
            accountType: accountType,
            socket: socket,
            next: null
        };
        console.log(obj);
        if ( first == null )
            first = last = obj;
        else
            last = last.next = obj;

        return true;
    };

    /**
     * Checks if username exists in the system and returns socket.
     */
    this.find = function(name) 
    {
        var g = first;
        while( g )
        {
            if ( g.username == name )
                return g;
            g = g.next;
        }
        return null;
    }

    /**
     * Gets socket for given user.
     */
    this.getSocket = function(name) 
    {
        var g = first;
        while( g )
        {
            if ( g.username == name )
                return g.socket;
            g = g.next;
        }
        return null;
    }

    /**
     * Executes callback for each element inside the list.
     */
    this.forEach = function(callback)
    {
        var g = first;
        var n, retval;
        while( g )
        {
            n = g.next;
            g.next = null;
            retval = callback(g);                
            g.next = n;
            if ( typeof retval == "boolean" && !retval ) 
                break;
            g = g.next;
        };
    }

    /**
     * Gets username by socket.
     */
    this.getUsernameBySocket = function(socket) 
    {
        var g = first;
        while( g )
        {
            if ( g.socket.id == socket.id )
                return g.username;
            g = g.next;
        }
        return null;
    }

    /**
     * Checks if user is supervisor.
     */
    this.isSupervisor = function(user) 
    {
        var g = first;
        while( g )
        {
            if ( g.username == user )
                return g.accountType == 1;
            g = g.next;
        }
        return false;
    };

    /**
     * Checks if user has active socket connection.
     */
    this.exists = function(name)
    {
        var g = first;
        while( g )
        {
            if ( g.username == name )
                return true;
            g = g.next;
        }
        return false;
    }

    /**
     * Deletes socket for given user.
     */
    this.delete = function(name) 
    {
        var curr = first, p = null;
        while( curr )
        {
            if ( curr.username == name )
            {
                // We delete form the beggining of the list.
                if ( p == null )
                {
                    first = curr.next;
                    if ( first == null )
                        last = null;
                } else
                {
                    p.next = curr.next;
                    if ( last == curr )
                        last = p;
                };
                return true;
            };
            p = curr;
            curr = curr.next;
        }
        return false;
    }

    /**
     * Deletes entry in the list by socket id.
     */
    this.deleteBySocketId = function(id) 
    {
        var curr = first, p = null;
        while( curr )
        {
            if ( curr.socket.id == id )
            {
                // We delete form the beggining of the list.
                if ( p == null )
                {
                    first = curr.next;
                } else
                {
                    p.next = curr.next;
                    if ( last == curr )
                        last = p;
                };
                return true;
            };
            p = curr;
            curr = curr.next;
        }
        return false;
    }
};

// Export "class" socketList.
module.exports = {
    list: new SocketList()
};