/**
 * File for socket management.
 */
var handlers = require("./socket-handlers");


// Start server and await connection!
const io = require('socket.io').listen(8081);
io.on('connection', client => {

    // MUTUAL REQUESTS
    client.on("pass", data => { handlers.mutual.passDataToDestination(client, data); });
    client.on("get-data-about-user", data => { handlers.mutual.getDataAboutUser(client, data); });
    client.on("supervisor-get-data-about-supervision", data => { handlers.mutual.getDataAboutSupervision(client, data); });
    client.on("supervisor-get-game-status", data => { handlers.mutual.getGameStatusForSupervisor(client, data); })
    client.on("get-game-status", data => { handlers.mutual.getGameStatus(client, data); });
    client.on("sp-points-review", data => { handlers.mutual.reviewPoints(client, data); });
    
    // Hello request.    
    client.on('hello', (data, accountType) => { handlers.mutual.hello(client, data, accountType); });

    // In-between requests.
    client.on('terminate-game-for-me', data => { handlers.mutual.terminateGameForMe(client); });

    // Signs in red contestant.
    client.on("sign-in-red-contestant", data => { handlers.mutual.signInRedContestant(client, data); });

    // Creates multiplayer game.
    client.on("create-multiplayer-game-for", data => { handlers.mutual.createGameFor(client, data) });
    client.on("decline-game-for-host", data => { handlers.mutual.declineGameFor(client, data) });
    client.on("can-sign-out", data => { handlers.mutual.canSignOut(client, data); });

    // Terminating request.
    client.on('disconnect', () => { handlers.mutual.terminateGameForMe(client); });

    // SINGLEPLAYER REQUESTS
    client.on("create-singleplayer-game-for", data => { handlers.singleplayer.createGame(client, data); })
    client.on("decline-singleplayer-game-for", data => { handlers.singleplayer.declineGame(client, data); })
    client.on("supervisor-sp-get-data-about-user", data => { handlers.singleplayer.getDataForUser(client, data); });
    client.on("supervisor-sp-signed-in", data => { handlers.singleplayer.registerSupervisor(client, data); });
    client.on("supervisor-sp-finish", data => { handlers.singleplayer.finishSupervision(client, data); });
    client.on("begin-game", data => { handlers.singleplayer.beginGame(client, data); });    
    client.on("sp-get-data-about-game", data => { handlers.singleplayer.initGameInfo(client, data); });
    client.on("sp-terminate-game", data => { handlers.singleplayer.terminateGame(client, data); });

    // ANAGRAM GAME
    client.on("sp-anagram-game-info", data => { handlers.singleplayer.anagram.init(client, data); });
    client.on("sp-anagram-finished", data => { handlers.singleplayer.anagram.finished(client, data); });
    // NUMBER GAME
    client.on("sp-number-game-info", data => { handlers.singleplayer.number.init(client, data); });
    client.on("sp-number-stop-spinning", data => { handlers.singleplayer.number.stopSpinning(client, data); });
    client.on("sp-number-finish-game", data => { handlers.singleplayer.number.finished(client, data); });
    // 5x5 GAME
    client.on("sp-fxf-game-info", data => { handlers.singleplayer.fxf.init(client, data); });
    client.on("sp-fxf-letter-pressed", data => { handlers.singleplayer.fxf.letterPressed(client, data); });
    client.on("sp-fxf-finish-game", data => { handlers.singleplayer.fxf.finished(client, data); });
    client.on("sp-fxf-release-all-table", data => { handlers.singleplayer.fxf.releaseAllTable(client, data); });
    // NATGEO GAME
    client.on("sp-natgeo-game-info", data => { handlers.singleplayer.natgeo.init(client, data); });
    client.on("sp-natgeo-save-input", data => { handlers.singleplayer.natgeo.save(client, data); });
    client.on("sp-natgeo-supervise-terms", data => { handlers.singleplayer.natgeo.superviseTerms(client, data); });
    client.on("sp-natgeo-finish-game", data => { handlers.singleplayer.natgeo.finished(client, data); });
    // TROPHIE GAME
    client.on("sp-trophie-game-info", data => { handlers.singleplayer.trophie.init(client, data); });
    client.on("sp-trophie-submit-answer", data => { handlers.singleplayer.trophie.submitAnswer(client, data); });


});

// On every 2s check if some user is not active.
var int = setInterval(function() {
    //console.log("tick");
}, 2000);

// Exports.
module.exports = {
    io: io
};