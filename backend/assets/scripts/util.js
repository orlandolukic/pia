/**
 * 
 * Util functions.
 */
var dbconn = require("./dbconn");

var functions = {

    isAdministrator: function(req) {
        return !(!req.session || req.session && req.session.username == undefined ||  req.session && req.session.accountType != 2);
    },

    isUserSupervisor: function(req) {
        return !(!req.session || req.session && req.session.username == undefined ||  req.session && req.session.accountType != 1);
    },

    isUser: function(req) {
        return !(!req.session || req.session && req.session.username == undefined ||  req.session && req.session.accountType != 0);
    },

    genAlphabetObject: function(charA, charZ)
    {
        var a = {}, i = charA.charCodeAt(0), j = charZ.charCodeAt(0), v;
        for (; i <= j; ++i) 
        {
          v = String.fromCharCode(i);
          switch( v )
          {
          case "W": 
          case "X":
          case "Y":
            break;
  
          default:
              a[v] = false;
              break;
          };          
        };
        
            a['Ć'] = false;
            a['Š'] = false;
            a['Đ'] = false;
            a['Ž'] = false;
            a['Č'] = false;       
        return a;
    },

    genAlphabetArray: function(charA, charZ, w)
    {
        var a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0), v;
        for (; i <= j; ++i) 
        {
          v = String.fromCharCode(i);
          switch( v )
          {
          case "W": case "X": case "Y": break;  
          default:
              a.push(v);
              break;
          };          
        };   
        if ( w )
        {     
            a.push("Ć");
            a.push("Š");
            a.push("Đ");
            a.push("Ž");
            a.push("Č");      
        };
        return a;
    },

    appendTermsInTheDatabse: function(terms) {
        for (var i=0; i<terms.length; i++)
        {
            if ( terms[i].checked )
            {
                dbconn.insert("words", {
                    word: terms[i].value,
                    category: terms[i].category,
                    letter: terms[i].value.charAt(0).toUpperCase()
                }, (resp) => {});
            };
        };
    },

    formatDate: function(date) {
        date = new Date(date);
        var days = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        days = days < 10 ? "0" + days : days;
        month = month < 10 ? "0" + month : month;
        return days + "." + month + "." + year + ".";
    }
};

module.exports = functions;