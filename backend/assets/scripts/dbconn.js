/**
 * MongoDB connection settings.
 * ===============================================
 * Database connection functions.
 */

var opt = {
    username: "admin",
    password: "administrator",
    database: "db1",
    db: null,
    mongodb: require('mongodb').MongoClient,
    settings: { useNewUrlParser: true, useUnifiedTopology: true },
    DEBUG: true,
    collection: null,
    query: null, 
    isConnected: false,
    depth: 0,
    operation: "",
    operationData: {
        search: {
            query: {}
        },
        insert: {
            
        },
        delete: {},
        update: {}
    },

    getURI: function() {
        return "mongodb+srv://" + this.username + ":" + this.password + "@lid20-1au2a.mongodb.net/" + this.database + "?retryWrites=true&w=majority";
    },

    setQueryParameter: function(param) {
        if ( this.operation === "search" )
            this.operationData.search.query = param.query;
        else if ( this.operation === "insert" )
            this.operationData.insert.document = param.document;
        else if ( this.operation === "delete" )
            this.operationData.delete.by = param.by;
        else if ( this.operation === "update" )
        {
            this.operationData.update.query = param.query;
            this.operationData.update.newobj = param.newobj;
            this.operationData.update.many = param.many;
        };
    },

    getCollection: function(collection) {
        return this.db.db(this.database).collection(collection);
    },

    disconnect: function() {
        var dis = false;
        if ( this.db )
        {
            try {
                this.db.close();
                dis = true;
            } catch(e) { dis = false; }
            this.db = null;
        };
        this.collection = null;
        this.query = null;
        this.isConnected = false;
        if ( this.DEBUG && dis )
            console.log("*** Database disconnected ***");
    },

    connect: function( request, callback ) {
        if ( this.isConnected )
        {
            if ( request.operation === "search" )
                this.search( request, callback );
            else if ( request.operation === "insert" )
                this.insert( request, callback );
            else if ( request.operation === "delete" )
                this.delete( request, callback );
            else if ( request.operation === "update" )      
                this.update( request, callback );
            else if ( request.operation === "aggregate" )
                this.aggregate( request, callback );
            return;
        };

        var that = this;
        this.depth = 0;

        this.mongodb.connect(this.getURI(), this.settings, function(err, db) {
            if (err) 
            {
                console.log(err);
                throw err;
            };


            that.db = db;
            that.isConnected = true;

            if ( that.DEBUG )
                console.log("*** Database connected ***");
    
            // Check if request exists.
            if ( request )
            {
                // Perform operation!
                if ( request.operation === "search" )
                    that.search( request, callback );
                else if ( request.operation === "insert" )
                    that.insert( request, callback );
                else if ( request.operation === "delete" )
                    that.delete( request, callback );      
                else if ( request.operation === "update" )      
                    that.update( request, callback );
                else if ( request.operation === "aggregate" )
                    that.aggregate( request, callback );
            }
        });
    },

    search: function( request, callback ) {
        //console.log("search");
        //console.log(this.operationData);
        var v = this;
        var g = null;
        try {
            try {
                g = this.db.db(this.database).collection(request.collection).find(request.query, request.project);
                if ( request.sort != null )
                    g = g.sort(request.sort);
                //g.toArray();
            } catch(e) { 
                v.depth++; 
                throw e; 
            };
            g.toArray(function(err, results) {
                if (err) 
                {
                    v.depth++;
                    throw err;
                };
                //console.log(results);
                callback( results, results.length );
                //that.disconnect();
            });
        } catch(err)
        {
            if ( v.depth > 1 )
            {
                this.disconnect();
                throw new Error("Could not connect to MongoDB.");
            }
            this.disconnect();
            this.connect( request, callback );
            this.depth = 0;
        };
    },

    insert: function ( request, callback ) {
        this.db.db(this.database).collection(request.collection)
            .insertOne(request.document, function(err, task) {
                if ( err )
                    throw err;
                //res.json(task);
                callback(task);
            });
    },

    aggregate: function( request, callback ) {
        var cursor = this.db.db(this.database).collection(request.collection)
        .aggregate(request.array, function(err, result) {
            callback(err, result);        
        });
        
    },

    delete : function( request, callback ) {
        this.db.db(this.database).collection(request.collection)
            .deleteMany(request.by, function(err, task) {
                if ( err )
                    throw err;
                //res.json(task);
                callback(task);
            });
    },

    update : function( request, callback ) {
        var collection = this.db.db(this.database).collection(request.collection);
        if ( request.many )
            collection.updateMany(request.query, request.newobj, {}, function(err, updatedNum, status) {
                if ( err )
                    throw err;
                callback(err, updatedNum, status);
            });
        else 
        {
            collection.updateOne(request.query, request.newobj,
            { 
                upsert: false 
            }, function(err, updatedNum, status) {
                if ( err )
                    throw err;
                callback(err, updatedNum, status);
            });
        };
    },

    setCollectionName: function( collection ) {
        this.collection = collection;
    }
};

/**
 * Database exported variable.
 */
var DBCONN = {

    /**
     * Initializes database connection.
     */
    init : function() {
        opt.connect(null, null);
    },

    /**
     * Queries data with specific object property in certain collection.
     * 
     * @param {string} collection 
     * @param {Object} query 
     * @param {Function} callback Function to execute when caught data.`
     * @param {any} sort Sorting mechanizm. null to escape. 
     */
    query: function( collection, query, callback, sort = null, project = null ) {
        opt.connect( { operation: "search", collection: collection, query: query, sort: sort, project: project == null ? {} : project }, callback );
    },

    /**
     * Gets all data from the collection.
     * 
     * @param {string} collection Collection to search.
     * @param {Function} callback Function to execute when caught data.
     */
    getAll: function( collection, callback ) {
        opt.connect( { operation: "search", collection: collection, query: null }, callback );
    },

    /**
     * Inserts new document into database.
     * 
     * @param {string} collection Collection name.
     * @param {any} document Any object to store in database.
     * @param {Function} callback Callback function to execute upon insert proccess.
     */
    insert : function( collection, document, callback ) {
        opt.connect ( { operation: "insert", collection: collection, document: document }, callback );
    },

    /**
     * 
     * @param {string} collection Collection in which delete action is made.
     * @param {any} obj Object used for delete process.
     * @param {Function} callback Callable to be executed upon completion of delete request.
     */
    delete : function( collection, obj, callback ) {
        opt.connect( { by: obj, collection: collection, operation: "delete" }, callback );
    },

    /**
     * Updates current row in database.
     * 
     * @param {string} collection Collection name.
     * @param {any} query Query to lookup.
     * @param {any} newobj New object to override old one.
     * @param {Function} callback Function to execute on completion of this request.
     * @param {boolean} If this request should modify one or many documents in database.
     */
    update : function( collection, query, newobj, callback, many = false ) {
        opt.connect( { operation: "update", collection: collection, query: query, newobj: newobj, many: many }, callback );
    },

    /**
     * Makes inner join request.
     * 
     * @param {string} collection Collection name.
     * @param {array} arr Array of aggregate objects.
     * @param {Function} callback Callback function on database finish.
     */
    aggregate : function( collection, arr, callback ) {
        opt.connect( { operation: "aggregate", collection: collection, array: arr }, callback );
    },

    /**
     * Gets collection for given param.
     *  
     * @param {string} collection Name of the collection. 
     */
    getCollection: function( collection ) {
        return opt.getCollection(collection);
    }
};

module.exports = DBCONN;