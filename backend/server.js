var express = require("express");
var path = require("path");
var parser = require("body-parser");
var cors = require('cors');
var session = require("express-session");
var MemoryStore = require("memorystore")(session);

// Include files.
var mngr = require("./assets/scripts/socket-management");
var gameMngr = require("./assets/scripts/game-management");
var user = require("./routes/user");
var supervisor = require("./routes/supervisor");
var administrator = require("./routes/administrator");
var ord = require("./routes/ord");
//ar tasks = require("./routes/tasks.js");
var dbconn = require("./assets/scripts/dbconn");

var app = express();
var port = 8080;
var production = false;

// Allow cors!
app.use(cors({origin: true, credentials: true}));

// Allow session for users.
app.use(session({
    name: "sessid",
    secret: 'q?hgq9PyPhbwfv@eqv_41_E64pg0sZ',
    resave: false,
    saveUninitialized: true,
    store: new MemoryStore({
        checkPeriod: 86400000 // prune expired entries every 24h
    }),
    cookie: { 
        secure: false,
        path: "/",
        maxAge: 1000 * 60 * 60 * 4    // 4 hours.
    }
}));

// Set application data.
app.set( 'views', path.join( __dirname, 'views') );
app.set( 'view engine', 'ejs' );
app.set( 'env', process.env.NODE_ENV );
app.engine('html', require('ejs').renderFile);

// Set static folder
// Angular stuff
// Set content folder!
app.use( express.static( path.join( __dirname, 'client') ) );
app.use( '/web-content', express.static( path.join( __dirname, 'assets' ) ) );

// Body parser
app.use( parser.json() );
app.use( parser.urlencoded({ extended: false }) );

// Account type requests.
app.use( '/user', user );
app.use( '/supervisor', supervisor );
app.use( '/admin', administrator );
app.use( '/ord', ord );

// Add not-found page.
app.route('/not-found').get(function(req, res) {
    res.send('404 not found');
});

app.listen(port, (req, res) => {
    console.log("Server successfully started on " + port + ".");
    dbconn.init();
});

/*
// Handle invalid requests!
app.route('*').get(function(req, res) {
    res.redirect('/not-found');
});
app.route('*').post(function(req, res) {
    res.redirect('/not-found');
});
*/

module.exports = {
    production: production
};

