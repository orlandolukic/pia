import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuUnregisteredComponent } from './menu-unregistered.component';

describe('MenuUnregisteredComponent', () => {
  let component: MenuUnregisteredComponent;
  let fixture: ComponentFixture<MenuUnregisteredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuUnregisteredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuUnregisteredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
