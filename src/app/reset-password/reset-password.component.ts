import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { Globals } from 'src/assets/code/globals';
import { Router } from '@angular/router';
import { Server } from 'src/assets/code/http-request';
import { sha256 } from 'js-sha256';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit, AfterViewInit, OnDestroy
{
  private usernameText: string;
  private oldPasswordText: string;
  private newPasswordText: string;
  private newPasswordVerifyText: string;

  private inCheck: boolean;
  private errors: Array<boolean>;
  private loaded: boolean;
  private onSuccess: boolean;
  private timer: number;
  private onError: boolean;
  private errorMessage: string;
  private errorTimeout: any;
  private successInterval: any;
  //private inRequest: boolean;

  private errorNum: number = 6;
  private ERROR_INVALID_USERNAME: number = 0;
  private ERROR_OLDPASSWORD_INVALID: number = 1;
  private ERROR_NEWPASSWORD_CRITERIA: number = 2;
  private ERROR_NEWPASSWORD_MATCH: number = 3;
  private ERROR_EMPTY_FIELDS: number = 4;
  private ERROR_USERNAME_PARSE: number = 5;

  @ViewChild("username", {static: true, read: MatInput}) field_username : MatInput;

  constructor( private globals: Globals, private router: Router, private server: Server ) 
  {
    this.loaded = false;
    this.inCheck = false;
    this.onSuccess = false;
    this.timer = 0;
    this.onError = false;
    this.errorMessage = null;
  }

  ngOnInit() 
  {
    this.globals.renameDocumentTitle("Promena lozinke");
    this.inCheck = false;
    this.errors = new Array<boolean>();
    for (var i=0; i<this.errorNum; i++)
      this.errors[i] = false;
  }

  ngAfterViewInit(): void {
    this.loaded = true;
  }

  ngOnDestroy()
  {
    clearInterval(this.successInterval);
    clearInterval(this.errorTimeout);
  }

  /**
   * Checks if form is valid.
   */
  public isValidFormStatic() : boolean
  {
    var g = false;
    for (var i=0; i<this.errorNum; i++)
      g = g || this.errors[i];

    return !g;
  }

  /**
   * Tries to change password.
   * 
   * @param event onSubmit event.
   */
  public attemptSubmit(event: Event) : void
  {
    event.preventDefault();

    this.errors[this.ERROR_EMPTY_FIELDS] = this.usernameText === undefined || this.oldPasswordText === undefined || this.newPasswordText === undefined || this.newPasswordVerifyText === undefined;

    if ( this.errors[this.ERROR_EMPTY_FIELDS] )
      return;

    this.errors[this.ERROR_USERNAME_PARSE] = !new RegExp(/^[a-zA-Z_][a-zA-Z0-9_]+$/).test(this.usernameText);
    this.errors[this.ERROR_NEWPASSWORD_MATCH] = this.newPasswordText !== this.newPasswordVerifyText;
    this.errors[this.ERROR_NEWPASSWORD_CRITERIA] = !(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/.test(this.newPasswordText)) || this.newPasswordText.length > 12 || this.newPasswordText.length < 8;
    this.errors[this.ERROR_OLDPASSWORD_INVALID]  = !(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/.test(this.oldPasswordText)) || this.oldPasswordText.length > 12 || this.oldPasswordText.length < 8;

    if ( this.isValidFormStatic() )
    {
      var obj = this;
      var interval = null;
      var data: any = {};

      this.inCheck = true;
      this.onSuccess = false;
      this.onError = false;
      clearTimeout(this.errorTimeout);

      data.username = this.usernameText;
      data.oldpwd = sha256(this.oldPasswordText);
      data.newpwd = sha256(this.newPasswordText);

      obj.server.request('post', 'user/change-password', data, function(resp) {
        obj.onError = !resp.userExists || !resp.changed;
        if ( !resp.changed )
          obj.errorMessage = "Lozinka za korisnika " + obj.usernameText + " nije ispravna.";
        if ( !resp.userExists )
          obj.errorMessage = "Korisnik ne postoji u sistemu.";

        obj.onSuccess = !obj.onError;
        obj.inCheck = false;

        if ( obj.onSuccess )
        {
          obj.timer = 5;
          obj.successInterval = setInterval(function() {
            if ( obj.timer === 0 )
            {
              clearInterval(obj.successInterval);
              obj.router.navigate(['/']);
              return;
            };
            obj.timer--;
          }, 1000);
        } else
        {
          setTimeout(function() {
            obj.field_username.focus();
          }, 500);

          obj.errorTimeout = setTimeout(function() {
            obj.onError = false;
            clearTimeout(obj.errorTimeout);
          }, 5000);
        }
      });

      // Make request to server.
    };
  }

}
