import { Component, OnInit } from '@angular/core';
import { RangList } from 'src/assets/code/user/rang-list';
import { Server } from 'src/assets/code/http-request';
import { utils } from 'protractor';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  loadedLast20: boolean;
  loadedMonth: boolean;
  displayedColumns: string[] = ['rang', 'name', 'date', 'points'];
  dataSource: Array<RangList>;
  dataSourceMonth: Array<RangList>;

  constructor(
    private server: Server
  ) { 
    this.loadedLast20 = false;
    this.loadedMonth = false;
  }

  private formatDate(d: string): string {
    var date: Date = new Date(d);
    var days: number = date.getDate();
    var month: number = date.getMonth() + 1;
    var year = date.getFullYear();
    var dd: string, dm: string;
    dd = days < 10 ? "0" + days : "" + days;
    dm = month < 10 ? "0" + month : "" + month;
    return dd + "." + dm + "." + year + ".";
  }

  ngOnInit() {
    this.server.requestAuth("post", "ord/get-results", {}, (resp: any) => {
      var last20: Array<any> = resp.last20;
      this.dataSource = new Array<RangList>();
      for (var i=0; i<last20.length; i++)
      {
        this.dataSource[i] = {
          date: this.formatDate( last20[i].date ),
          fullname: last20[i].userData[0].name + " " + last20[i].userData[0].surname,
          image: last20[i].userData[0].image,
          points: last20[i].total,
          rang: last20[i].rang,
          username: last20[i].username
        };
      };
      this.loadedLast20 = true;

      var month: Array<any> = resp.month;
      this.dataSourceMonth = new Array<RangList>();
      for (var i=0; i<month.length; i++)
      {
        this.dataSourceMonth[i] = {
          date: this.formatDate( month[i].date ),
          fullname: month[i].userData[0].name + " " + month[i].userData[0].surname,
          image: month[i].userData[0].image,
          points: month[i].total,
          rang: month[i].rang,
          username: month[i].username
        };
      };
      this.loadedMonth = true;
      
    }, (err: any) => {
      alert("Dogodila se greška!")
    });
  }

}
