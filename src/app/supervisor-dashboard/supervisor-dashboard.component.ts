import { Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit, ViewContainerRef, ComponentFactoryResolver, OnDestroy, EventEmitter } from '@angular/core';
import { Globals } from 'src/assets/code/globals';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Server } from 'src/assets/code/http-request';
import { MatDialog, MatInput } from '@angular/material';
import { ModalAddNewAnagramsComponent } from 'src/assets/components/modals/modal-add-new-anagram/modal-add-new-anagram';
import { SelectionModel } from '@angular/cdk/collections';
import { List5x5Games } from 'src/assets/code/supervisor/list-5x5-games';
import { TrophieHandling } from 'src/assets/code/supervisor/trophie-handling';
import { NatGeoComponent } from 'src/assets/components/nat-geo/nat-geo.component';
import { Supervise } from 'src/assets/code/supervisor/supervise';

export interface AnagramData {
  position: number;
  title: string;
  result: number;
}

@Component({
  selector: 'app-supervisor-dashboard',
  templateUrl: './supervisor-dashboard.component.html',
  styleUrls: ['./supervisor-dashboard.component.css']
})
export class SupervisorDashboardComponent implements OnInit, AfterViewInit, OnDestroy {

  private anagramsLoading: boolean;
  private mode: number;
  private anagramsSelection: SelectionModel<AnagramData>;
  private selected: boolean;
  private listOf5x5Games: List5x5Games;
  private trophieGame: TrophieHandling;

  displayedColumns: string[] = ['select', 'position', 'title', 'result'];
  dataSource: MatTableDataSource<AnagramData>;


  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild("anagramsTable", {static: true, read: MatTable}) anagramsTable: MatTable<any>;
  @ViewChild("anagramsSearch", {static: false, read: MatInput}) anagramsSearch: MatInput;
  @ViewChild("gamesPlaceholder", {static: false, read: ViewContainerRef}) gamesPlaceholder: ViewContainerRef;
  @ViewChild(NatGeoComponent, {static: true}) childWordsComponent: NatGeoComponent;

  constructor( 
    private globals: Globals, 
    private changeRef: ChangeDetectorRef, 
    private server: Server,
    private dialog: MatDialog,
    private cfr: ComponentFactoryResolver,
    private cdr: ChangeDetectorRef,
    private supervise: Supervise
  ) 
  {
    this.anagramsLoading = true;
    this.globals.renameDocumentTitle("Supervizor sekcija");

    const initialSelection = [];
    const allowMultiSelect = true;
    this.anagramsSelection = new SelectionModel<AnagramData>(allowMultiSelect, initialSelection);

    // Creates game.
    this.listOf5x5Games = new List5x5Games(cfr, server, cdr, dialog);

    // Creates trophie handling.
    this.trophieGame = new TrophieHandling( this.server, dialog );
  }

  ngOnInit() {
    this.mode = -1;
    this.listOf5x5Games.ngOnInit();
    this.changeRef.reattach();
  }

  ngAfterViewInit()
  {    
    this.refreshAnagramTable();
    this.listOf5x5Games.ngAfterViewInit();    
    this.listOf5x5Games.gamesPlaceholder = this.gamesPlaceholder;

    this.globals.getLogoutObservable().subscribe(() => {   
      this.supervise.cleanUp();
    });

    // Prepare socket connection.
    this.supervise.prepare();
  }

  ngOnDestroy()
  {
    this.listOf5x5Games.ngOnDestroy();
    this.changeRef.detach();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.anagramsSelection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if ( !this.dataSource )
      return;
    this.isAllSelected() ?
        this.anagramsSelection.clear() :
        this.dataSource.data.forEach(row => this.anagramsSelection.select(row));
  }

  private refreshAnagramTable(): void
  {
    this.server.requestAuth("post", "supervisor/list-anagrams", {}, resp => {
      this.dataSource = new MatTableDataSource<AnagramData>(resp.anagrams);      
      this.dataSource.paginator = this.paginator;
      this.anagramsLoading = false;
    }, err => {
      window.location.reload();
    });
  }

  public getFullName(): string
  {
    if ( this.globals.getUser() == null )
      return "";
    var u = this.globals.getUser().getData();
    return u.name + " " + u.surname;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * Opens form for adding new anagram.
   */
  public addAnagram(): void
  {
    this.dialog.open(ModalAddNewAnagramsComponent, {
      backdropClass: "backdrop",
      autoFocus: true,
      disableClose: true,
      width: "350px"
    }).afterClosed().subscribe((result) => {
      if ( result > 0 )
      {
        this.anagramsLoading = true;
        this.refreshAnagramTable();
      };
    });
  }


  public indexChanged(event: any)
  {
    if ( event == 1 )
    {
      this.listOf5x5Games.gamesPlaceholder.clear();
      this.listOf5x5Games.loaded = false;
      this.listOf5x5Games.get5x5Games();
    } else if ( event == 2 )
    {
      this.childWordsComponent.fetchWords();
    } else if ( event == 3 )
    {
      this.trophieGame.fetchTrophies();
    };
  }

  /**
   * Removes anagrams from the table.
   */
  public removeAnagrams(): void
  {
    var selected: AnagramData[] = this.anagramsSelection.selected;
    var x = confirm("Da li ste sigurni da brisemo anagram(e)?");
    if ( x )
    {
      this.server.requestAuth("post", "supervisor/remove-anagrams", { anagrams: selected }, resp => {
        this.anagramsSelection.clear();
        this.anagramsSearch.value = "";
        this.refreshAnagramTable();
      }, err => {
        window.location.reload();
      });
    }
  }

}
