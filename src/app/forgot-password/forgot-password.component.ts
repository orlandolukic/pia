import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { Globals } from 'src/assets/code/globals';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { MatInput } from '@angular/material';
import { Server } from 'src/assets/code/http-request';
import { sha256 } from 'js-sha256';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy, AfterViewInit
{
  
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  inProcess: boolean;
  isImportantToStay: boolean;
  submitted: boolean;
  private usernameText: string;
  private jmbgText: string;
  private active: number;
  private question: string;
  private timer: number;

  private onError: boolean;
  private onSuccessSubmit: boolean;
  private onErrorSubmit: boolean;
  private onErrorTimeout: any;
  private onSuccessInterval: any;
  private errors: Array<boolean>;
  private errorType: number;
  private errorMessage: string;
  private user:any;
  private forceDisabled: boolean;
  private errorNum: number = 4;
  private ERROR_EMPTY_FIELDS: number = 0;
  private ERROR_JMBG_FORMAT: number = 1;
  private ERROR_PASSWORD_FORMAT: number = 2;
  private ERRPR_PASSWORD_MATCH: number = 3;

  @ViewChild('stepper', {static: false, read: MatHorizontalStepper}) stepperObj: MatHorizontalStepper;
  @ViewChild('username', {static: false, read: MatInput}) username: MatInput;
  @ViewChild('jmbg', {static: false, read: MatInput}) jmbg: MatInput;
  @ViewChild('answer', {static: false, read: MatInput}) answer: MatInput;
  @ViewChild('password', {static: false, read: MatInput}) password: MatInput;
  @ViewChild('passwordAgain', {static: false, read: MatInput}) passwordAgain: MatInput;

  constructor(private globals : Globals, private _formBuilder: FormBuilder, private server: Server, private router: Router) 
  {
    this.globals.renameDocumentTitle("Zaboravljena lozinka");
    this.inProcess = false;
    this.onSuccessSubmit = false;
    this.onErrorSubmit = false;
    this.onError = false;
    this.onErrorTimeout = null;
    this.errorType = -1;
    this.forceDisabled = false;
    this.errors = new Array<boolean>();
    this.errorMessage = null;
    this.active = 1;
    this.timer = 0;
    this.onSuccessInterval = null;
    for ( var i=0; i<this.errorNum; i++ )
      this.errors[i] = false;
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({});
    this.secondFormGroup = this._formBuilder.group({});
  }

  ngOnDestroy()
  {
    clearTimeout(this.onErrorTimeout);
  }

  ngAfterViewInit()
  {
    var obj = this;
    setTimeout(function() { 
      obj.username.focus();
    }, 200);
  }

  /**
   * Submits certain part of the global form.
   *  
   * @param part Part of the form.
   */
  public submitPart(event: Event, part: number) : void
  {
    event.preventDefault();
    if ( part == 1 )
      this.handleFirst();
    else if ( part == 2 )
      this.handleSecond();
    else if ( part == 3 )
      this.handleThird();
  }

  /**
   * Checks if this form has errors.
   */
  public hasErrors(): boolean
  {
    var e = false;
    for (var i=0; i<this.errorNum; i++)
      e = e || this.errors[i];
    return e;
  }

  public handleFirst() : void
  {
    var username = this.username.value;
    var jmbg = this.jmbg.value;

    this.errors[this.ERROR_EMPTY_FIELDS] = username === "" || jmbg === "";
    if ( !this.errors[this.ERROR_EMPTY_FIELDS] )
    {
      var reg_jmbg: RegExp = new RegExp(/((0[1-9]|[1-2][0-9]|3[0-1])(0(1|3|5|7|8)|1(0|2))|(0[1-9]|[1-2][0-9]|30)(0(2|4|6|9)|11)|(0[1-9]|[1-2][0-8])02)(9[0-9]{2}|0[0-9]{2})[0-9]{2}[0-9]{3}[0-9]/);
      this.errors[this.ERROR_JMBG_FORMAT] = !reg_jmbg.test(jmbg);
    };

    // Set error/success variables.
    this.onError = this.hasErrors();

    // Change step if user typed everything ok.
    if ( !this.onError )    
    {      
      this.inProcess = true;
      var obj = this;
      // Go and check this user...
      this.server.request('post', 'user/recover-account-1', { username: username, jmbg: jmbg }, function(resp) {
        obj.inProcess = false;
        obj.user = {};
        if ( resp.exists )
        {
          obj.user.username = username;
          obj.user.jmbg = jmbg;
          obj.question = resp.question;
          obj.onErrorSubmit = false;
          clearTimeout(obj.onErrorTimeout);
          obj.stepperObj.next();
        } else
        {
          obj.username.focus();
          obj.user.username = username;
          obj.user.jmbg = jmbg;
          obj.errorType = 0;
          obj.errorMessage = "Korisnik {" + username + " :: " + jmbg + "} sistemu.";
          clearTimeout(obj.onErrorTimeout);
          obj.onErrorTimeout = setTimeout(function() {
            obj.onErrorSubmit = false;
            clearTimeout(obj.onErrorTimeout);
          }, 5000);           
        };
        obj.onErrorSubmit = !resp.exists;
        obj.onSuccessSubmit = false;
      });
      
    };
  }

  /**
   * Handle question & answer.
   */
  public handleSecond() : void
  {
    var answer = this.answer.value;

    this.errors[this.ERROR_EMPTY_FIELDS] = answer === "";
    this.onError = this.hasErrors();
    var obj = this;

    if ( !this.onError )
    {
      this.inProcess = true;
      this.server.request('post', 'user/recover-account-2', {username: this.user.username, answer: sha256(answer)}, function(resp) {
        obj.inProcess = false;
        if ( resp.confirmed )
        {
          obj.onErrorSubmit = false;
          clearTimeout(obj.onErrorTimeout);
          obj.stepperObj.next();
        } else
        {
          setTimeout(function() {
            obj.answer.focus();
          }, 100);
          clearTimeout(obj.onErrorTimeout);
          obj.onErrorTimeout = setTimeout(function() {
            obj.router.navigate(['/']);
          }, 3000);  
          obj.forceDisabled = true;
          obj.errorType = 1;
          obj.errorMessage = "Netačan odgovor. Preusmeravanje...";
          obj.onErrorSubmit = true;
        }
      });
    };
  }

  public handleThird() : void
  {
    var password = this.password.value;
    var pswconf = this.passwordAgain.value;
    
    this.errors[this.ERROR_EMPTY_FIELDS] = password === "" || pswconf === "";
    if ( !this.errors[this.ERROR_EMPTY_FIELDS] )
    {
      this.errors[this.ERROR_PASSWORD_FORMAT] = !(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/.test(password)) || password.length > 12 || password.length < 8;
      this.errors[this.ERRPR_PASSWORD_MATCH] = password !== pswconf;
    };
    this.onError = this.hasErrors();

    if ( !this.onError )
    {
      var obj = this;
      this.isImportantToStay = true;
      this.inProcess = true;
      this.server.request('post', "user/recover-account-3", {username: this.user.username, password: sha256(password)}, function(resp) {
        obj.inProcess = false;  
        obj.forceDisabled = true;      
        if ( resp.changed )
        {
          obj.onSuccessSubmit = true;
          obj.isImportantToStay = false;
          obj.timer = 5;
          clearInterval(obj.onSuccessInterval);
          obj.onSuccessInterval = setInterval(function() {
            if ( obj.timer == 0 )
            {              
              clearInterval(obj.onSuccessInterval);
              obj.router.navigate(['/']);
              return;
            };
            obj.timer--;
          }, 1000);
        } else
        {
          clearTimeout(obj.onErrorTimeout);
          obj.onErrorTimeout = setTimeout(function() {
            obj.router.navigate(['/']);
          }, 3000);  
          obj.forceDisabled = true;
          obj.errorType = 1;
          obj.errorMessage = "Dogodila se greška. Molimo pokušajte kasnije. Preusmeravanje...";
          obj.onErrorSubmit = true;
        }
      });  
    };
  }

  public changeStep(event: StepperSelectionEvent): void
  {
    for ( var i=0; i<this.errorNum; i++ )
      this.errors[i] = false;
    this.active++;
  }

  public focus(): void
  {
    if ( this.active == 2 )
      this.answer.focus();
    else if ( this.active == 3 )
      this.password.focus();
  }

}
