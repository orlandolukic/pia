import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { Globals } from 'src/assets/code/globals';
import { MatExpansionPanel, MatButton, MatRadioButton, MatDialog } from '@angular/material';
import { RegistrationPanel } from 'src/assets/code/registration/registration-panel';
import { BasicRegistrationPanel } from 'src/assets/code/registration/basic-registration-panel';
import { Registration } from 'src/assets/code/registration/registration';
import { AccountRegistrationPanel } from 'src/assets/code/registration/account-registration-panel';
import { Server } from 'src/assets/code/http-request';
import { UploadRegistrationPanel } from 'src/assets/code/registration/upload-registration-panel';
import { SecurityRegistrationPanel } from 'src/assets/code/registration/security-registration-panel';
import { DialogOverviewExampleDialog } from 'src/assets/code/registration/modal-registration';
import { DialogData } from 'src/assets/code/registration/registration';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, AfterViewInit
{
  private isReady : boolean = false;
  private loaded : boolean = false;
  private regop : Registration;

  // Panel : basic
  @ViewChild('panel_basic', {static: false, read: MatExpansionPanel}) panel_basic: MatExpansionPanel;
  @ViewChild('name', { static: false, read: MatInput }) panel_basic_name:MatInput;
  @ViewChild('surname', { static: false, read: MatInput }) panel_basic_surname : MatInput;
  @ViewChild('panel_basic_submit', { static: false, read: MatButton }) panel_basic_submit : MatButton;
  @ViewChild('male', { static: false, read: MatRadioButton }) panel_basic_male : MatRadioButton;
  @ViewChild('female', { static: false, read: MatRadioButton }) panel_basic_female : MatRadioButton;
  @ViewChild('jmbg', { static: false, read: MatInput }) panel_basic_jmbg : MatInput;
  @ViewChild('profession', { static: false, read: MatInput }) panel_basic_profession : MatInput;

  // Panel : account
  @ViewChild('panel_account', {static: false, read: MatExpansionPanel}) panel_account : MatExpansionPanel;
  @ViewChild('panel_account_username', { static: false, read : MatInput }) panel_account_username : MatInput;
  @ViewChild('panel_account_email', { static: false, read : MatInput }) panel_account_email : MatInput;
  @ViewChild('panel_account_password', { static: false, read : MatInput }) panel_account_password : MatInput;
  @ViewChild('panel_account_password_confirm', { static: false, read : MatInput }) panel_account_password_confirm : MatInput;

  // Panel : security
  @ViewChild('panel_security', { static : false, read: MatExpansionPanel }) panel_security : MatExpansionPanel;
  @ViewChild('secq', { static : false, read: MatInput }) panel_security_question : MatInput;
  @ViewChild('seca', { static : false, read: MatInput }) panel_security_answer : MatInput;

  // Panel : upload
  @ViewChild('panel_upload', { static: false, read: MatExpansionPanel }) panel_upload : MatExpansionPanel;
  //@ViewChild('panel_upload_submit', { static: false, read : MatInput }) panel_upload_submit : MatInput;

  // Main button.
  @ViewChild('button_login', { static: false, read: MatButton }) button_login : MatButton;

  constructor( private globals : Globals, private server : Server, public dialog : MatDialog )
  {
    this.regop = new Registration( dialog, server );
    this.isReady = true;
  }

  ngOnInit() {
    this.globals.renameDocumentTitle("Registracija");
  }

  ngAfterViewInit()
  {
    this.regop.button_submit = this.button_login;

    // Register basic panel.
    this.regop.register(
      new BasicRegistrationPanel( this.panel_basic, 4, {
        name : this.panel_basic_name,
        surname : this.panel_basic_surname,
        male :  this.panel_basic_male,
        female : this.panel_basic_female,
        jmbg : this.panel_basic_jmbg,
        profession : this.panel_basic_profession
      } )
    );

    // Register account panel.
    this.regop.register(
      new AccountRegistrationPanel( this.panel_account, 5, {
        username : this.panel_account_username,
        email: this.panel_account_email,
        password: this.panel_account_password,
        password_confirm: this.panel_account_password_confirm
      }, this.server )
    );

    // Register security panel.
    this.regop.register(
      new SecurityRegistrationPanel( this.panel_security, 2, {
        secq : this.panel_security_question,
        seca: this.panel_security_answer
      } )
    );

    // Register picture upload panel.
    this.regop.register(
      new UploadRegistrationPanel( this.panel_upload, 2, {}, this.regop )
    );

    this.loaded = true;
    this.regop.setLoaded();
    this.regop.setVisible(0);
    this.regop.focusFirst();
  }

  /**
   * Checks if component is loaded.
   */
  public isLoaded() : boolean
  {
    return this.loaded;
  }

  /**
   * Validates current panel.
   */
  public validate(ev : Event) : void {
    this.regop.verify();
    ev.preventDefault();
  }

  /**
   * Registers user.
   */
  public register(event : Event) : void 
  {
    event.preventDefault();
    if ( this.regop.isValid() )
      this.regop.registerUser();
  }

  /**
   * Opens new panel.
   * 
   * @param panelNo New opened panel.
   */
  public openedPanel(panelNo : number) : void
  {
    this.regop.setPanel(panelNo);
  }

  /**
   * Closes current panel.
   */
  public closedPanel() : void
  {
    this.regop.exitPanel();
  }

}
