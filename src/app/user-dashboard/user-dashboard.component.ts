import { Component, OnInit, ViewEncapsulation, ViewChild, ComponentFactoryResolver, OnDestroy, ViewContainerRef, AfterViewInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { Globals } from 'src/assets/code/globals';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MultiplayerRequests } from 'src/assets/code/user/multiplayer-requests';
import { Server } from 'src/assets/code/http-request';
import { ModalWaitForUser } from 'src/assets/components/modals/modal-wait-for-user/modal-wait-for-user';


import io from 'socket.io-client';
import { MultiplayerGame } from 'src/assets/code/multiplayer-game';
import { Router } from '@angular/router';
import { SingleplayerGame } from 'src/assets/code/user/singleplayer-request';
import { WaitingModalData } from 'src/assets/code/util/connection-util';
import { RangList } from 'src/assets/code/user/rang-list';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserDashboardComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy
{
  private hasGameOfTheDay: boolean;
  private loaded: boolean;
  private gameOfTheDayData: any;
  private playedGameOfTheDay: boolean;
  private emptyRangList: boolean;
  private gameOfTheDayResults: any;
  private loadingGameOfTheDay: boolean;
  //private interval: any;
  multiplayerHandle: MultiplayerRequests;
  displayedColumns: string[] = ['rang', 'name', 'date', 'points'];
  dataSource: Array<RangList>;

  @ViewChild("myComp", {static: true, read: ViewContainerRef}) multiplayerPlaceholder: ViewContainerRef;    

  constructor( 
    private globals: Globals, 
    private componentFactoryResolver: ComponentFactoryResolver, 
    private server: Server, 
    private dialog: MatDialog ,
    private cdRef:ChangeDetectorRef,
    private multiplayer: MultiplayerGame,
    private router: Router,
    private singleplayer: SingleplayerGame,
    private detectorRef: ChangeDetectorRef
  ) 
  {
    var st = this.globals.getUser().getData().sex == 0 ? "Dobrodošao" : "Dobrodošla";
    this.globals.renameDocumentTitle( st + ", " + this.globals.getUser().getData().username );
    this.multiplayerHandle = new MultiplayerRequests(componentFactoryResolver, server, cdRef);
    this.hasGameOfTheDay = false;
    this.loaded = false;
    this.playedGameOfTheDay = false;
    this.loadingGameOfTheDay = true;
    this.dataSource = null;
    this.emptyRangList = false;
  }

  ngOnInit() {

  }

  ngAfterViewInit()
  {
    this.multiplayerHandle.multiplayerPlaceholder = this.multiplayerPlaceholder;
    this.multiplayerHandle.refreshComponents(false); 

    // Get rang list.
    this.server.requestAuth("post", "user/get-rang-list", {}, (resp: any) => {
      var arr: RangList[] = resp.ranglist;
      var el: RangList;
      var username: string = this.globals.getUser().getData().username;
      if ( resp.length == 0 )
      {
        this.emptyRangList = true;
        return;
      }
      // Sort rang list.
      for (var i=0; i<arr.length-1; i++)
      {
        for (var j=i+1; j<arr.length; j++)
          if ( arr[i].points < arr[j].points )
          {
            el = arr[i];
            arr[i] = arr[j];
            arr[j] = el;
          };
      };

      // Check if user is here!
      var found: boolean = false;
      var ind: number = 0;
      for (var i=0; i<arr.length; i++)
      {
        arr[i].rang = (i+1);
        if ( arr[i].username == username )
        {
          found = true;
          ind = i;
        };
      };

      // Append new row.
      if ( ind > 9 )
      {

      };

      this.dataSource = arr;
    }, (err: any) => {});

    this.server.requestAuth("post", "user/active-games-check", {}, (resp: any) => {
      if ( resp.multiplayer.hasActiveWaitingGame )
      {
        var data: any = this.globals.getUser().getData();
        var name: string = data.name + " " + data.surname;
        this.multiplayer.prepare();
        this.openWaitingDialog({ myName: name, isMultiplayer: true, supervisor: null, otherUser: null });
      } else if ( resp.multiplayer.isActivelyWaiting || resp.singleplayer.activeGame )
      {
        console.log("heree");
        this.router.navigate(['/user/etf-quiz']);
      } else if ( resp.singleplayer.hasActiveWaitingGame )
      {
        var data: any = this.globals.getUser().getData();
        var name: string = data.name + " " + data.surname;
        this.singleplayer.prepare();
        this.openWaitingDialog({ myName: name, isMultiplayer: false, supervisor: resp.singleplayer.data.supervisor, otherUser: null });
      };

      this.hasGameOfTheDay = resp.gameOfTheDay.found;
      this.gameOfTheDayData = resp.gameOfTheDay.data;
      this.playedGameOfTheDay = resp.gameOfTheDay.played;
      this.gameOfTheDayResults = resp.gameOfTheDay.results;
      this.loadingGameOfTheDay = false;
      this.loaded = true; 
    }, err => {
      //window.location.reload();
      console.error(err);
    });
    
    this.multiplayerHandle.refreshComponents();
  }

  ngOnDestroy()
  {
    
  }

  ngAfterViewChecked()
  {
    this.cdRef.detectChanges();
  }

  public refreshMultiplayerGames(): void
  {
    
  }

  public getTotalPoints(): number
  {
    var f: any = this.gameOfTheDayResults;
    return f.anagram + f.number + f.fxf + f.natgeo + f.trophie;
  }

  /**
   * Creates new match for user...
   */
  public createMatch() : void
  {
    var data: any = this.globals.getUser().getData();
    var name: string = data.name + " " + data.surname;

    // Creates new game for username
    this.multiplayer.createNewGame( this.globals.getUser().getData().username );

    // Open waiting dialog.
    this.openWaitingDialog({ myName: name, isMultiplayer: true, supervisor: null, otherUser: null });
  }

  /**
   * Opens waiting dialog.
   */
  public openWaitingDialog(data: WaitingModalData): MatDialogRef<ModalWaitForUser>
  {
    // Open waiting dialog.
    return this.dialog.open(ModalWaitForUser, {
        disableClose: true,
        closeOnNavigation: false,
        backdropClass: 'backdrop',
        width: "350px",
        data: data
    });
  }

  /**
   * Opens waiting dialog.
   */
  public playGameOfTheDay(): void
  {
    var data: any = this.globals.getUser().getData();
    var name: string = data.name + " " + data.surname;

    // Creates new game for username.
    this.singleplayer.createPendingGame();

    // Open waiting dialog.
    this.openWaitingDialog({ myName: name, isMultiplayer: false, supervisor: null, otherUser: null });
  }
}
