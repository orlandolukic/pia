import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, AfterContentInit, ChangeDetectorRef, OnChanges } from '@angular/core';
import { Globals } from 'src/assets/code/globals';
import { MultiplayerGame } from 'src/assets/code/multiplayer-game';
import { Router } from '@angular/router';
import { SingleplayerGame } from 'src/assets/code/user/singleplayer-request';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatHorizontalStepper } from '@angular/material';
import { GameManagement } from 'src/assets/code/quiz/game-management';
import { Server } from 'src/assets/code/http-request';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges
{
  private waiting: boolean;
  private loading: boolean;
  private loadingPoints: boolean;
  private terminating: boolean;
  private pAnagram: number;
  private pNumber: number;
  private pFxF: number;
  private pNatGeo: number;
  private pTrophie: number;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  fifthFormGroup: FormGroup;

  @ViewChild(MatHorizontalStepper, {static: true, read: MatHorizontalStepper}) childStepper: MatHorizontalStepper;

  constructor( 
    private globals: Globals, 
    private router: Router, 
    private server: Server,
    private multiplayer: MultiplayerGame, 
    private singleplayer: SingleplayerGame,
    private formBuilder: FormBuilder,
    private management: GameManagement,
    private detectorRef: ChangeDetectorRef
  ) {

    this.globals.renameDocumentTitle("Kviz");

    this.waiting = true;
    this.loading = true;
    this.loadingPoints = true;
    this.terminating = false;
    this.pAnagram = 0;
    this.pFxF = 0;
    this.pNatGeo = 0;
    this.pTrophie = 0;
    this.pNumber = 0;
  }

  ngOnInit() 
  {
    // Create forms for mat-stepper. 
    this.firstFormGroup = this.formBuilder.group({});
    this.secondFormGroup = this.formBuilder.group({}); 
    this.thirdFormGroup = this.formBuilder.group({}); 
    this.fourthFormGroup = this.formBuilder.group({}); 
    this.fifthFormGroup = this.formBuilder.group({});   

    if ( this.globals.inGame == 0 )
    {
      this.singleplayer.prepare();
      this.singleplayer.addEventListeners(() => {
        this.singleplayer.addEventListener("ctrl-sp-points-review", (data: any) => {
          this.pAnagram = data.points.anagram;
          this.pFxF = data.points.fxf;
          this.pNatGeo = data.points.natgeo;
          this.pTrophie = data.points.trophie;
          this.pNumber = data.points.number;
          this.loadingPoints = false;
        });
      });
    } else if ( this.globals.inGame == 1 )
    {
      this.multiplayer.prepare();
      this.multiplayer.addEventListener("on-game-termination", (data: any) => { this.onQuizTermination();});
    };  

    this.management.init( this.detectorRef, this.globals.inGame == 1 ).then((data: any) => {
      this.detectorRef.detectChanges();
      this.management.start();
      try {
        this.management.getGameWrapper().getCurrentGame().onPreview();
        if ( this.management.getTimeManager().checkTimeout() )
          this.management.onTimeout();
      } catch(e) {
        this.management.canLeaveFlag = true;
        this.singleplayer.sendRegularRequest("sp-points-review", null);
      }
    }); 
  }

  ngOnDestroy()
  {
    //this.multiplayer.socketDisconnect();
    this.management.onDestroy();
  }

  ngAfterViewInit()
  {
    //this.management.start();
    // Load game management.    
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void 
  {
    //throw new Error("Method not implemented.");
  }


  public calculateTotalPoints(): number
  {
    return this.pAnagram + this.pNumber + this.pFxF + this.pNatGeo + this.pTrophie;
  } 

  public finish(): void
  {
    this.terminating = true;
    this.singleplayer.addEventListener("ctrl-sp-terminate-game", (data: any) => {
      this.router.navigate(['/user/dashboard']);
    });
    this.singleplayer.sendRegularRequest("sp-terminate-game", null);
  }

  /**
   * Exists quiz.
   */
  protected onQuizTermination(): void
  {
    alert("Partija je otkazana.");    
    this.multiplayer.terminateGame(false);
    this.router.navigate(['/user/dashboard']);
  }



}
