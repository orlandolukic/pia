import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Globals } from 'src/assets/code/globals';
import { MatTabChangeEvent } from '@angular/material';
import { AdminRequestsComponent } from 'src/assets/components/admin-requests/admin-requests.component';
import { Server } from 'src/assets/code/http-request';
import { AdminGameOfTheDayComponent } from 'src/assets/components/admin-game-of-the-day/admin-game-of-the-day.component';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit, AfterViewInit
{
  @ViewChild(AdminRequestsComponent, {static: true, read: AdminRequestsComponent}) childAdminRequestsComponent: AdminRequestsComponent;
  @ViewChild(AdminGameOfTheDayComponent, {static: true, read: AdminGameOfTheDayComponent}) childGamesOfTheDay: AdminGameOfTheDayComponent;

  constructor(
    private globals: Globals,
    private server: Server
  ) {
    this.globals.renameDocumentTitle("Administrator sekcija");
  }

  ngOnInit() {

  }

  ngAfterViewInit()
  {
    this.childAdminRequestsComponent.fetchRequests();
  }

  public getFullName(): string
  {
    try {
      var u = this.globals.getUser().getData();
      return u.name + " " + u.surname;
    } catch(e) {
      return null;
    }
  }

  public changedTab(event: MatTabChangeEvent): void
  {
    if ( event.index == 0 )
    {
      this.childAdminRequestsComponent.fetchRequests();
    } else if ( event.index == 1 )
    {
      this.childGamesOfTheDay.clearDate();
      this.childGamesOfTheDay.refreshGamesOfTheDay();
    };

    if ( event.index > 0 )
      this.childAdminRequestsComponent.leaveFocus();
  }

}
