import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { RouterChecks } from 'src/assets/code/router-check';
import { Globals } from 'src/assets/code/globals';
import { Server } from 'src/assets/code/http-request';
import { MatInput } from '@angular/material/input';
import { sha256 } from 'js-sha256';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {

  private g : number;
  private usernameText : string;
  private passwordText : string;
  private timeout : any;
  private timeoutInterval : number = 8000;
  private info: boolean;
  private infoTimeout: any;
  private infoMessage: string;
  private infoClass: string;

  @ViewChild("username", {static: true, read: MatInput}) field_username : MatInput;
  @ViewChild("password", {static: true, read: MatInput}) field_password : MatInput;

  constructor( 
    private router: Router, 
    private main : AppComponent, 
    g : RouterChecks, 
    private globals : Globals,
    private server : Server
  ) 
  {
    this.info = false;
  }


  ngAfterViewInit(): void 
  {
    var obj = this;
    setTimeout(function() {
      obj.field_username.focus();
    }, 500);
  }

  ngOnInit() : void {
    this.globals.renameDocumentTitle("Takmiči se i ti!");
    this.globals.inLogin = false;
    var k = this;
    //setTimeout( function() { AppComponent.getInstance().setLogged(true); console.log("set logged!"); }, 5000 );
    //this.router.navigate(['forgot-password']);
  }

  ngOnDestroy(): void 
  {
    //console.log("destroyed");
    this.globals.inLogin = false;
  }

  /**
   * Prepares form for next request.
   */
  private prepareForm(enable : boolean, st : boolean) : void
  {
    this.field_username.value = "";
    this.field_password.value = "";
    this.field_password.disabled = !enable;
    this.field_username.disabled = !enable;
    clearTimeout(this.timeout);
    this.globals.errorCredentials = true;
    this.globals.inLogin = false;
    var obj = this;
    if ( st )
    {
      this.timeout = setTimeout(function() {
        obj.globals.errorCredentials = false;
      }, this.timeoutInterval);
      setTimeout(function() {
        obj.field_username.focus();
      }, 100);
    };
  }

  /**
   * Tries to login into the system.
   */
  public login(event: Event) : void 
  {
    event.preventDefault();
    this.globals.inLogin = true;
    var obj = this;
    clearTimeout(this.timeout);
    //this.timeout = null;
    this.globals.errorCredentials = false;

    if ( typeof this.usernameText === "undefined" || typeof this.passwordText === "undefined" || this.usernameText === "" || this.passwordText === "" )
    {
      this.field_username.value = "";
      this.field_password.value = "";
      this.field_password.disabled = false;
      this.field_username.disabled = false;
      this.globals.inLogin = false;
      
      this.infoMessage = "Postoje prazna polja.";
      this.infoClass = "error-card";
      this.info = true;
      clearTimeout(this.infoTimeout);
      this.infoTimeout = setTimeout(function() {
        obj.info = false;
      }, 5000);
      this.field_username.focus();
      return;
    };
    this.server.request('post', 'user/exists-with-credentials', {username: this.usernameText, password: sha256(this.passwordText)}, function(d) {
      obj.usernameText = "";
      obj.passwordText = "";
      if ( !d.exists )
      {
        obj.field_username.value = "";
        obj.field_password.value = "";
        obj.globals.inLogin = false;

        setTimeout(function() {
          obj.field_username.focus();
        }, 100);
        clearTimeout(obj.timeout);
        //obj.timeout = null;

        obj.infoMessage = "Korisničko i lozinka nisu ispravni.";
        obj.info = true;
        obj.infoClass = "error-card";
        clearTimeout(obj.infoTimeout);
        obj.infoTimeout = setTimeout(function() {
          obj.info = false;
        }, obj.timeoutInterval);

      } else if ( d.exists && !d.isValid )   // Not activated user.
      {

        obj.field_username.value = "";
        obj.field_password.value = "";        
        obj.globals.errorCredentials = false;
        obj.globals.inLogin = false;
        setTimeout(function() {
          obj.field_username.focus();
        }, 100);

        obj.infoClass = "warning-card";
        obj.infoMessage = "Nalog pregleda administrator, biće aktivan veoma brzo!";
        obj.info = true;
        clearTimeout(obj.infoTimeout);
        obj.infoTimeout = setTimeout(function() {
          obj.info = false;
        }, 5000);

      } else  // Go to user section.
      {
        obj.router.navigate(['/user/dashboard']);
      }
    });
  }

  public keyPress(event: KeyboardEvent) : void
  {
    if ( event.keyCode === 13 )
    {
      event.preventDefault();
      this.field_password.focus();
    }
  }

}
