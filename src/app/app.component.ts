import { Component, OnInit, Injectable } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { Title } from '@angular/platform-browser';
import { environment } from '../environments/environment';
import { RouteConfigLoadEnd, NavigationStart, NavigationEnd } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { Router } from '@angular/router';
import { RouterChecks } from 'src/assets/code/router-check';
import { Globals } from 'src/assets/code/globals';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Injectable()
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent
{
  private static instance : AppComponent;
  public static getInstance() : AppComponent 
  {
    return AppComponent.instance;
  }

  public constructor(private titleService : Title, router: Router, g : RouterChecks, private globals: Globals ) 
  {
    AppComponent.instance = this;
    this.titleService.setTitle( "Takmiči se i ti! - " + environment.mainTitle  );
    this.isLogged = false;
    //RouterChecks.create(router);
  }
  
  // Data used in application.
  private logoSrc : string = "../assets/images/logo.png";
  private isLogged : boolean;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  public setNewTitle(title : string) 
  {
    this.titleService.setTitle(title + " - " + environment.mainTitle);
  }

  /**
   * Sets logged in indicator.
   * 
   * @param logged Indicator if user is logged in.
   */
  public setLogged(logged : boolean) : void
  {
    this.isLogged = logged;
  }
}
