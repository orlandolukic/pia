import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule, MatRadioButton, MatRadioModule, MatDialogModule, MatDividerModule, MatTabsModule, MatProgressBarModule, MatTableModule, MatPaginatorModule, MatPaginatorIntl, MatCheckboxModule, MatTooltipModule, MatSelectModule, MatSnackBarModule, MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatStepperModule } from '@angular/material/stepper';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { routes } from '../routes';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MenuUnregisteredComponent } from './menu-unregistered/menu-unregistered.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { CommonModule } from '@angular/common';
import { MenuRegisteredComponent } from './menu-registered/menu-registered.component';
import { RouterChecks } from 'src/assets/code/router-check';
import { Globals } from 'src/assets/code/globals';
import { Server } from 'src/assets/code/http-request';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { ResultsComponent } from './results/results.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { DialogOverviewExampleDialog } from 'src/assets/code/registration/modal-registration';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ConfirmDeactivateForgotPasswordGuard } from 'src/assets/code/forgot-password/guard';
import { ForgotPasswordGuard, LoggedInGuard, QuizGuard, QuizGuardActivation, SupervisorGuard } from 'src/assets/code/guards';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { AppInitCallbacks, app_init } from 'src/assets/code/app-init-callbacks';
import { LogoutComponent } from './logout/logout.component';
import { MultiplayerUserRequestComponent } from 'src/assets/components/multiplayer-user-request/multiplayer-user-request';
import { ModalWaitForUser } from 'src/assets/components/modals/modal-wait-for-user/modal-wait-for-user';
import { QuizComponent } from './quiz/quiz.component';
import { MultiplayerGame } from 'src/assets/code/multiplayer-game';
import { SupervisorDashboardComponent } from './supervisor-dashboard/supervisor-dashboard.component';
import { MatPaginatorIntlSerbian } from 'src/assets/code/util/mat-paginator-sr';
import { ModalAddNewAnagramsComponent } from 'src/assets/components/modals/modal-add-new-anagram/modal-add-new-anagram';
import { Game5x5Component } from 'src/assets/components/game-5x5/game-5x5';
import { ModalAddNew5x5Component } from 'src/assets/components/modals/modal-add-new-5x5/modal-add-new-5x5.component';
import { TrophieEditComponent } from 'src/assets/components/trophie-edit/trophie-edit.component';
import { ModalAddNewTrophieComponent } from 'src/assets/components/modals/modal-add-new-trophie/modal-add-new-trophie.component';
import { NatGeoComponent } from 'src/assets/components/nat-geo/nat-geo.component';
import { ModalAddNewWordComponent } from 'src/assets/components/modals/modal-add-new-word/modal-add-new-word.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminRequestsComponent } from 'src/assets/components/admin-requests/admin-requests.component';
import { AdminGameOfTheDayComponent } from 'src/assets/components/admin-game-of-the-day/admin-game-of-the-day.component';
import { GameFxfTableComponent } from 'src/assets/components/game-fxf-table/game-fxf-table.component';
import { SingleplayerGame } from 'src/assets/code/user/singleplayer-request';
import { Supervise } from 'src/assets/code/supervisor/supervise';
import { SupervisorGameRequestsComponent } from 'src/assets/components/supervisor-game-requests/supervisor-game-requests.component';
import { SupervisorGameRequestComponent } from 'src/assets/components/supervisor-game-request/supervisor-game-request.component';
import { SpSectionSuperviseComponent } from 'src/assets/components/supervisor/sp-section-supervise/sp-section-supervise.component';
import { SuperviseActivateGuard, SuperviseDeactivateGuard } from 'src/assets/code/guards-supervisor';
import { GameManagement } from 'src/assets/code/quiz/game-management';
import { QuizAnagramComponent } from 'src/assets/components/quiz/quiz-anagram/quiz-anagram.component';
import { QuizFxfComponent } from 'src/assets/components/quiz/quiz-fxf/quiz-fxf.component';
import { QuizNatGeoComponent } from 'src/assets/components/quiz/quiz-nat-geo/quiz-nat-geo.component';
import { QuizNumberComponent } from 'src/assets/components/quiz/quiz-number/quiz-number.component';
import { QuizTrophieComponent } from 'src/assets/components/quiz/quiz-trophie/quiz-trophie.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    MenuUnregisteredComponent,
    ForgotPasswordComponent,
    MenuRegisteredComponent,
    RegisterComponent,
    ResultsComponent,
    DialogOverviewExampleDialog,
    ResetPasswordComponent,
    UserDashboardComponent,
    LogoutComponent,
    MultiplayerUserRequestComponent,
    ModalWaitForUser,
    ModalAddNewAnagramsComponent,
    ModalAddNew5x5Component,
    ModalAddNewTrophieComponent,
    QuizComponent,
    SupervisorDashboardComponent,
    Game5x5Component,
    TrophieEditComponent,
    NatGeoComponent,
    ModalAddNewWordComponent,
    AdminDashboardComponent,
    AdminRequestsComponent,
    AdminGameOfTheDayComponent,
    GameFxfTableComponent,
    SupervisorGameRequestsComponent,
    SupervisorGameRequestComponent,
    SpSectionSuperviseComponent,
    QuizAnagramComponent,
    QuizFxfComponent,
    QuizNatGeoComponent,
    QuizNumberComponent,
    QuizTrophieComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatRadioModule,
    MatCardModule,
    RouterModule.forRoot(routes, {enableTracing: false}),
    RouterModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatTabsModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [ 
    AppComponent, 
    RouterChecks, 
    Globals, 
    Server, 
    HttpClient, 
    ForgotPasswordGuard,
    QuizGuard,
    SupervisorGuard,
    QuizGuardActivation,
    LoggedInGuard,
    AppInitCallbacks,
    MultiplayerGame,
    {
      provide: MatPaginatorIntl,
      useClass: MatPaginatorIntlSerbian
    },
    SingleplayerGame,
    Supervise,
    SuperviseActivateGuard,
    SuperviseDeactivateGuard,
    GameManagement,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogOverviewExampleDialog,
    MultiplayerUserRequestComponent,
    ModalWaitForUser,
    ModalAddNewAnagramsComponent,
    ModalAddNew5x5Component,
    Game5x5Component,
    ModalAddNewTrophieComponent,
    ModalAddNewWordComponent,
    SupervisorGameRequestComponent
  ],
})
export class AppModule { }
