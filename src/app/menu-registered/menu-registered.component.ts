import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Globals } from 'src/assets/code/globals';

@Component({
  selector: 'app-menu-registered',
  templateUrl: './menu-registered.component.html',
  styleUrls: ['./menu-registered.component.css']
})
export class MenuRegisteredComponent implements OnInit, AfterViewInit {

  private accType: number;
  private loaded: boolean;
  constructor( private globals: Globals ) 
  { 
    this.loaded = false;
    this.accType = 0;
  }

  ngOnInit() {
    this.accType = this.globals.getUser().getData().accType;
    this.loaded = true;
  }

  ngAfterViewInit()
  {
    
  }

}
