/**
 * Routing check.
 */
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class RouterChecks
{
    constructor(private router: Router) 
    {
        /*
        // Checks login action.
        this.router.events.subscribe( function(event) {
            if ( event instanceof NavigationStart )
            console.log("Navigation started");
            else if ( event instanceof NavigationEnd )
            console.log("Navigation ended.");
        } );
        */
    }
}