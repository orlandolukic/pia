/**
 * Class for managing supervisor details.
 */
export class SupervisorData
{        
    private username: string;
    private name: string;
    private surname: string;

    constructor() 
    {
        this.username = null;
        this.name = null;
        this.surname = null;
    }

    public getUsername(): string { return this.username; }
    public getName(): string { return this.name; }
    public getSurname(): string { return this.surname; }
    public getFullName(): string { return this.name + " " + this.surname; }

    public setUsername(value: string): void { this.username = value; }
    public setName(value: string): void { this.name = value; }
    public setSurname(value: string): void { this.surname = value; }

    public isLoaded(): boolean
    {
        return this.username == null || this.name == null || this.surname == null;
    }

}