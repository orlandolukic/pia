export interface WaitingModalData
{
  isMultiplayer: boolean;
  myName: string;
  supervisor: any;
  otherUser: any;
}