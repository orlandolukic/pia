import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanDeactivate } from '@angular/router';
import { Server } from './http-request';
import { Supervise } from './supervisor/supervise';
import { Injectable } from '@angular/core';
import { SpSectionSuperviseComponent } from '../components/supervisor/sp-section-supervise/sp-section-supervise.component';
import { MatSnackBar } from '@angular/material';

/**
 * Guard for handling singleplayer game.
 */
@Injectable()
export class SuperviseActivateGuard implements CanActivate
{
    constructor(
        private server: Server,
        private router: Router,
        private supervise: Supervise
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state:  RouterStateSnapshot): Promise<boolean> {        
        return new Promise<boolean>((resolve) => {
            this.server.checkIfUserIsLoggedIn().then((logged) => {
                if ( !logged )
                {
                    this.router.navigate(['/supervisor/dashboard']);
                    resolve(false);                    
                    return;
                };

                if ( this.supervise.inSupervision )
                {
                    resolve(true);
                    return;
                };

                this.server.requestAuth("post", "supervisor/can-open-supervise-section", {}, (resp: any) => {
                    if ( !resp.singleplayer.hasActiveQuiz )
                    {                  
                        this.router.navigate(['/supervisor/dashboard']);      
                        resolve(false);                        
                    } else
                        resolve(true);                    
                }, err => {
                    console.error("Could not check if supervisor has access to the supervise section.");
                    resolve(false);
                });
            });
        });
    }
    
}

@Injectable()
export class SuperviseDeactivateGuard implements CanDeactivate<SpSectionSuperviseComponent>
{
    constructor(
        private snackBar: MatSnackBar,
        private supervise: Supervise
    ) {}
     
    canDeactivate(component: SpSectionSuperviseComponent, croute: ActivatedRouteSnapshot, cstate: RouterStateSnapshot, nstate: RouterStateSnapshot): Promise<boolean>  {
        return new Promise<boolean>((resolve) => {
            if ( this.supervise.inSupervision )
            {
                this.snackBar.open("Nije moguće napustiti partiju dok se ona ne završi ili dok korisnik ne odustane od nje.", null, {
                    duration: 5000
                });
            };
            resolve ( !this.supervise.inSupervision );
            
        });
    }

}