import { Injectable } from '@angular/core';
import { Server } from './http-request';

/**
 * App init callbacks.
 */
 export function app_init(instance: AppInitCallbacks): Function
 {
    return () => instance.init(Server.getInstance());
 }


 /**
  * Class for handling init requests.
  */
 @Injectable()
 export class AppInitCallbacks
 {

     /**
      * Inits application.
      */
    public init(server: Server) : Promise<any>
    {
        return new Promise<any>((resolve, reject) => {
            /*
            server.request('post', 'user/is-active-session', {}, function(data, resp) {
                console.log(resp);
                resolve();
            });
            */
           resolve();
        })
    }
 }