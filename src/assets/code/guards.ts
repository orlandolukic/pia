import { Injectable } from '@angular/core';
import { CanDeactivate, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { ForgotPasswordComponent } from 'src/app/forgot-password/forgot-password.component';
import { Globals } from './globals';
import { Server } from './http-request';
import { User } from './user';
import { Observable, Subject } from 'rxjs';
import { QuizComponent } from 'src/app/quiz/quiz.component';
import { MultiplayerGame } from './multiplayer-game';
import { Supervise } from './supervisor/supervise';
import { SingleplayerGame } from './user/singleplayer-request';
import { GameManagement } from './quiz/game-management';

@Injectable()
export class ForgotPasswordGuard implements CanDeactivate<ForgotPasswordComponent> {
    
    canDeactivate(target: ForgotPasswordComponent) {
        if ( target.isImportantToStay )
        {
            alert("Lozinka se menja. Nije moguće napustiti stranicu.");
            return false;
        } else if (target.inProcess) {
            return window.confirm('Da li ste sigurni da želite da odustanete?');
        };
        return true;
    }
}

@Injectable()
export class QuizGuard implements CanDeactivate<QuizComponent> {

    constructor( 
        private server: Server, 
        private router: Router, 
        private multiplayer: MultiplayerGame,
        private singleplayer: SingleplayerGame,
        private globals: Globals,
        private management: GameManagement
    ) {}

    canDeactivate(target: QuizComponent, route: ActivatedRouteSnapshot, state: RouterStateSnapshot, nextState: RouterStateSnapshot): Promise<boolean> {                
        return new Promise<boolean>((resolve) => {
            if ( this.globals.inGame == 1 )
            {
                this.multiplayer.canSignOut().then(retval => {                
                    if ( retval == null )
                    {
                        console.log("Error occured");
                        resolve(false);
                        return;
                    };

                    if ( !retval.multiplayer.empty )
                    {
                        if ( retval.multiplayer.canSignOut )
                        {
                            var x = confirm("Napuštate igru protiv ovog takmičara. Nastavljamo?");
                            if ( x )
                            {
                                // Go to the database.
                                // Terminate multiplayer game.                   
                                this.multiplayer.terminateGame();
                                resolve(true);
                            } else
                                resolve(false);
                        } else if ( retval.empty )
                        {
                            resolve(true);
                        } else
                        {
                            resolve(false);
                            alert("Partija je započela. Nije moguće napustiti je.");
                        };  
                    } else if ( !retval.singleplayer.empty )    // Check singleplayer.
                    {
                        resolve(false);
                        alert("Partija je vec pocela. Nije moguce napustiti je.");
                    } else
                        resolve(true);        
                });  
            } else if ( this.globals.inGame == 0 ) // Singleplayer.
            {
                if ( this.management.canLeave() )
                {
                    resolve(true);
                } else 
                {
                    var x = confirm("Ukoliko izađete iz igre, predajete partiju. Nastavljamo?");                
                    if ( x )
                    {                    
                        this.singleplayer.destroyMatch().then(() => {                        
                            this.globals.inGame = -1;                        
                                                    
                            if ( nextState.url != "/logout" )
                                this.router.navigate(['/user/dashboard']);
                            resolve(true);                        
                        });                   
                        
                    } else
                        resolve(false);
                };
            } else
                resolve(true);       
        });
    }
}

@Injectable()
export class QuizGuardActivation implements CanActivate
{
    constructor( private server: Server, private router: Router, private globals: Globals ) {}

    /**
     * Checks if user has some active matches.
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean>
    {
        return new Promise<boolean>((resolve) => { 

            // Go check for user!            
            this.server.checkIfUserIsLoggedIn().then(retval => {
                if ( !retval )
                {
                    this.router.navigate(['/']);
                    resolve(false);
                } else
                {
                    if ( route.queryParams != undefined && route.queryParams.force != undefined && route.queryParams.force == 1 )
                    {
                        console.log("canActivate guard for Quiz.");
                        resolve(true);
                        return;
                    };

                    setTimeout(() => {
                        this.server.requestAuth("post", "user/has-active-quiz", {}, resp => {
                            if ( resp.hasquiz )
                            {
                                this.globals.inGame = resp.inGame;
                                resolve(true);
                            } else
                            {   
                                this.router.navigate(['/user/dashboard']);
                                resolve(false);
                            };
                        }, err => {
                            this.router.navigate(['/']);
                            resolve(false);                
                        });      
                    }, 500);   
                }
            });   
            
                           
        });
    }
}

@Injectable()
export class LoggedInGuard implements CanActivate
{
    private static instance: LoggedInGuard;
    public static getInstance(): LoggedInGuard
    {
        return LoggedInGuard.instance;
    }

    private allowedLoggedIn: Array<string> = [
        "/user/dashboard",
        "/user/etf-quiz"
    ];
    private allowedSupervisorLoggedIn: Array<string> = [
        "/supervisor/dashboard",
        "/supervisor/supervise/singleplayer-game",
        "/supervisor/supervise/multiplayer-game"
    ];
    private allowedAdminLoggedIn: Array<string> = [
        "/admin/dashboard"
    ];

    private beforeRouting: Subject<GuardAuthority>;

    constructor(
        private router: Router, 
        private globals: Globals, 
        private server: Server, 
        private multiplayer: MultiplayerGame,
        private singleplayer: SingleplayerGame,
        private supervise: Supervise
    ) 
    {
        LoggedInGuard.instance = this;    
        this.beforeRouting = new Subject<GuardAuthority>();   
    }

    /**
     * Gets before routing listener.
     */
    public getBeforeRoutingObservable() : Observable<GuardAuthority>
    {
        return this.beforeRouting.asObservable();
    }

    /**
     * Checks if component could be activated.
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> 
    {       
        var obj = this;
        return new Promise<boolean>((resolve, reject) => {

            var redirected: boolean = false;

            // Logout request.
            if ( state.url === "/logout" )
            {
                // Check if user is supervisor and check if user has active quiz.
                var f: Promise<any> = this.server.checkIfSupervisorHasActiveQuiz().then((resp: any) => {
                    if ( resp == null )
                    {
                        return;
                    };
                    try {
                        if ( resp.singleplayer.hasActiveQuiz )
                        {
                            this.router.navigate(['/supervisor/supervise/singleplayer-game']);
                            redirected = true;
                            resolve(false);                        
                        } else if ( resp.multiplayer.hasActiveQuiz )
                        {
                            this.router.navigate(['/supervisor/supervise/multiplayer-game']);
                            redirected = true;
                            resolve(false);                        
                        };  
                    } catch(e) {
                        redirected = true;
                        this.router.navigate(['/']);
                        resolve(false);
                    }                     
                });

                f.then(() => {
                    if ( !redirected )
                    {
                        this.server.request('post', "user/logout", {}, (data, resp) => {
                            this.globals.getLogoutSubject().next();
                            this.globals.clearLogoutSubject();
                            this.globals.user = null;
                            this.globals.inGame = -1;
        
                            this.singleplayer.socketDisconnect();
                            this.multiplayer.socketDisconnect();
                            this.supervise.socketDisconnect();
        
                            this.router.navigate(['/']);
                            resolve(false);      
                        }); 
                    };
                });                
                return;                               
            };
            
            if ( this.globals.getUser() != null )
            {
                var allow = true;
                var user = this.globals.getUser();
                if ( this.globals.getUser().isStillValid() )
                {                    
                    if ( user.isRegularUser() && !this.allowedLoggedIn.includes( state.url ) )
                    {
                        this.router.navigate(['/user/dashboard']);
                        allow = false;
                    } else if ( user.isSupervisor() && !this.allowedSupervisorLoggedIn.includes( state.url ) )
                    {
                        allow = false;
                        this.router.navigate(['/supervisor/dashboard']);
                    } else if ( user.isAdministrator() && !this.allowedAdminLoggedIn.includes( state.url ) )
                    {
                        allow = false;
                        this.router.navigate(['/admin/dashboard']);
                    } else
                    {
                        // Supervisor section check!
                        if ( user.isSupervisor() && this.supervise.inSupervision && (state.url != "/supervisor/supervise/singleplayer-game" && state.url != "/supervisor/supervise/multiplayer-game") )
                        {
                            allow = false;
                            this.router.navigate(['/supervisor/supervise/' + (this.supervise.isMultiplayer ? 'multiplayer' : 'singleplayer') +  '-game']);                            
                        };
                    };
                } else
                {
                    if ( this.allowedLoggedIn.includes( state.url ) )
                    {
                        this.router.navigate(['/']);                    
                        allow = false;
                    };
                };   

                resolve(allow);
            } else
            {
                this.server.request('post', 'user/is-active-session', {}, function(data, resp) {
                    var allow = true;
                    var isLoggedIn = data.isLoggedIn;
                    //console.log("Checked logged user on server.");

                    if ( isLoggedIn )
                    {
                        obj.globals.user = new User(data.user, data.expiry);
                        var user = obj.globals.getUser();
                        if ( user.isRegularUser() && !obj.allowedLoggedIn.includes( state.url ) )
                        {
                            obj.router.navigate(['/user/dashboard']);
                            allow = false;
                        } else if ( user.isSupervisor() && !obj.allowedSupervisorLoggedIn.includes( state.url ) )
                        {
                            obj.router.navigate(['/supervisor/dashboard']);
                            allow = false;
                        } else if ( user.isAdministrator() && !obj.allowedAdminLoggedIn.includes( state.url ) )
                        {
                            allow = false;
                            obj.router.navigate(['/admin/dashboard']);
                        } else 
                        {
                            // Supervisor section check!
                            if ( user.isSupervisor() && data.addit.inSupervision && (state.url != "/supervisor/supervise/singleplayer-game" && state.url != "/supervisor/supervise/multiplayer-game") )
                            {
                                allow = false;                                
                                obj.router.navigate(['/supervisor/supervise/' + (data.addit.isMultiplayer ? 'multiplayer' : 'singleplayer') +  '-game']);                            
                            } else if ( user.isSupervisor() && !data.addit.inSupervision && (state.url == "/supervisor/supervise/singleplayer-game" || state.url == "/supervisor/supervise/multiplayer-game") )
                            {
                                allow = false;
                                obj.supervise.inSupervision = false;
                                obj.router.navigate(['/supervisor/dashboard']);   
                            }
                        }
                    } else if ( !isLoggedIn )
                    {
                        if ( 
                            obj.allowedLoggedIn.includes( state.url ) 
                            || 
                            obj.allowedSupervisorLoggedIn.includes( state.url ) 
                            ||
                            obj.allowedAdminLoggedIn.includes( state.url )
                        )                        
                        {
                            obj.router.navigate(['/']);
                            allow = false;
                        };
                    };         

                    resolve(allow);
                });  
            };            
        })
    }    
}

@Injectable()
export class SupervisorGuard implements CanActivate
{

    constructor(
        private supervise: Supervise
    ) 
    {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> 
    {
        return this.supervise.inSupervision;
    }
    
}

/**
 * Guard authority used for canActivate action check.
 */
export class GuardAuthority
{
    constructor( public redirect: boolean, public redirectURI: string, public url: string ) {}
}