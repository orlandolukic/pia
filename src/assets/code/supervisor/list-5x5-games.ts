import { Injectable, Inject, Component, Type, ComponentFactoryResolver, ViewChild, ViewContainerRef, ChangeDetectorRef, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { MultiplayerUserRequestComponent, MultiplayerComponent } from 'src/assets/components/multiplayer-user-request/multiplayer-user-request';
import { Server } from '../http-request';
import { Game5x5Component, Game5x5Interface } from 'src/assets/components/game-5x5/game-5x5';
import { MatDialog } from '@angular/material';
import { ModalAddNew5x5Component } from 'src/assets/components/modals/modal-add-new-5x5/modal-add-new-5x5.component';

/**
 * Multiplayer item.
 */
export class GameItem
{    
    public instance: Game5x5Interface;
    constructor( public component: Type<any>, public data: any, public listOfGames: List5x5Games ) {}
}

// ============================================================================

/**
 * Handling multiplayer requests.
 */
@Injectable()
export class List5x5Games implements OnInit, OnDestroy, AfterViewInit
{
    selected: number;
    deleting: boolean;
    selectedGamesComponents: Array<Game5x5Component>;

    private games: Array<GameItem>;

    loaded: boolean;
    nogames: boolean;
    gamesPlaceholder: ViewContainerRef;

    /**
     * Creates list of 5x5 games.
     * 
     * @param componentFactoryResolver Component resolver.
     * @param multiplayerPlaceholder Game placeholder ref.
     */
    constructor( 
        private componentFactoryResolver: ComponentFactoryResolver, 
        private server: Server, 
        private cdRef: ChangeDetectorRef,
        private dialog: MatDialog
    )
    {
        this.games = new Array<GameItem>();
        this.loaded = true;
        this.deleting = false;
        this.nogames = false;
        this.selected = 0;
    }

    ngOnInit()
    {

    }

    ngAfterViewInit()
    {
        
    }

    ngOnDestroy()
    {

    }

    /**
     * Deletes 5x5 games.
     */
    public delete(): void
    {
        if ( this.selected > 0 )
        {
            var x = confirm("Da li ste sigurni da zelite da obrisete igre?");
            if ( x )
            {
                this.deleting = true;
                var ids: string[] = new Array<string>();
                for ( var i=0; i<this.selected; i++ )
                {
                    ids[i] = this.selectedGamesComponents[i].data._id;
                };
                this.server.requestAuth("post", "supervisor/remove-5x5-games", {games: ids}, resp => {
                    this.deleting = false;
                    this.gamesPlaceholder.clear();
                    this.get5x5Games();
                }, err => {
                    console.error("Could not delete 5x5 games.");
                });
            };
        };
    }

    public selectedGames(): number
    {
        return this.selected;
    }

    /**
     * 
     * @param component Component of the game.
     * @param ctrl If user wants to delete multiple components.
     */
    public addSelectedGame(component: Game5x5Component, ctrl: boolean) : void
    {
        if ( this.selected == 0 )
            this.selectedGamesComponents = new Array<Game5x5Component>();

        if ( ctrl )
        {
            if ( !component.isSelected() )
            {
                component.select();            
                this.selectedGamesComponents.push(component);
                this.selected++;
            } else
            {
                this.removeFromGameComponents(component);
                component.unselect();   
            }
        } else 
        {
            // Deselect all.
            if ( this.selected > 0 )
            {
                if ( this.selected == 1 && this.selectedGamesComponents[0] == component && component.isSelected() )
                {
                    component.unselect();
                    this.selected = 0;
                    return;
                };

                for (var i=0,h=this.selectedGamesComponents.length; i<h ;i++)
                {
                    this.selectedGamesComponents[i].unselect();
                };

                this.selected = 0;
                this.addSelectedGame(component, ctrl);
                return;
            } else
            {
                this.selectedGamesComponents.push(component);
                this.selected++;
            };

            if ( !component.isSelected() )
            {         
                component.select();
            } else
            {
                component.unselect();
            }
        }
    }

    /**
     * Removes component from selected components array.
     * 
     * @param component Given component to search for.
     */
    private removeFromGameComponents( component: Game5x5Component )
    {
        var arr: Array<Game5x5Component> = new Array<Game5x5Component>();
        var ref = this.selectedGamesComponents;
        for (var i=0,h=ref.length; i<h; i++)
        {
            if ( ref[i] != component )
                arr.push( ref[i] );
        };
        this.selected--;
        this.selectedGamesComponents = arr;
    }

    /**
     * Searches for existing 5x5 games.
     */
    public get5x5Games(): void
    {
        // Get all 5x5 games from database.
        this.server.requestAuth('post', 'supervisor/get-5x5-games', {}, (data) => 
        {     
            this.loaded = true; 
            this.nogames = !data.found;         
            if ( data.found )   
            {
                this.selected = 0;
                var active = data.games.length;
                this.games = new Array<GameItem>();
                var games = data.games;
                for ( var i=0; i<active; i++ )
                {
                    var game: any = games[i];                
                    this.games[i] = new GameItem(Game5x5Component, game, this);
                };
                this.loadComponents();
            } else
            {
                this.selected = 0;
            };
        }, (resp) => {
            window.location.reload();
        });
    }

    loadComponents(): void
    {
        // Clear all games.
        this.gamesPlaceholder.clear();
        this.selected = 0;
        
        if ( this.games.length > 0 )
        {            
            var a = this.games.length;
            for ( var i=0; i<a; i++ )
            {
                console.log(i);
                var componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.games[i].component);
                var componentRef = this.gamesPlaceholder.createComponent(componentFactory);
                console.log(componentRef);
                var m: Game5x5Interface = (<Game5x5Interface>componentRef.instance);
                m.data = this.games[i].data;                
                m.listOfGames = this;
                m.server = this.server;                
                this.games[i].instance = m;
                m.onUpdatedData();
            };
        };
    }

    /**
     * Opens modal dialog for user to add new 5x5 game.
     */
    public add(): void
    {
        this.dialog.open(ModalAddNew5x5Component, {            
            disableClose: true,
            width: "550px",
            backdropClass: "backdrop",
            closeOnNavigation: false
        }).afterClosed().subscribe(() => {
            this.get5x5Games();
        });
    }
}