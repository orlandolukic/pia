import { SuperviseUser } from './supervise-user';

/**
 * Game type subdivision.
 */

 export interface SuperviseSingleplayerGame
 {
    user: SuperviseUser;
 }