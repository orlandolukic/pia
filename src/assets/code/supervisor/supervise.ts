import { Socket } from '../connection/socket';
import { Injectable } from '@angular/core';
import { Globals } from '../globals';
import { Server } from '../http-request';
import { Subject, Observable } from 'rxjs';
import { SuperviseSingleplayerGame } from './game-types';
import { SuperviseUser } from './supervise-user';
import { SuperviseGame } from './supervisor-util';

/**
 * Class for managing supervision for the supervisor.
 */
@Injectable()
export class Supervise extends Socket
{
    isMultiplayer: boolean;
    inSupervision: boolean;

    singleplayer: SuperviseSingleplayerGame = {
        user: new SuperviseUser()
    };
    game: SuperviseGame;

    constructor(
        globals: Globals,
        server: Server
    )
    {
        super(globals, server); 
        this.inSupervision = false;   
        this.isMultiplayer = false;
        this.game = new SuperviseGame();
    }

    private disconnect(): void
    {
        var username: string = this.globals.getUser().getData().username;
        this.sendEvent("supervisor-logout", username);
        this.socketDisconnect();
    }

    public cleanUp(): void
    {
        this.disconnect();
        this.inSupervision = false;
        this.isMultiplayer = false;
    }


}