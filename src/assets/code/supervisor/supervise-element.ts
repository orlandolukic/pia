import { MatButton, MatInput } from '@angular/material';

/**
 * Supervision element.
 */
export class SuperviseElement
{
    forSupervision: boolean;
    accepted: boolean;

    name: string;
    buttonAccept: MatButton;
    buttonDecline: MatButton;
    input: MatInput;

    constructor() 
    {
        this.forSupervision = false;
        this.accepted = false;
    }

    public setName(name: string): void
    {
        this.name = name;
    }

    public setButtons(accept: MatButton, decline: MatButton): void
    {
        this.buttonAccept = accept;
        this.buttonDecline = decline;
    }

    public setInput(input: MatInput): void
    {
        this.input = input;
    }

    public isDisabled(): boolean
    {
        return !this.forSupervision;
    }

    public isDisabledAcceptButton(): boolean
    {
        return !this.forSupervision || this.accepted;
    }

    public isDisabledDeclineButton(): boolean
    {
        return !this.forSupervision || !this.accepted;
    }
}