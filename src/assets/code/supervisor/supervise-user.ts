/**
 * User data for supervise session time.
 */
export class SuperviseUser
{
    public username: string;
    public name: string;
    public surname: string;
    public image: string;
    public email: string;
    public profession: string;

    constructor() {}

    public getFullName(): string
    {
        return this.name + " " + this.surname;
    }

    public getImage(): string
    {
        return this.image;
    }

    public getUsername(): string
    {
        return this.username;
    }

    public getEmail(): string
    {
        return this.email;
    }

    public getProfession(): string
    {
        return this.profession;
    }
}