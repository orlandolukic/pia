import { Server } from '../http-request';
import { MatSelectChange, MatDialogRef, MatDialog } from '@angular/material';
import { EventEmitter } from '@angular/core';
import { ModalAddNewTrophieComponent } from 'src/assets/components/modals/modal-add-new-trophie/modal-add-new-trophie.component';

/**
 * Manages trophie game handling.
 */
export class TrophieHandling
{
    private trophiesCount: number;
    private loading: boolean;
    private trophies: Array<any>;
    private activeTrophie: any;
    private visibleTrophiePreview: boolean;
    selectValue: string;

    constructor( private server: Server, private dialogRef: MatDialog )
    {
        this.trophiesCount = 0;
        this.loading = false;
        this.visibleTrophiePreview = false;
        this.selectValue = "-1";
    }

    public hasTrophies(): boolean
    {
        return this.trophiesCount > 0;
    }

    public isLoadingTrophie(): boolean
    {
        return this.loading;
    }

    public getActiveTrophie(): any
    {
        return this.activeTrophie;
    }

    public shouldPreview(): boolean
    {
        return this.visibleTrophiePreview;
    }

    public fetchTrophies(): void
    {
        this.activeTrophie = null;
        this.trophies = null;
        this.visibleTrophiePreview = false;
        this.selectValue = "-1";
        this.server.requestAuth("post", "supervisor/get-trophies", {}, resp => {
            if ( resp.gameCount > 0 )
            {                
                this.trophiesCount = resp.gameCount;
                this.trophies = resp.games;                
            } else
            {
                this.trophies = null;                
            }
        }, err => {
            console.log("Could not fetch trophie games.");
        });
    }

    public getAllTrophies(): Array<any>
    {
        return this.trophies;
    }

    public select(event: MatSelectChange): void
    {
        if ( event.value == -1 )
        {
            this.visibleTrophiePreview = false;
        } else
        {
            for (var i=0; i<this.trophies.length; i++)
            {
                if ( this.trophies[i]._id == event.value )
                {
                    this.activeTrophie = this.trophies[i];       
                    this.visibleTrophiePreview = true;    
                    break;         
                }
            };
        }
    }

    public getSelectValue(): string
    {
        return this.selectValue;
    }

    /**
     * Deletes actual trophie game.
     */
    public delete(): void
    {
        if ( this.activeTrophie )
        {
            var x = confirm("Da li ste sigurni da zelite da obrisete ovaj primer igre \"Pehar\"?");
            if ( x )
            {
                this.server.requestAuth("post", "supervisor/delete-trophie-game", { id: this.activeTrophie._id }, resp => {
                    this.selectValue = "-1";
                    this.visibleTrophiePreview = false;
                    this.fetchTrophies();
                }, err => {
                    console.error("Could not delete trophie game.");
                });
            };
        }
    }

    public add(): void
    {
        this.dialogRef.open(ModalAddNewTrophieComponent, {
            backdropClass: "backdrop",
            disableClose: true,
            closeOnNavigation: false,
            width: "500px"
        }).afterClosed().subscribe(() => {
            this.fetchTrophies();
        });
    }
}