/**
 * Class for user request.
 */
export class UserRequests<T>
{
    private arr: Array<any>;

    constructor()
    {
        this.arr = new Array<any>();
    }

    public exists(username: string): boolean
    {
        for (var i=0; i<this.arr.length; i++)
            if ( this.arr[i].username == username )
                return true;
        return false;
    }

    public add( username: string, name: string, surname: string, image: string, index: number ): any
    {
        var el: any = {
            username: username, 
            name: name,
            surname: surname,
            image: image,
            index: index
        };
        this.arr.push(el);
        return el;
    }

    public addRequest( req: T )
    {
        this.arr.push(req);
    }

    public remove(username: string): void
    {
        var el: Array<any> = new Array<any>();
        for (var i=0,m=0; i<this.arr.length; i++)
        {
            if ( this.arr[i].username != username )
                el[m++] = this.arr[i];
        };
        this.arr = el;
    }

    public getUserRequest(username: string): any
    {
        for (var i=0; i<this.arr.length; i++)
            if ( this.arr[i].username == username )
                return this.arr[i];
        return null;
    }
}

export interface SingleplayerRequest
{
    username: string;
    name: string;
    surname: string;
    image: string;
    index: number;
}

export interface MultiplayerRequest
{
    username: string;
    name: string;
    surname: string;
    image: string;
    index: number;
}