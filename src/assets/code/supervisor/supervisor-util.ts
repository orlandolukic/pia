/**
 * Utility classes.
 */
export class SuperviseGame
{
    private static NAMES: string[] = [
        "Anagram",
        "Moj broj",
        "5 x 5",
        "Zanimljiva geografija",
        "Pehar"
    ];

    private index: number;

    constructor() 
    {
        this.index = 0;    
    }

    /**
     * Gets name of the current game.
     */
    public getName(): string
    {
        return SuperviseGame.NAMES[this.index];
    }

    /**
     * Changes current game.
     */
    public goToTheNextGame(): void
    {
        this.index++;
    }

    /**
     * Gets progress in %.
     */
    public getProgress(): number
    {
        return (this.index / 5) * 100;
    }

    public setCurrentGame(index: number): void
    {
        this.index = index;
    }


}