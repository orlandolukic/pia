import { RegistrationPanel } from './registration-panel';
import { MatButton, MatDialog } from '@angular/material';
import { RegisterComponent } from 'src/app/register/register.component';
import { DialogOverviewExampleDialog } from './modal-registration';
import { Server } from '../http-request';

export interface DialogData
{
    
}

/**
 * Handling all registration process.
 */
 export class Registration
 {
     [x: string]: any;
    private arr : Array<RegistrationPanel>;
    private NUM : number; 
    private current : number;
    private checking : boolean;
    private ready : boolean;
    public button_submit : MatButton;
    private loaded : boolean;
    private correction: boolean;
    private registering: boolean;

    private allowchange : boolean;
    private formData: FormData;

    constructor( public dialog : MatDialog, private server : Server ) 
    {
        /*
        if ( Registration.instance != null )
            throw new Error("Registration handling is already running.");
            */

        this.loaded = false;
        this.arr = new Array<RegistrationPanel>();
        this.NUM = 0;
        this.current = 0;
        this.checking = false;
        this.ready = false;
        this.allowchange = true;
        this.correction = false;
        this.registering = false;
        this.formData = new FormData();
        /*
        this.hasError = false;
        Registration.instance = this;
        */
    }

    /**
     * Sets loaded indicator for this component.
     */
    public setLoaded() : void
    {
        this.loaded = true;
    }

    /**
     * Checks if body is loaded.
     */
    public isLoadedBody() : boolean
    {
        return this.loaded;
    }

    /**
     * Sets panel as open.
     * 
     * @param ind Panel's index.
     */
    public setVisible(ind : number) : void
    {
        this.arr[ind].setOpened();
    }

    /**
     * Sets panel as closed.
     * 
     * @param ind Panel's index.
     */
    public resetVisible(ind : number) : void
    {
        this.arr[ind].setClosed();
    }

    /**
     * 
     * Registers new registration panel.
     * 
     * @param regp New registration panel.
     */
    public register( regp : RegistrationPanel ) : void
    {
        this.arr.push( regp );
        this.NUM++;
    }

    /**
     * Checks for errors on current panel.
     */
    public hasErrors() : boolean
    {
        if ( this.current != -1 )
            return this.arr[this.current].hasErrors();
        return false;
    }

    /**
     * Checks if certain error is active for given panel.
     * 
     * @param ind Index of the panel.
     * @param err Error index.
     */
    public isError(ind : number, err : number) : boolean
    {
        return this.arr[ind].isError(err);
    }

    /**
     * Returns indicator if angular is checking data.
     */
    public isCheckingData() : boolean
    {
        return this.checking;
    }

    /**
     * Checks if whole registration is ready.
     */
    public isValid() : boolean
    {
        var f = true;
        for (var i=0; i<this.arr.length; i++)
            f = f && this.arr[i].isValidData();
        return f;
    }

    /**
     * Checks if form is ready for submit action.
     */
    public isReadyForSubmit() : boolean
    {
        return this.isValid() && this.ready;
    }

    /**
     * Checks if current panel is the last one.
     */
    private isLastOne() : boolean
    {
        return this.current+1 == this.NUM;
    }

    /**
     * Checks if data are loaded from server.
     */
    public isLoadingData() : boolean
    {
        if ( this.current != -1 )
            return this.arr[this.current].isLoadingData();

        return false;
    }

    /**
     * Focuses first component.
     */
    public focusFirst() : void
    {
        var f = this;
        setTimeout(function() {
            f.arr[0].focus();
        }, 200);
    }

    /**
     * Gets number of active registration panels.
     */
    public getRegistrationPanelNumber() : number
    {
        return this.NUM;
    }

    /**
     * Gets wanted panel.
     * 
     * @param ind Index of the panel.
     */
    public getRegistrationPanel(ind : number) : RegistrationPanel
    {
        return this.arr[ind];
    }

    /**
     * Sets panelNo as new opened panel.
     * 
     * @param panelNo New panel to focus.
     */
    public setPanel(panelNo : number) : void
    {
        if ( this.loaded )
        {
            console.log("opened " + panelNo + ", current " + this.current);
            console.log("===================");

            // Disable all panels except current one.
            for ( var i=0; i<this.NUM; i++ )
                if ( i != panelNo )
                {
                    this.arr[i].setClosed();
                    this.arr[i].getPanel().disabled = true;
                };

            // Set correction variable.
            if ( this.ready )
                this.correction = true;

            this.current = panelNo;
            this.arr[this.current].setOpened();
            this.arr[this.current].focus();
            this.checking = false;
            this.ready = false;
        };
    }

    /**
     * No panels to preview.
     */
    public exitPanel() : void
    {
        console.log("panel close, current = " + this.current);
        this.arr[this.current].setClosed();
        this.current = -1;
    }

    /**
     * Gets current panel index.
     */
    public getCurrentPanelIndex() : number
    {
        return this.current;
    }

    /**
     * Checks if process is in registration mode.
     */
    public isInRegistration() : boolean
    {
        return this.registering;
    }

    /**
     * Verification.
     */
    public verify() : void
    {
        var current = this.current;
        var next = current+1;


        this.checking = true;
        if ( current != -1 && this.arr[current].verify(this) )
        {
            this.onSuccessValidation(current);
        };
        this.checking = false;
    }

    /**
     * Callback on success validation.
     * 
     * @param current Current active panel.
     * @param change Whether to change current active panel.
     */
    public onSuccessValidation( current : number, change : boolean = false ) : void
    {
        if ( current == -1 )
            current = this.current;
        var next = current + 1;

        if ( next != this.NUM && this.arr[next].getPanel().disabled )
            this.arr[next].getPanel().disabled = false;
        
        // Close current panel.
        this.arr[current].getPanel().close();

        // Go to the next one.
        if ( next != this.NUM )
        {
            if ( !this.correction )
                this.arr[next].getPanel().open();
            else
            {
                for (var i=0; i<this.NUM; i++)
                    this.arr[i].getPanel().disabled = false;
            }
            this.correction = false;
            current++;
        } else
        {
            // Enable button register!
            if ( !this.hasErrors() )
            {
                // Ready for submit action.
                this.ready = true;

                // Enable all panels.
                for (var i=0; i<this.NUM; i++)
                    this.arr[i].getPanel().disabled = false;
            }
        };

        if ( change ) 
            this.current = next;
    }

    /**
     * Registration process.
     */
    public registerUser() : void
    {
        // Disable all panels.
        for (var i=0; i<this.NUM; i++)
            this.arr[i].getPanel().disabled = true;

        this.ready = false;
        this.registering = true;
        var obj = this;
        
        var g: any = {};
        for (var i=0; i<this.NUM; i++)
            this.arr[i].collect(g);

        console.log(g);

        this.server.upload('user/upload-image', g.image, function(resp) {
            g.image = resp.path;
            g.isValid = false;
            g.accType = 0;
            obj.server.request('post', 'user/register-user', g, function(resp) {
                if ( resp.registered )
                {
                    // Open modal dialog...
                    const dialogRef = obj.dialog.open(DialogOverviewExampleDialog, {
                        width: '300px',
                        disableClose: true,
                        autoFocus: false,
                        backdropClass: 'backdrop'
                    });
            
                    dialogRef.afterClosed().subscribe(result => {
                        console.log('The dialog was closed');
                        //this.animal = result;
                    });
                    //this.server.request();
                };
            });
        }, function(err) {
            alert("Registration error - Picture could not be uploaded!");
            window.location.reload();
        });
    }
 }