import { RegistrationPanel } from './registration-panel';
import { ViewChild } from '@angular/core';
import { MatExpansionPanel } from '@angular/material';
import { Registration } from './registration';

/**
 * Basic registration panel.
 */
 export class BasicRegistrationPanel extends RegistrationPanel
 {
    private ERROR_NAME : number = 0;
    private ERROR_SURNAME : number = 1;
    private ERROR_JMBG : number = 2;
    private ERROR_PROFESSION : number = 3; 

     /**
      * Function which verifies this form.
      */
     public verify( reg : Registration ): boolean {
        var name = this.comps.name.value;
        var surname = this.comps.surname.value;
        var jmbg = this.comps.jmbg.value;
        var male = this.comps.male.checked;
        var profession = this.comps.profession.value;

        this.errors[this.ERROR_NAME] = !new RegExp(/^[a-zA-ZČčĆćŠšĐŽž]{2,}$/).test(name);       
        this.errors[this.ERROR_SURNAME] = !new RegExp(/^[a-zA-ZČčĆćŠšĐđŽž]{2,}$/).test(surname);
        this.errors[this.ERROR_PROFESSION] = !new RegExp(/^[a-zA-ZČčĆćŠšĐđŽž]{2}[a-zA-ZČčĆćŠšĐđŽž ]*$/).test(profession);

        // 31 days : (0[1-9]|[1-2][0-9]|3[0-1])
        // 30 days : (0[1-9]|[1-2][0-9]|30)
        // 28 days : (0[1-9]|[1-2][0-8])
        var jmbg_regexp;
        if ( male )
            jmbg_regexp = new RegExp(/((0[1-9]|[1-2][0-9]|3[0-1])(0(1|3|5|7|8)|1(0|2))|(0[1-9]|[1-2][0-9]|30)(0(2|4|6|9)|11)|(0[1-9]|[1-2][0-8])02)(9[0-9]{2}|0[0-9]{2})[0-9]{2}[0-4][0-9]{2}[0-9]/);
        else
            jmbg_regexp = new RegExp(/((0[1-9]|[1-2][0-9]|3[0-1])(0(1|3|5|7|8)|1(0|2))|(0[1-9]|[1-2][0-9]|30)(0(2|4|6|9)|11)|(0[1-9]|[1-2][0-8])02)(9[0-9]{2}|0[0-9]{2})[0-9]{2}[5-9][0-9]{2}[0-9]/);
 
        this.errors[this.ERROR_JMBG] = jmbg.length < 13 || !jmbg_regexp.test(jmbg);
        
        return !this.hasErrors();
     } 
     
     /**
     * Focuses component.
     */
    public focus(): void 
    {
        this.comps.name.focus();
    }

    /**
     * Collects data.
     * 
     * @param fd FormData for registration request.
     */
    public collect(obj: any): void 
    {
        var name = this.comps.name.value;
        var surname = this.comps.surname.value;
        var jmbg = this.comps.jmbg.value;
        var male = this.comps.male.checked;
        var profession = this.comps.profession.value;

        obj.name = name;
        obj.surname = surname;
        obj.jmbg = jmbg;
        obj.sex = male ? 0 : 1;
        obj.profession = profession;

        //console.log(fd);
    }
 }