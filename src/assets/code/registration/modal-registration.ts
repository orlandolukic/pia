import { Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from './registration';

@Component({
    selector: 'modal-registration',
    templateUrl: 'modal-registration.html',
})
export class DialogOverviewExampleDialog 
{
    constructor(
        public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

    public close() : void
    {
        this.dialogRef.close();
    }

}