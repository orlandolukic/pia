import { RegistrationPanel } from './registration-panel';
import { Server } from '../http-request';
import { MatExpansionPanel } from '@angular/material';
import { Registration } from './registration';
import { sha256 } from 'js-sha256';

export class AccountRegistrationPanel extends RegistrationPanel
{
    private usernameLooked: boolean;
    //private usernameExists: boolean;
    private username: string;

    private ERROR_PASSWORD_CRITERIA : number = 0;
    private ERROR_PASSWORD_NOT_MATCH : number = 1;
    private ERROR_USERNAME_TAKEN : number = 2;
    private ERROR_USERNAME_INVALID : number = 3;
    private ERROR_EMAIL : number = 4;

    constructor( p : MatExpansionPanel, g : number, obj : any, private server : Server ) 
    {
        super(p,g,obj);
        this.usernameLooked = false;
        //this.usernameExists = false;
        this.username = null;
    }

    /**
     * Verifies account panel.
     */
    public verify( reg : Registration ) : boolean {
        var ret = !this.hasErrors();
        var username = this.comps.username.value;
        var email = this.comps.email.value;
        var password = this.comps.password.value;
        var passwordConfirm = this.comps.password_confirm.value;
        var unpopulated = username === "" || email === "" || password === "" || passwordConfirm === "";
        var panelIndex = reg.getCurrentPanelIndex();

        this.errors[this.ERROR_USERNAME_INVALID] = !new RegExp(/^[a-zA-Z_][a-zA-Z0-9_]+$/).test(username);

        if ( username.length >= 2 && !unpopulated )
        {
            // Go and check.
            if ( !this.usernameLooked || this.usernameLooked && username !== this.username )
            {
                this.isLoading = true;
                var myself = this;
                this.errors[this.ERROR_USERNAME_TAKEN] = true;
                this.server.request('post', 'user/exists', {username: username}, function(d) {
                    myself.errors[myself.ERROR_USERNAME_TAKEN] = d.exists;
                    myself.isLoading = false;                
                    myself.prepared = !myself.hasErrors();
                    myself.usernameLooked = true;
                    myself.username = username;
                    if ( !myself.hasErrors() )
                        reg.onSuccessValidation(panelIndex, true);
                });
            };
        }; 
        
        this.errors[this.ERROR_EMAIL] = !this.validateEmail(email);
        this.errors[this.ERROR_PASSWORD_CRITERIA] = !(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/.test(password)) || password.length > 12 || password.length < 8 || new RegExp("[a-zA-Z]{3,}").test(password) || !new RegExp("^[a-zA-Z]").test(password);
        this.errors[this.ERROR_PASSWORD_NOT_MATCH] = password !== passwordConfirm;
        this.prepared = !this.hasErrors();

        return !this.hasErrors();
    }


    /**
     * Validates e-mail.
     * 
     * @param email Given email.
     */
    private validateEmail(email : string) : boolean 
    {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    /**
     * Focuses component.
     */
    public focus(): void 
    {
        this.comps.username.focus(); 
    }


    public collect( obj : any ) : void
    {
        var username = this.comps.username.value;
        var email = this.comps.email.value;
        var password = this.comps.password.value;

        obj.username = username;
        obj.email = email;
        obj.password = sha256(password);
    }
}