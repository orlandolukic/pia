import { RegistrationPanel } from './registration-panel';
import { Registration } from './registration';
import { MatExpansionPanel } from '@angular/material';
import { Server } from '../http-request';

/**
 * Handling upload registration panel.
 */
export class UploadRegistrationPanel extends RegistrationPanel
{
    private ERROR_IMAGE_EMPTY : number = 0;
    private ERROR_IMAGE_SIZE : number = 1;

    private hasImage : boolean;
    private src : string;
    private reg : Registration;
    private image : any;
    private img: any;
    private file: File;

    constructor( p : MatExpansionPanel, g : number, obj : any, reg : Registration ) 
    {
        super(p,g,obj);
        this.hasImage = false;
        this.src = null;
        this.reg = reg;
        this.image = document.querySelector('#uploadImagePreview');
        this.img = null;
    }
    
    /**
     * Verification of the image.
     * 
     * @param reg Registration object.
     */
    public verify(reg: Registration): boolean {
        var f = this.hasImage;
        var g = this.checkImageSize();
        this.errors[this.ERROR_IMAGE_EMPTY] = !f;
        this.errors[this.ERROR_IMAGE_SIZE] = !g;
        return !this.hasErrors();
    }

    /**
     * Checks image size.
     */
    private checkImageSize() : boolean
    {
        if ( !this.hasImage )
            return true;
        return this.image.width <= 300 && this.image.height <= 300;
    }

    /**
     * Checks if image is put.
     */
    public isReadyForUpload() : boolean
    {
        return this.hasImage;
    }

    /**
     * Gets image's src.
     */
    public getImage() : string
    {
        return this.src;
    }

    /**
     * Handler on image change.
     */
    public imageChanged() : void
    {
        var srcResult : any;
        const inputNode: any = document.querySelector('#file');
        var url = URL.createObjectURL(inputNode.files[0]);
        this.file = inputNode.files[0];
        var name = inputNode.files[0].name;

        var myself = this;
        var cond: boolean;
        this.img = new Image();
        this.img.onload = function() 
        {
            myself.errors[myself.ERROR_IMAGE_SIZE] = cond = myself.img.width > 300 || myself.img.height > 300;
            if ( cond )
            {
                inputNode.value = "";
                myself.hasImage = false;
                myself.image.src = "";
                myself.src = null;
                myself.isLoading = false;
                return;
            };
            myself.hasImage = true;
            myself.image.src = url;
            myself.src = name;
            myself.isLoading = false;
        };

        this.isLoading = true;
        this.img.src = url;
    }

    /**
     * Focuses component.
     */
    public focus(): void 
    {
        
    }

    public collect( obj : any ) : void
    {
        obj.image = new FormData();
        obj.image.append('file', this.file, this.file.name);
    }
    
}