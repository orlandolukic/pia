import { MatExpansionPanel } from '@angular/material';
import { Registration } from './registration';

/**
 * Registration panel.
 */
 export abstract class RegistrationPanel
 {
    // =============================================================
    //      NON-STATIC DATA
    // =============================================================

    protected panel : MatExpansionPanel;
    protected isChecking : boolean;
    protected isLoading : boolean;
    protected isVisible : boolean;
    protected errors : Array<boolean>;
    protected prepared : boolean;
    protected comps : any;

    /**
     * Create registration panel.
     */
    constructor( panel : MatExpansionPanel, errnum : number, comps : any ) 
    {
        this.panel = panel;
        this.isChecking = false;
        this.isLoading = false;
        this.isVisible = false;
        this.prepared = false;
        this.errors = new Array<boolean>();
        this.comps = comps;
        for (var i=0; i<errnum; i++)
            this.errors.push(false);
    }

    /**
     * Sets panel as open.
     */
    public setOpened() : void
    {
        this.isVisible = true;
    }

    /**
     * Sets panel as closed.
     */
    public setClosed() : void
    {
        this.isVisible = false;
    }

    /**
     * Gets panel.
     */
    public getPanel() : MatExpansionPanel
    {
        return this.panel;
    }

    /**
     * Sets checking.
     * 
     * @param val New value of checking parameter.
     */
    public setChecking(val : boolean) : void
    {
        this.isChecking = val;
    }

    /**
     * Checks if angular is verifying data.
     */
    public isCheckingData() : boolean
    {
        return this.isChecking;
    }

    /**
     * Checks if data are loaded from server.
     */
    public isLoadingData() : boolean
    {
        return this.isLoading;
    }

    /**
     * Checks if given error is active.
     * 
     * @param num Error's index.
     */
    public isError(num : number) : boolean
    {
        return this.isVisible && this.errors[num-1];
    }

    /**
     * Checks if component is valid.
     */
    public isValidData() : boolean
    {
        // this.isValid
        return !this.hasErrors();
    }

    /**
     * Function which checks errors within panel.
     */
    public hasErrors() : boolean
    {
        var g = false;
        for ( var i=0; i<this.errors.length && !g; i++ )
            g = g || this.errors[i];
        return g;
    }

    /**
     * Function which verifies panel.
     */
    public abstract verify( reg : Registration ) : boolean;

    /**
     * Focuses component on show.
     */
    public abstract focus() : void;

    /**
     * Collects set of data to send to server.
     * 
     * @param fd FormData to send to server.
     */
    public abstract collect( fd : any ) : void;
 }