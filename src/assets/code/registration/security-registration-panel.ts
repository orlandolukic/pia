import { RegistrationPanel } from './registration-panel';
import { Registration } from './registration';

/**
 * Handling security registration panel.
 */
export class SecurityRegistrationPanel extends RegistrationPanel
{
    private ERROR_QUESTION_INVALID: number = 0;
    private ERROR_ANSWER_INVALID: number = 1;

    /**
     * Verifies security question and answer.
     * 
     * @param reg Registration object.
     */
    public verify(reg: Registration): boolean 
    {
        var question: string = this.comps.secq.value;
        var answer: string = this.comps.seca.value;
        this.errors[this.ERROR_QUESTION_INVALID] = question.length < 5;
        this.errors[this.ERROR_ANSWER_INVALID] = answer.length < 3;
        return !this.hasErrors();
    }    
    
    
    /**
     * Focuses this panel on show.
     */
    public focus(): void 
    {
        this.comps.secq.focus();    
    }

    public collect( obj : any ) : void
    {
        var question: string = this.comps.secq.value;
        var answer: string = this.comps.seca.value;

        obj.secq = question;
        obj.seca = answer;
    }
}