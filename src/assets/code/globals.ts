import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationStart } from '@angular/router';
import { User } from './user';
import { MultiplayerGame } from './multiplayer-game';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class Globals
{
    private static instance: Globals;
    public static getInstance(): Globals
    {
        return Globals.instance;
    }

    public user: User;

    private title: string = "ETF Kvizkoteka";
  
    /*
    public nodeServer : string = "http://192.168.15.128:8080";
    public angular : string = "http://192.168.15.128:4200";
    public socketServer: string = "http://192.168.15.128:8081";
    */

    public nodeServer : string = "http://localhost:8080";
    public angular : string = "http://localhost:4200";
    public socketServer: string = "http://localhost:8081";
   

    public inLogin : boolean = false;
    public errorCredentials : boolean = false;
    public inGame: number;
    private logoutSubject: Subject<any>;

    constructor( private titleService: Title, private router: Router ) 
    {
        this.user = null;
        Globals.instance = this;
        this.logoutSubject = new Subject<any>();
        this.inGame = -1;
    }

    public getLogoutSubject(): Subject<any>
    {
        return this.logoutSubject;
    }

    public getLogoutObservable(): Observable<any>
    {
        return this.logoutSubject.asObservable();
    }

    public clearLogoutSubject(): void
    {
        this.logoutSubject = new Subject<any>();
    }

    /**
     * Gets current user.
     */
    public getUser() : User
    {
        return this.user;
    }

    /**
     * Changes title of the document.
     * 
     * @param title New title to add.
     */
    public renameDocumentTitle( title: string ) : void
    {
        this.titleService.setTitle( title + " - " + this.title );
    };

    /**
     * Function is called after user is logged out.
     */
    public logout() : void
    {
        //this.resetMultiplayerGame();
    }
}