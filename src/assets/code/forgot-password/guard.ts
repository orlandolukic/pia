import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { ForgotPasswordComponent } from 'src/app/forgot-password/forgot-password.component';

@Injectable()
export class ConfirmDeactivateForgotPasswordGuard implements CanDeactivate<ForgotPasswordComponent> {
    
    canDeactivate(target: ForgotPasswordComponent) {
        if (target.inProcess) {
            return window.confirm('Da li zaista želite da odustanete?');
        };
        return true;
    }

}

/*
// hasChanges - function in 'ViewthatyouwantGuard' which will return true or false based on unsaved changes

// And in your routing file provide root like 
{path:'rootPath/', component: ViewthatyouwantGuard, canDeactivate:[ConfirmDeactivateGuard]},

// Last but not least, also this guard needs to be registered accordingly:
@NgModule({
    ...
    providers: [
        ...
        ConfirmDeactivateGuard
    ]
 })
 export class AppModule {}
 */