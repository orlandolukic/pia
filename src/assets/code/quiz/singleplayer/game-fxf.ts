import { Game } from '../game';
import { QuizFxfComponent } from 'src/assets/components/quiz/quiz-fxf/quiz-fxf.component';
import { SingleplayerGame } from '../../user/singleplayer-request';

/**
 * Singleplayer game fxf.
 */
export class GameFxF extends Game
{
    private instance: QuizFxfComponent;

    public onFinished(): void {
        
    }
    public onPreview(): void {
        this.instance.onPreview();
    }

    public appendEventListeners(controller: SingleplayerGame): void {
 
    }    
    
    public removeEventListeners(controller: SingleplayerGame): void {
   
    }

    public submitData(controller: SingleplayerGame, request: string, data: any): void {        
        controller.addEventListener("ctrl-" + request, (data: any) => {
            controller.removeEventListener("ctrl-" + request, null);
            this.instance.fromServer(false, request, data);
        });
        controller.sendRegularRequest(request, data);
    }

    public onTimeout(): void {
        this.instance.onTimeout();
    }

    public onLoad(): void {
       this.instance = <QuizFxfComponent>this.component;
    }

    public onDestroy(): void {
        
    }   
}