import { Game } from '../game';
import { QuizAnagramComponent } from 'src/assets/components/quiz/quiz-anagram/quiz-anagram.component';
import { SingleplayerGame } from '../../user/singleplayer-request';

/**
 * Anagram game.
 */
export class AnagramGame extends Game
{
    private instance: QuizAnagramComponent;

    public onFinished(): void {
        this.instance.onFinished();    
    }

    public onPreview(): void {       
        this.instance.onPreview();    
    }

    public onLoad(): void {        
        this.instance = <QuizAnagramComponent>this.component;
    }

    public onDestroy(): void {
        
    }

    public onTimeout(): void {
        this.instance.onTimeout();
    }
    
    public appendEventListeners(controller: SingleplayerGame): void {
         
    }    
    
    public removeEventListeners(controller: SingleplayerGame): void {
    
    }
    
    public submitData(controller: SingleplayerGame, request:string, data: any): void {

        switch( request )
        {
        case "sp-anagram-game-info":
            controller.addEventListener("ctrl-sp-anagram-game-info", (data: any) => {
                controller.removeEventListener("ctrl-sp-anagram-game-info", null);
                this.instance.fromServer(false, "init", data);
            });
            controller.sendRegularRequest(request, data);            
            break;

        case "sp-anagram-finished":
            controller.addEventListener("ctrl-sp-anagram-finished", (data: any) => {
                controller.removeEventListener("ctrl-sp-anagram-finished", null);
                this.instance.fromServer(false, "finished", data);
            });
            controller.sendRegularRequest(request, data);            
            break;
        }
    }

    
}