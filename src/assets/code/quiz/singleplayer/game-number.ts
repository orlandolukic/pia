import { Game } from '../game';
import { QuizNumberComponent } from 'src/assets/components/quiz/quiz-number/quiz-number.component';
import { SingleplayerGame } from '../../user/singleplayer-request';

/**
 * Anagram game.
 */
export class GameNumber extends Game
{
    private instance: QuizNumberComponent;

    public onFinished(): void {
        this.instance.onFinished();    
    }

    public onPreview(): void {
        this.instance.onPreview();    
    }

    public onLoad(): void {        
        this.instance = <QuizNumberComponent>this.component;
    }

    public onDestroy(): void {
       
    }

    public onTimeout(): void {
        this.instance.onTimeout();
    }
    
    public appendEventListeners(controller: SingleplayerGame): void {}   
    public removeEventListeners(controller: SingleplayerGame): void {}
    
    public submitData(controller: any, request: string, data: any): void 
    {
        switch(request)
        {
        case "sp-number-game-info":
            controller.addEventListener("ctrl-sp-number-game-info", (data: any) => {
                controller.removeEventListener("ctrl-sp-number-game-info", null);
                this.instance.fromServer(false, "init", data);
            });
            controller.sendRegularRequest(request, data);     
            break;

        case "sp-number-stop-spinning":
            controller.addEventListener("ctrl-sp-number-stop-spinning", (data: any) => {
                controller.removeEventListener("ctrl-sp-number-stop-spinning", null);
                this.instance.fromServer(false, "stopSpinning", data);
            });
            controller.sendRegularRequest(request, data); 
            break;

        case "sp-number-finish-game":
            controller.addEventListener("ctrl-sp-number-finish-game", (data: any) => {
                controller.removeEventListener("ctrl-sp-number-finish-game", null);
                this.instance.fromServer(false, "finish", data);
            });
            controller.sendRegularRequest(request, data); 
            break;

        };
    }

    
}