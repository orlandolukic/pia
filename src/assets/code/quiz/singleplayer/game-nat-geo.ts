import { Game } from '../game';
import { QuizNatGeoComponent } from 'src/assets/components/quiz/quiz-nat-geo/quiz-nat-geo.component';
import { SingleplayerGame } from '../../user/singleplayer-request';

export class GameNatGeo extends Game
{
    private instance: QuizNatGeoComponent;

    public appendEventListeners(controller: SingleplayerGame): void {
        controller.addEventListener("ctrl-sp-supervisor-finished", (data: any) => { 
            this.instance.fromServer(false, "ctrl-sp-supervisor-finished", data);
        });
    }    
    
    public removeEventListeners(controller: SingleplayerGame): void {
        
    }

    public submitData( controller: SingleplayerGame, request: string, data: any ): void {
        controller.addEventListener("ctrl-" + request, (data: any) => {
            controller.removeEventListener("ctrl-" + request, null);
            this.instance.fromServer(false, request, data);
        });
        controller.sendRegularRequest(request, data);
    }

    public onTimeout(): void {
        this.instance.onTimeout();
    }

    public onFinished(): void {
        this.instance.onFinished();
    }

    public onPreview(): void {
        this.instance.onPreview();
    }

    public onLoad(): void {
        this.instance = <QuizNatGeoComponent>this.component;
    }

    public onDestroy(): void {
        
    }


}