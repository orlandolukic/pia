import { Game } from '../game';
import { QuizTrophieComponent } from 'src/assets/components/quiz/quiz-trophie/quiz-trophie.component';
import { SingleplayerGame } from '../../user/singleplayer-request';

/**
 * Anagram game.
 */
export class GameTrophie extends Game
{
    private instance: QuizTrophieComponent;

    public onFinished(): void {
        this.instance.onFinished();    
    }

    public onPreview(): void {   
        this.instance.onPreview();    
    }

    public onLoad(): void {        
        this.instance = <QuizTrophieComponent>this.component;
    }

    public onDestroy(): void {
        
    }

    public onTimeout(): void {
        this.instance.onTimeout();
    }
    
    public appendEventListeners(controller: SingleplayerGame): void {
       
    }    
    
    public removeEventListeners(controller: SingleplayerGame): void {
        
    }
    
    public submitData( controller: SingleplayerGame, request: string, data: any): void {
        controller.addEventListener("ctrl-" + request, (data: any) => {
            controller.removeEventListener("ctrl-" + request, null);
            this.instance.fromServer(false, request, data);
        });
        controller.sendRegularRequest(request, data);
    }

    
}