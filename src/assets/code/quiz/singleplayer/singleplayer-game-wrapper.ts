import { GameWrapper } from '../game-wrapper';
import { AnagramGame } from './game-anagram';
import { GameFxF } from './game-fxf';
import { GameNumber } from './game-number';
import { GameNatGeo } from './game-nat-geo';
import { GameTrophie } from './game-trophie';
import { SingleplayerGame } from '../../user/singleplayer-request';

/**
 * Handles singleplayer game on client side.
 */
export class SingleplayerGameWrapper extends GameWrapper
{
    constructor(controller: SingleplayerGame)
    {
        super(controller);
        this.games = [
            new AnagramGame(),
            new GameNumber(),
            new GameFxF(),
            new GameNatGeo(),
            new GameTrophie()
        ];
    }
    
    public canChangeGame(): boolean {
        throw new Error("Method not implemented.");
    }    
    
    public submitData(multiplayer: boolean, request: string, data: any): void {
        if ( multiplayer )
            return;
        this.games[this.currentGameIndex].submitData(this.controller, request, data);
    }

    protected _appendEventListeners(): void {        
        for(var i=0; i<this.games.length; i++)
            this.games[i].appendEventListeners(this.controller);
    }

    public removeEventListeners(): void {
        for(var i=0; i<this.games.length; i++)
            this.games[i].removeEventListeners(this.controller);
    } 
}