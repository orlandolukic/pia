import { Game } from './game';
import { SingleplayerGame } from '../user/singleplayer-request';
import { MultiplayerGame } from '../multiplayer-game';
import { GameManagement } from './game-management';

/**
 * Game wrapper for quiz frame. Abstract class.
 */
export abstract class GameWrapper
{
    protected currentGameIndex: number;
    protected games: Array<Game>;
    protected controller: SingleplayerGame | MultiplayerGame;

    constructor(controller: SingleplayerGame | MultiplayerGame) 
    {
        this.currentGameIndex = 0; 
        this.controller = controller;       
    }

    /**
     * Gets current game index.
     */
    public getCurrentGameIndex(): number
    {
        return this.currentGameIndex;
    }

    /**
     * Sets current game index.
     * 
     * @param index Current game index.
     */
    public setCurrentGameIndex(index: number): void
    {
        this.currentGameIndex = index;
    }

    /**
     * Checks if user can change game.
     */
    public abstract canChangeGame(): boolean;

    /**
     * Sends data to the server.
     */
    public abstract submitData(multiplayer: boolean, request: string, data: any): void;

    /**
     * Appends event listeners to the active socket.
     */
    protected abstract _appendEventListeners(): void;

    /**
     * Appends event listeners to the active socket.
     */
    public appendEventListeners(): void
    {
        this.controller.addEventListeners(() => {
            this._appendEventListeners();
        });
    }
    
    /**
     * Removes event listeners for the current game.
     */
    public abstract removeEventListeners(): void;

    /**
     * Gets current game.
     */
    public getCurrentGame(): Game
    {
        return this.games[this.currentGameIndex];
    }

    /**
     * What happens when timer ticks 0.
     */
    public onTimeout(): void
    {
        this.games[this.currentGameIndex].onTimeout();
    }

    /**
     * Gets game within scope.
     * 
     * @param index Index of the game.
     */
    public getGame(index: number): Game
    {
        return this.games[index];
    }

    /**
     * Gets games length.
     */
    public getGamesLength(): number
    {
        return this.games != null ? this.games.length : 0;
    }

    /**
     * Changes current game.
     */
    public nextGame(): void
    {
        this.games[this.currentGameIndex].onFinished();
        this.currentGameIndex++;
        if ( this.currentGameIndex == this.games.length )
            return;
        this.games[this.currentGameIndex].onPreview();
    }

    /**
     * Gets controller for the socket messages.
     */
    public getController(): SingleplayerGame|MultiplayerGame
    {
        return this.controller;
    }

    /**
     * Checks if controller is for multiplayer game.
     */
    public isMultiplayer(): boolean
    {
        return this.controller instanceof MultiplayerGame;
    }
}