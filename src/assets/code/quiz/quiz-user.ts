/**
 * Data neccessary for quiz handling.
 */
export class QuizUser
{
    public username: string;
    public name: string;
    public surname: string;
    public image: string;
    public email: string;
    public profession: string;
    public loaded: boolean;
    public points: number;

    public timeout: any;
    public showAddPoints: boolean;
    public additionOfPoints: number;

    constructor() {
        this.points = 0;
    }
}