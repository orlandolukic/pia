/**
 * Data neccessary for supervisor's preview.
 */
export class QuizSupervisor
{
    public username: string;
    public name: string;
    public surname: string;
    public image: string;
    public email: string;
    public profession: string;
    public loaded: boolean;
}