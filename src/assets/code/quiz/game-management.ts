import { Injectable, ComponentRef, ChangeDetectorRef } from '@angular/core';
import { Globals } from '../globals';
import { GameWrapper } from './game-wrapper';
import { Server } from '../http-request';
import { SingleplayerGame } from '../user/singleplayer-request';
import { MultiplayerRequests } from '../user/multiplayer-requests';
import { MultiplayerGame } from '../multiplayer-game';
import { QuizUser } from './quiz-user';
import { QuizSupervisor } from './quiz-supervisor';
import { User } from '../user';
import { SingleplayerGameWrapper } from './singleplayer/singleplayer-game-wrapper';
import { QuizComponent } from './quiz-component-interface';
import { promise } from 'protractor';
import { Subject, Observable } from 'rxjs';
import { TimeManagement } from './time-management';

/**
 * Handling game management.
 */
@Injectable()
export class GameManagement
{
    protected isInit: boolean;
    protected isStarted: boolean;
    protected onLoadSubject: Subject<void>;
    protected onLoadObservable: Observable<void>;
    protected detectorRef: ChangeDetectorRef;
    protected isLoading: boolean;
    protected multiplayer: boolean;
    protected timeout: any;
    protected time: TimeManagement;
    protected blue: QuizUser;
    protected red: QuizUser;
    protected supervisor: QuizSupervisor;
    protected wrapper: GameWrapper;
    protected turn: number;                 // turn: 0|1. 0 - Blue, 1 - Red    
    protected myself: number; 
    protected onLoadPromise: Promise<void>;
    protected onLoadPromiseResolved: boolean;
    protected onTimeoutHappened: boolean;
    canLeaveFlag: boolean;

    protected controller: SingleplayerGame | MultiplayerGame;

    constructor(        
        protected globals: Globals,
        protected server: Server,   
        private cSingleplayer: SingleplayerGame,
        private cMultiplayer: MultiplayerGame     
    ) {            
        this._init();
    }

    protected _init(): void
    {
        this.timeout = null;
        this.isInit = false;
        this.isStarted = false;
        this.blue = new QuizUser();
        this.red = new QuizUser();
        this.supervisor = new QuizSupervisor();
        this.turn = 0;
        this.myself = 0;
        this.isLoading = true;
        this.onTimeoutHappened = false;
        this.canLeaveFlag = false;
        this.time = new TimeManagement();
        this.onLoadSubject = new Subject<void>();
        this.onLoadObservable = this.onLoadSubject.asObservable();
        this.onLoadPromise = new Promise<void>((resolve) => {
            this.onLoadPromiseResolved = true;    
            resolve();
        });        
        this.onLoadPromiseResolved = false;  
    }

    /**
     * Sets multiplayer indicator.
     * 
     * @param val Multiplayer indicator.
     */
    public init( detectorRef: ChangeDetectorRef, val: boolean): Promise<any>
    {
        return new Promise<any>((resolve) => {
            this._init();
            this.multiplayer = val;                
            if ( val )
            {
                this.controller = this.cMultiplayer;
            } else
            {            
                this.controller = this.cSingleplayer;        
                this.wrapper = new SingleplayerGameWrapper(this.controller);
            };   
            
            // Inits game wrapper.
            this.wrapper.appendEventListeners();

            if ( !this.multiplayer )
            {
                this.controller.addEventListeners(() => {
                    this.controller.addEventListener("ctrl-get-data-about-game", (data: any) => { 
                        this.singleplayerInitGame(data); 
                        this.isInit = true;                         
                        resolve(data.game); 
                    });
                });
                this.controller.sendRegularRequest("sp-get-data-about-game", null);
            };
        });        
    }

    public canLeave(): boolean
    {
        return this.canLeaveFlag;
    }

    /**
     * Inits game for singleplayer.
     */
    private singleplayerInitGame(data: any): void
    {
        //console.log(data);
        // Copy data.
        this.supervisor = data.supervisor;
        this.blue = data.blue;
        if ( !data.finished )
            this.wrapper.setCurrentGameIndex(data.currentGame);        
        else
            this.wrapper.setCurrentGameIndex(5);
        this.isLoading = false;       
    }

    /**
     * Gets time manager.
     */
    public getTimeManager(): TimeManagement
    {
        return this.time;
    }

    /**
     * Calls ontimeout if it happened.
     */
    public onTimeout(): void
    {
        this.wrapper.getCurrentGame().onTimeout();
    }

    public stopTimeTicking(): void
    {
        this.time.stopInterval();
    }

    /**
     * Checks if page is loading.
     */
    public isStillLoading(): boolean
    {
        return this.isLoading;
    }

    /**
     * Checks if game type is multiplayer.
     */
    public isMultiplayer(): boolean
    {
        return this.multiplayer;
    }

    /**
     * Gets current game wrapper.
     */
    public getGameWrapper(): GameWrapper
    {
        return this.wrapper;
    }

    /**
     * Checks if blue user is logged in.
     */
    public isLoadedBlue(): boolean
    {
        return this.blue.loaded;
    }

    /**
     * Checks if red user is loaded.
     */
    public isLoadedRed(): boolean
    {
        return this.red.loaded;
    }

    /**
     * Checks if supervisor is loaded.
     */
    public isLoadedSupervisor(): boolean
    {
        return this.supervisor.loaded;
    }

    /**
     * Loads game wrapper.
     */
    public onLoad(): Promise<void>
    {
        return this.onLoadPromise;
    }

    /**
     * Gets load observable.
     */
    public getOnLoadObservable(): Observable<void>
    {
        return this.onLoadObservable;
    }

    /**
     * Destroys all games.
     */
    public onDestroy(): void
    {
        for (var i=0; i<this.wrapper.getGamesLength(); i++)
            this.wrapper.getGame(i).onDestroy(); 
    }

    /**
     * Checks if current user is on 
     * 
     * @param name opponent|myself
     */
    public isUserOnTurn(name: string): boolean
    {
        if ( name == "opponent" )
            return this.myself == 0 ? this.turn == 1 : this.turn == 0;
        else if ( name == "myself" )
            return this.myself == 1 ? this.turn == 1 : this.turn == 0; 
        else 
            return false;
    }

    /**
     * 
     * @param type Type of the user: blue|red
     */
    public getUser(type: string): QuizUser
    {
        if ( type == "blue" )
            return this.blue;
        else if ( type == "red" )
            return this.red;
        else
            return null;
    }

    /**
     * Gets supervisor for the game.
     */
    public getSupervisor(): QuizSupervisor
    {
        return this.supervisor;
    }

    /**
     * Gets current game index.
     */
    public getCurrentGameIndex(): number
    {
        return this.wrapper.getCurrentGameIndex();
    }

    /**
     * Adds points to the game for given user. Only for visual representation. 
     * 
     * @param type Type of user: blue|red
     * @param amount Amount to add.
     */
    public showAdditionOfPoints(type: string, amount: number): Promise<void>
    {
        var user: QuizUser = type == "blue" ? this.blue : this.red;         
        user.points += amount;
        return new Promise<void>((resolve) => {   
            clearTimeout(user.timeout);
            user.showAddPoints = true;
            user.additionOfPoints = amount;        
            user.timeout = setTimeout(() => {
                user.showAddPoints = false;                
                resolve();
            }, 2000);
        });        
    }

    /**
     * Switches to the next game.
     */
    public nextGame(): void
    {
        this.wrapper.nextGame();
    }

    /**
     * Sets component for the game.
     * 
     * @param name 
     * @param ref 
     */
    public setComponentForGame(name: string, ref: QuizComponent): void
    {
        switch(name)
        {
        case "anagram":
            this.wrapper.getGame(0).setComponent(ref);
            break;

        case "number":
            this.wrapper.getGame(1).setComponent(ref);
            break;

        case "fxf":
            this.wrapper.getGame(2).setComponent(ref);
            break;

        case "nat-geo":
            this.wrapper.getGame(3).setComponent(ref);
            break;

        case "trophie":
            this.wrapper.getGame(4).setComponent(ref);
            break;
        }
    }

    /**
     * Starts all components.
     */
    public start(): void
    {
        // Send signal to all listening component!
        this.onLoadSubject.next();

        for (var i=0; i<this.wrapper.getGamesLength(); i++)
            this.wrapper.getGame(i).onLoad(); 

        this.isStarted = true;

        this.time.registerCallback(() => {            
            this.onTimeout();
        });
    }
}