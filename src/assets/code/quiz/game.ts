import { QuizComponent } from './quiz-component-interface';
import { MultiplayerGame } from '../multiplayer-game';
import { SingleplayerGame } from '../user/singleplayer-request';


/**
 * Single game handling. Abstract class.
 */
export abstract class Game implements QuizComponent
{
    protected component: QuizComponent;

    public setComponent(ref: QuizComponent): void
    {
        this.component = ref;
    }

    public getComponent(): QuizComponent
    {
        return this.component;
    }

    public abstract appendEventListeners(controller: SingleplayerGame | MultiplayerGame): void;
    public abstract removeEventListeners(controller: SingleplayerGame | MultiplayerGame): void;

    public abstract submitData(controller: any, request: string, data: any): void;

    public abstract onTimeout(): void;
    public abstract onFinished(): void
    public abstract onPreview(): void;

    public abstract onLoad(): void;
    public abstract onDestroy(): void;
}