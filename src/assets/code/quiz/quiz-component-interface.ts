/**
 * Every game of the quiz should implement these methods.
 */
export interface QuizComponent
{
    onFinished(): void;
    onPreview(data: any): void;
    onTimeout(): void;
}