/**
 * Handling game time.
 */
export class TimeManagement
{
    private minutes: number;
    private seconds: number;
    private ticking: boolean;
    private interval: any;
    private callback: any;
    private shouldNotify: boolean;

    constructor() 
    {
        this.minutes = 0;
        this.seconds = 0;
        this.ticking = false;
        this.shouldNotify = false;
    }

    public setTickingPeriod(seconds: number)
    {
        if ( this.ticking )
            return;
    
        this.minutes = Math.floor( seconds / 60 );
        this.seconds = seconds % 60;
        this.ticking = true;

        this.interval = setInterval(() => {
            this.intervalCallback();
        }, 1000);
    }

    public isValid(date: Date): boolean
    {
        var currDate: Date = new Date();
        if ( date.getTime() - currDate.getTime() <= 0 )
            return false;
        return true;
    }

    public setTickingTimeoutValue(date: Date): void
    {
        if ( this.ticking )
            return;

        var currDate: Date = new Date();
        if ( date.getTime() - currDate.getTime() < 0 )
        {
            this.minutes = 0;
            this.seconds = 0;
            this.ticking = false;            
            if ( this.callback != null )
            {
                this.shouldNotify = false;
                this.callback();
            } else    
                this.shouldNotify = true;
            return;
        };

        var output: Date = new Date( date.getTime() - currDate.getTime() );
        var minutes: number = output.getMinutes();
        var seconds: number = output.getSeconds();    

        this.minutes = minutes;
        this.seconds = seconds;
        this.ticking = true;

        this.interval = setInterval(() => {
            this.intervalCallback();
        }, 1000);

    }

    private intervalCallback(): void
    {
        // Notify user.
        if ( this.minutes == 0 && this.seconds == 0 )
        {   
            clearInterval(this.interval);
            this.ticking = false;
            if ( this.callback != null )
            {
                this.shouldNotify = false;
                this.callback();
            } else    
                this.shouldNotify = true;
            return;
        };

        if ( this.seconds == 0 )
        {
            this.minutes--;
            this.seconds = 59;
        } else
            this.seconds--;
    }

    public getMinutes(): string
    {
        return this.ticking ? (this.minutes < 10 ? "0" + this.minutes : "" + this.minutes) : "--";
    }

    public getSeconds(): string
    {
        return this.ticking ? (this.seconds < 10 ? "0" + this.seconds : "" + this.seconds) : "--";
    }

    public registerCallback(callback: any): void
    {
        this.callback = callback;
    }

    public stopInterval(): void
    {
        if ( this.ticking )
        {
            clearInterval(this.interval);
            this.ticking = false;
            this.minutes = 0;
            this.seconds = 0;
        };
    }

    /**
     * Checks if user should be notified.
     */
    public checkTimeout(): boolean
    {
        return this.shouldNotify;
    }


}