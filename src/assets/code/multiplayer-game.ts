import { Injectable } from '@angular/core';
import { Globals } from './globals';
import { Subject, Observable } from 'rxjs';
import { Server } from './http-request';
import { Socket } from './connection/socket';

@Injectable()
export class MultiplayerGame extends Socket {

    private redUsername: string;
    private redName: string;
    private redSurname: string;

    private supervisorUsername: string;
    private supervisorName: string;
    private supervisorSurname: string;

    private active: boolean;
    private ready: boolean;
    private inRequest: boolean;

    private registerEvent: Subject<void>;

    // Listeners. 

    constructor( globals: Globals, server: Server ) 
    {
        super(globals, server);
        this.registerEvent = new Subject<void>();
    }

    public getRegisterEventsObservable(): Observable<void>
    {
        return this.registerEvent.asObservable();
    }

    /**
     * Creates new game.
     * 
     * @param username Username for which new game is created.
     */
    public createNewGame(username: string): void
    {
        // Ensure socket connection!
        this.prepare();
        
        // Creates game on server side.
        this.sendEvent( "create-multiplayer-game-for", username );
    }

    /**
     * Shuts down current game.
     */
    public terminateGame(sendEvent: boolean = true) : void
    {
        // Terminate game.
        if ( this.socket && this.socket.connected )
        {
            if ( sendEvent ) 
                this.sendEvent("terminate-game-for-me", {});
            this.socketDisconnect();
        }
    }

    /**
     * Checks if user can sign out from the match.
     */
    public canSignOut(): Promise<any>
    {
        return new Promise<any>((resolve) => {
            this.server.requestAuth("post", "user/can-sign-out", {}, resp => {
                resolve(resp);            
            }, err => {
                resolve(null);
            });                      
        });        
    }

    public getRedUsername(): string {
        return this.redUsername;
    }

    public setRedUsername(username: string): void {
        this.redUsername = username;
    }

    public getSupervisorUsername(): string {
        return this.supervisorUsername;
    }

    public setSupervisorUsername(username: string): void {
        this.supervisorUsername = username;
    }

    public isActive(): boolean {
        return this.active;
    }

    public setActive(val: boolean): void {
        this.active = val;
    }

    public isReady(): boolean {
        return this.ready;
    }

    public setReady(val: boolean): void {
        this.ready = val;
    }

    public getSupervisorName(): string {
        return this.supervisorName;
    }

    public getSupervisorSurname(): string {
        return this.supervisorSurname;
    }

    public getSupervisorWholeName(): string {
        return this.supervisorName + " " + this.supervisorSurname;
    }

    public getRedName(): string {
        return this.redName;
    }

    public getRedSurname(): string {
        return this.redSurname;
    }

    public getRedWholeName(): string {
        return this.redName + " " + this.redSurname;
    }

    public setSupervisorName(name: string): void {
        this.supervisorName = name;
    }

    public setSupervisorSurname(name: string): void {
        this.supervisorSurname = name;
    }

    public setRedName(name: string): void {
        this.redName = name;
    }

    public setRedSurname(name: string): void {
        this.redSurname = name;
    }
}