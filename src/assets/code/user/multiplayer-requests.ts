import { Injectable, Inject, Component, Type, ComponentFactoryResolver, ViewChild, ViewContainerRef, ChangeDetectorRef } from '@angular/core';
import { MultiplayerUserRequestComponent, MultiplayerComponent } from 'src/assets/components/multiplayer-user-request/multiplayer-user-request';
import { Server } from '../http-request';

/**
 * Multiplayer item.
 */
export class MultiplayerItem
{    
    public instance: MultiplayerComponent;
    constructor( public component: Type<any>, public data: any, public index: number, public multiplayerRequests: MultiplayerRequests ) {}
}

// ============================================================================

/**
 * Handling multiplayer requests.
 */
@Injectable()
export class MultiplayerRequests
{
    multiRefresh: boolean;
    activeMultiplayers: number;
    joining: boolean;
    multiplayerPlaceholder: ViewContainerRef;
    private users: Array<MultiplayerItem>;

    /**
     * 
     * @param componentFactoryResolver Component resolver.
     * @param multiplayerPlaceholder Multiplayer placeholder ref.
     */
    constructor( 
        private componentFactoryResolver: ComponentFactoryResolver, 
        private server: Server, 
        private cdRef: ChangeDetectorRef 
    )
    {
        this.multiRefresh = false;
        this.activeMultiplayers = 0;
        this.joining = false;
        this.users = new Array<MultiplayerItem>();
    }

    /**
     * Refreshes component.
     */
    public refreshComponents(setMultiRefresh: boolean = true): void
    {
        if ( setMultiRefresh )
            this.multiRefresh = true;

        // Get available multiplayer(s).
        this.server.requestAuth('post', 'user/get-avail-multiplayers', {}, (data, resp) => 
        {          
            if ( setMultiRefresh )  
                this.multiRefresh = false;
            this.activeMultiplayers = data.num;
            var active = data.num;
            this.users = new Array<MultiplayerItem>();
            var users = data.users;
            for ( var i=0; i<active; i++ )
            {
                var user: any = users[i];
                var start = new Date(user.fromBeginTime);
                var end = new Date();
                var now = end.getTime() - start.getTime();
                var seconds = Math.floor( ( now / 1000 ) ) % 60;
                var minutes = Math.floor( ( (now / 1000) / 60 ) ) % 60;
                var hours =  Math.floor( ((now / 1000) / (60*60)) ) % 24 ;
                user.fromBeginTime = this.formatTime(hours, minutes, seconds);
                this.users[i] = new MultiplayerItem(MultiplayerUserRequestComponent, users[i], i, this);
            };
            this.loadComponent();
        }, (resp) => {
            window.location.reload();
        });
    }

    /**
     * Returns time format as __h __min __s.
     * 
     * @param hours Hours.
     * @param minutes Minutes.
     * @param seconds Seconds.
     */
    private formatTime( hours: number, minutes: number, seconds: number ): string
    {
        var ret = "";
        var addedHours = false,
            addedMinutes = false;
        if ( hours > 0 )
        {
            ret += hours + "h:";
            addedHours = true;
        };

        if ( minutes > 0 || addedHours )
        {
            ret += minutes + "min:";
            addedMinutes = true;
        };

        ret += seconds + "s";

        return ret;
    }

    /**
     * Loads new component.
     */
    private loadComponent() : void
    {
        // Clear all players.
        //this.multiplayerPlaceholder.clear();
        
        if ( this.users.length > 0 )
        {
            var a = this.users.length;
            for ( var i=0; i<a; i++ )
            {
                const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.users[i].component);
                const componentRef = this.multiplayerPlaceholder.createComponent(componentFactory);
                var m: MultiplayerComponent = (<MultiplayerComponent>componentRef.instance);
                m.data = this.users[i].data;
                m.index = this.users[i].index;
                m.multiplayerRequests = this;
                m.server = this.server;
                this.users[i].instance = m;
            };
        };
    }

    public deactivateExcept( index: number ): void
    {
        for ( var i=0; i<this.users.length; i++ )
        {
            if ( i == index )
                continue;
            this.users[i].instance.deactivateComponent();
        }
    }
}