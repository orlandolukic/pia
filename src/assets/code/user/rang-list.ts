/**
 * Displays rang list on user side.
 */
export interface RangList
{
    rang: number;
    username: string;
    fullname: string;
    image: string;
    points: number;
    date: string;
}