import { Injectable } from '@angular/core';
import { Socket } from '../connection/socket';
import { SupervisorData } from '../util/supervisor-data';
import { Globals } from '../globals';
import { Server } from '../http-request';

/**
 * Manages singleplayer requests.
 */
@Injectable()
export class SingleplayerGame extends Socket
{
    supervisor: SupervisorData;

    constructor(
        globals: Globals,
        server: Server
    ) {
        super( globals, server );
        this.supervisor = new SupervisorData();
    }

    public createPendingGame(): void
    {
        var username: string = this.globals.getUser().getData().username;
        this.prepare();
        this.sendEvent("create-singleplayer-game-for", username);
    }

    /**
     * Checks if user can signout of quiz game.
     */
    public canCloseQuiz(): Promise<any>
    {
        return new Promise<any>((resolve) => {
            this.server.requestAuth("post", "user/can-sign-out", {}, resp => {
                resolve(resp);            
            }, err => {
                resolve(null);
            });   
        });
    }

    /**
     * Destroys match for user.
     */
    public destroyMatch(): Promise<void>
    {
        return new Promise<void>((resolve) => {
            this.sendRegularRequest("decline-singleplayer-game-for", null);
            this.socketDisconnect();
            resolve();
        });
    }
}