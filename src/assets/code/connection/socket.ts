import { Injectable } from '@angular/core';
import io from 'socket.io-client';
import { Server } from '../http-request';
import { Globals } from '../globals';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class Socket
{
    private registerEvents: Subject<void>;
    private pendingOutputQueue: Subject<void>;
    protected socket: any;
    protected created: boolean;

    constructor( protected globals: Globals, protected server: Server ) 
    {
        this.registerEvents = new Subject<void>();
        this.pendingOutputQueue = new Subject<void>();
    }

    /**
     * Creates socket connection with server.
     */
    protected socketConnect(): void
    {
        if ( !this.created && ( !this.socket || this.socket && this.socket.disconnected ) )
        {
            var user = this.globals.getUser().getData();
            this.socket = io(this.globals.socketServer, {
                autoConnect: false
            });
            this.created = true;
            this.socket = this.socket.open(); 

            // Append event listeners for socket connection.
            this.appendEventListeners();
            
            // Say hallo to the server.
            this.sendEvent("hello", user.username, user.accType );             

            console.log("Socket connected!");
        };
    }

    /**
     * Gets queue for registering events when socket connects.
     */
    public getRegisterEventsObservable(): Observable<void>
    {
        return this.registerEvents.asObservable();
    }

    /**
     * Appends event listeners.
     */
    private appendEventListeners(): void
    {
        this.socket.on("connect", () => {

            setTimeout(() => {
                // Execute pending requests.
                this.pendingOutputQueue.next();
            }, 200);

            // Append all listeners.
            this.registerEvents.next();
        });

        // On reconnection.
        this.socket.on("reconnect", () => {

            setTimeout(() => {
                // Execute pending requests.
                this.pendingOutputQueue.next();
            }, 500);

            // Append all listeners.
            this.registerEvents.next();
            console.warn("Socket reconnection...");
        });
    }

    /**
     * Passes data to system user.
     * 
     * @param username 
     * @param requestName 
     * @param data 
     */
    public passDataToUser( username: string, requestName: string, data: any ): void
    {
        this.sendEvent("pass", {
            sender: this.globals.getUser().getData().username,
            dest: username,
            name: requestName,
            data: data
        });
    }

    /**
     * Sends regular request with identification.
     * 
     * @param username 
     * @param requestName 
     * @param data 
     */
    public sendRegularRequest( name: string, data: any ): void
    {
        this.prepare();

        this.sendEvent(name, {
            sender: this.globals.getUser().getData().username,
            data: data
        });
    }

    /**
     * Adds event listeners.
     */
    public addEventListeners( func: Function ): void
    {
        this.prepare();

        if ( this.isSocketConnected() )
            func();
        else
            this.getRegisterEventsObservable().subscribe(() => {
                func();
            });
    }

    /**
     * Disconnects socket connection.
     */
    public socketDisconnect(): void
    {        
        if ( this.socket && this.socket.connected )
        {
            this.socket.close();
            this.created = false;
            this.socket = null;
            console.log("Socket disconnected!");
        };
    }

    /**
     * Sends event to the server via socket.
     * 
     * @param event Event name to emit to server via socket.
     * @param data Data object to send to server.
     */
    public sendEvent( event: string, ...data: any[] ) : void
    {
        this.prepare();
            
        // Emit data!
        if ( this.isSocketConnected() )
            this.socket.emit( event, ...data );
        else
            this.pendingOutputQueue.asObservable().subscribe(() => {                
                try {
                    this.socket.emit(event, ...data);                
                } catch(e) { console.error(e); }
            });
    }

    /**
     * Makes connecion to server via socket.io
     */
    public prepare(): void
    {
        // Ensure socket connection!
        if ( !this.isSocketConnected() )
            this.socketConnect();
    }    

    /**
     * Registers new handler for given event.
     * 
     * @param event Event on which handler should be called.
     * @param func Handler for this event.
     */
    public addEventListener( event: string, func: any )
    {
        this.prepare();

        if ( this.isSocketConnected() )           
            this.socket.on(event, func);
        else 
            this.getRegisterEventsObservable().subscribe(() => {
                this.socket.on(event, func);
            });
    }

    /**
     * Unregisters new handler for given event.
     *  
     * @param event Event on which given handler should not be called.
     * @param func Handler for this event.
     */
    public removeEventListener( event: string, func: Function )
    {
        if ( this.isSocketConnected() )
            if ( func == null )
                this.socket.off(event);
            else    
                this.socket.off(event, func);
    }

    public isSocketConnected(): boolean {
        return this.socket != null ? this.socket.connected : false;
    }
}