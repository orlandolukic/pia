/**
 * Class for handling the game.
 */
export abstract class Game
{
    /**
     * Prepares form for the game.
     */
    public abstract prepareForm(): void;

    /**
     * Starts game between oponents.
     */
    public abstract startGame(): void;

    /**
     * Checks for answers.
     */
    public abstract checkForAnswers(): void;
}