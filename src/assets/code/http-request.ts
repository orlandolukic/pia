import { Injectable } from '@angular/core';
import { Globals } from './globals';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { User } from './user';

/**
 * Manipulating with http requests.
 */
 @Injectable()
 export class Server
 {
    private  static instance: Server;
    public static getInstance(): Server
    {
        return Server.instance;
    }
    
    constructor( private http : HttpClient, private globals : Globals ) 
    {
        Server.instance = this;
    }

    /**
     * Makes reqeust to NodeJS.
     * 
     * @param type Method type { POST | GET | DELETE | PUT }
     * @param page Page on which request needs to be made.
     * @param data Data to send to server.
     * @param callback Callback to execute on server returning data.
     */
    public request( type : string, page : string, data : any, callback : Function ) : void 
    {
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set("Access-Control-Allow-Origin", "*");

        // Check if request exists.
        if ( !this.http[type] )
        {
            console.error("Could not make " + type.toUpperCase() + " request on " + this.globals.nodeServer + "/" + page + ".")
            return;
        };
        this.http[type](this.globals.nodeServer + "/" + page, JSON.stringify(data), {
            headers: headers,
            withCredentials: true,
            observe: 'response'
        })
        .subscribe(r => {
            callback(r.body, r);
        });
    }

    /**
     * Makes reqeust to NodeJS.
     * 
     * @param type Method type { POST | GET | DELETE | PUT }
     * @param page Page on which request needs to be made.
     * @param data Data to send to server.
     * @param callback Callback to execute on server returning data.
     */
    public requestAuth( type : string, page : string, data : any, callback : Function, error: Function ) : void 
    {
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set("Access-Control-Allow-Origin", "*");

        // Check if request exists.
        if ( !this.http[type] )
        {
            console.error("Could not make " + type.toUpperCase() + " request on " + this.globals.nodeServer + "/" + page + ".")
            return;
        };
        var req = this.http[type](this.globals.nodeServer + "/" + page, JSON.stringify(data), {
            headers: headers,
            withCredentials: true,
            observe: 'response'
        })
        .subscribe(r => {
            if ( r.status !== 200 )
                error(r);
            else
                callback(r.body, r);
        }, r => {
            error(r);
        });        
    }

    /**
     * 
     * @param page Page on which server needs to listen.
     * @param data Form data with file.
     * @param callback Callback function to execute on success.
     * @param errorHandler Error handler.
     */
    public upload( page: string, data: FormData, callback: Function, errorHandler: Function = function(error) {}) : void
    {
        const headers = new HttpHeaders()
            .set('Authorization', 'my-auth-token');

        headers.append('Content-Type', 'multipart/form-data');

        this.http.post(this.globals.nodeServer + "/" + page, data, { headers: headers })
        .subscribe(data => {
            callback(data);
        }, error => {
            errorHandler(error);
        });
    }

    /**
     * Checks if user is still logged in.
     */
    public checkIfUserIsLoggedIn(): Promise<boolean> 
    {
        return new Promise<boolean>((resolve) => {
            if ( this.globals.getUser() != null )
            {
                resolve(true);
                return;
            };
           
            this.request('post', 'user/is-active-session', {}, (data: any, resp: any) => {
                var allow = true;
                var isLoggedIn = data.isLoggedIn;                    

                if ( isLoggedIn )
                {            
                    this.globals.user = new User(data.user, data.expiry);            
                    resolve(true);
                } else {
                    resolve(false);                            
                };                          
            });
          
        });
    }

    /**
     * Checks if user has active quizes.
     */
    public checkIfSupervisorHasActiveQuiz(): Promise<any>
    {
        return new Promise<any>((resolve) => {            
            this.checkIfUserIsLoggedIn().then((logged) => {
                if ( logged )
                {
                    if ( this.globals.getUser().isSupervisor() )
                    {
                        this.request("post", "supervisor/can-open-supervise-section", {}, (resp: any) => {
                            resolve(resp);
                        });
                    } else
                        resolve(null);                    
                } else
                    resolve(null);
            });      
        });
    }
 }