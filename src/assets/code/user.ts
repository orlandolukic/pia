import { Time } from '@angular/common';

/**
 * User session.
 */
export class User
{
    private user: any;
    private expiry: Date;
    private accType: number;

    constructor( user: any, expiry: string ) 
    {
        this.user = user;
        this.expiry = new Date(expiry);
        this.user.image = this.user.image.replace("\\","/");
        this.user.image = "assets/upload/" + this.user.image;
        this.accType = user.accType;
    }

    public getData() : any { return this.user; }
    public getExpiry() : any { return this.expiry; }     
    public setExpiry( expiry: any ): any { this.expiry = expiry; }
    public isStillValid(): boolean 
    {
        var today = new Date();
        return today <= this.expiry;
    }

    public isRegularUser(): boolean { return this.accType == 0; }
    public isSupervisor(): boolean { return this.accType == 1; }
    public isAdministrator(): boolean { return this.accType == 2; }
}