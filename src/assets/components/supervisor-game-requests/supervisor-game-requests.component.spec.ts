import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorGameRequestsComponent } from './supervisor-game-requests.component';

describe('SupervisorGameRequestsComponent', () => {
  let component: SupervisorGameRequestsComponent;
  let fixture: ComponentFixture<SupervisorGameRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorGameRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorGameRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
