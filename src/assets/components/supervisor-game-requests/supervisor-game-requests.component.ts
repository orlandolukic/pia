import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactory, ComponentFactoryResolver, AfterViewInit, OnDestroy, SimpleChange, OnChanges, SimpleChanges, ChangeDetectorRef, ComponentRef } from '@angular/core';
import { SupervisorGameRequestComponent } from '../supervisor-game-request/supervisor-game-request.component';
import { Supervise } from 'src/assets/code/supervisor/supervise';
import { UserRequests, MultiplayerRequest } from 'src/assets/code/supervisor/user-request';
import { SingleplayerGame } from 'src/assets/code/user/singleplayer-request';

@Component({
  selector: 'supervisor-game-requests',
  templateUrl: './supervisor-game-requests.component.html',
  styleUrls: ['./supervisor-game-requests.component.css']
})
export class SupervisorGameRequestsComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy
{
  private render: boolean;
  private countSingleplayer: number;
  private countMultiplayer: number;

  private arrSingleplayer: UserRequests<SingleplayerGame>;
  private arrMultiplayer: UserRequests<MultiplayerRequest>;

  private childSingleplayerRequestsContainer: ViewContainerRef;
  private childMultiplayerRequestsContainer: ViewContainerRef;

  @ViewChild("singleplayerRequests", {static: false, read: ViewContainerRef}) set a(content: ViewContainerRef)
  {
    this.childSingleplayerRequestsContainer = content;
  }
  @ViewChild("multiplayerRequests", {static: false, read: ViewContainerRef}) set b(content: ViewContainerRef)
  {
    this.childMultiplayerRequestsContainer = content;
  }

  constructor(
    private componentFactory: ComponentFactoryResolver,
    private supervise: Supervise,
    private changeDetectorRef: ChangeDetectorRef
  ) 
  { 
    this.countMultiplayer = 0;
    this.countSingleplayer = 0;  
    this.arrSingleplayer = new UserRequests<SingleplayerGame>();
    this.arrMultiplayer = new UserRequests<MultiplayerRequest>();

    this.supervise.addEventListeners(() => {      
      this.supervise.addEventListener("ctrl-sp-notify-supervisor", (val: string) => { this.notifySupervisorSingleplayer(val); });            
      this.supervise.addEventListener("ctrl-sp-user-declined", (val: string) => { this.spUserDeclined(val); })
      this.supervise.addEventListener("ctrl-sp-data-about-user", (val: any) => { this.spAddNewMatch(val); })
    });
  }

  ngOnInit() {
    
  }

  ngOnChanges(changes: SimpleChanges)
  {
    
  }

  ngAfterViewInit()
  {
  
  }

  ngOnDestroy()
  {
    this.supervise.removeEventListener("ctrl-sp-notify-supervisor", null);            
    this.supervise.removeEventListener("ctrl-sp-user-declined", null);
    this.supervise.removeEventListener("ctrl-sp-data-about-user", null);
  } 

  /**
     * Notifies supervisor.
     * 
     * @param username 
     */
  public notifySupervisorSingleplayer(username: string): void {
    try {
        // Check if username already exists in the system.
        if ( this.arrSingleplayer.exists( username ) )
          return;
      
        this.supervise.sendEvent("supervisor-sp-get-data-about-user", username);

    } catch(e) {
        console.error(e);   
    }
  }

  /**
   * User declines match.
   * 
   * @param username Username of the user which declined match.
   */
  public spUserDeclined(username: string): void
  {
    var el: any = this.arrSingleplayer.getUserRequest(username);
    if ( el != null )
    {
      this.arrSingleplayer.remove(username);
      this.countSingleplayer--;
      this.childSingleplayerRequestsContainer.remove(el.index);
      this.render = false;
      if ( !this.changeDetectorRef['destroyed'] )
        this.changeDetectorRef.detectChanges();
    };
  }

  public spAddNewMatch(data: any): void
  {      
    if ( this.arrSingleplayer.exists(data.username) )
      return;

    if ( !this.childSingleplayerRequestsContainer )
    {
      this.render = true;
      if ( !this.changeDetectorRef['destroyed'] )
        this.changeDetectorRef.detectChanges();
    };

    var resolver = this.componentFactory.resolveComponentFactory(SupervisorGameRequestComponent);
    var comp: ComponentRef<SupervisorGameRequestComponent> = this.childSingleplayerRequestsContainer.createComponent(resolver);    

    var el: any = this.arrSingleplayer.add(data.username, data.name, data.surname, data.image, this.countSingleplayer);
    this.countSingleplayer++;

    // Inject data into new component. 
    comp.instance.isMultiplayer = false;
    comp.instance.data = data;

    console.log("Notified supervisor about user: ", data.username);
  }

}
