import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameFxfTableComponent } from './game-fxf-table.component';

describe('GameFxfTableComponent', () => {
  let component: GameFxfTableComponent;
  let fixture: ComponentFixture<GameFxfTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameFxfTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameFxfTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
