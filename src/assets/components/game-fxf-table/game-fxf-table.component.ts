import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'game-fxf-table',
  templateUrl: './game-fxf-table.component.html',
  styleUrls: ['./game-fxf-table.component.css']
})
export class GameFxfTableComponent implements OnInit {

  @Input("words")
  words: Array<string>;

  constructor() { }

  ngOnInit() {
  }

}
