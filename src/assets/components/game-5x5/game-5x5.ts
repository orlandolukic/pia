import { Component, OnInit, Input, ViewChild, ElementRef, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { GameItem, List5x5Games } from 'src/assets/code/supervisor/list-5x5-games';
import { Server } from 'src/assets/code/http-request';


@Component({
    selector: "game-5x5",
    templateUrl: "./game-5x5.html",
    styleUrls: ["./game-5x5.css"],
    encapsulation: ViewEncapsulation.None
})
export class Game5x5Component implements OnInit, AfterViewInit, Game5x5Interface
{
    private static ID: number = 0;

    private id: number;
    private tooltipMessage: string;
    private selected: boolean;
    private loaded: boolean;
    private matrix: Array<Array<string>>;
    private table: any;

    @Input() data: any;
    @Input() listOfGames: List5x5Games;
    @Input() server: Server;

    constructor() {
        this.selected = false;
        this.loaded = false;
        this.tooltipMessage = "Kliknite da selektujete";
        this.id = Game5x5Component.ID++;
    }
    
    ngOnInit(): void {
              
    }

    ngAfterViewInit(): void {
        // Print data to the screen.
        var table: any = document.getElementById("myTable-"+this.id);
        console.log(table);
        console.log(table.rows.length);
        for ( var i:number = table.rows.length-1; i>=0; i-- )
        {
            table.deleteRow(i);
        };       
        
        for ( var i=0; i<5; i++ )
        {
            var row = table.insertRow(i);
            for (var j=0; j<5; j++)
            {
                var cell = row.insertCell(j);
                cell.innerHTML = this.matrix[i][j];
            };
        }; 
    }

    public mark(event: MouseEvent): void
    {
        this.listOfGames.addSelectedGame(this, event.ctrlKey);
    }

    public isSelected(): boolean
    {
        return this.selected;
    }

    public select(): void
    {
        this.selected = true;
        this.tooltipMessage = "Kliknite da deselektujete";
    }

    public unselect(): void
    {
        this.selected = false;
        this.tooltipMessage = "Kliknite da selektujete";
    }

    public onUpdatedData(): void
    {
        this.matrix = new Array<Array<string>>();
        var subarr: Array<string>;
        for (var i=0; i<5; i++)
        {
            subarr = this.data["w" + (i+1)].split('');
            subarr = subarr.map((x) => { return x.toUpperCase(); });
            this.matrix[i] = subarr;  
        };
        this.loaded = true;
    }

}

export interface Game5x5Interface
{
    data: any;
    listOfGames: List5x5Games;
    server: Server;
    //deactivateComponent() : void;
    onUpdatedData(): void;
}