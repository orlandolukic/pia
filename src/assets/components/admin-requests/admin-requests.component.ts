import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSnackBar, MatSelectChange } from '@angular/material';
import { Server } from 'src/assets/code/http-request';

@Component({
  selector: 'admin-requests',
  templateUrl: './admin-requests.component.html',
  styleUrls: ['./admin-requests.component.css']
})
export class AdminRequestsComponent implements OnInit, OnDestroy 
{
  private loading: boolean;
  private count: number;
  private activeRequest: any;
  private requests: Array<any>;
  private inAction: boolean;
  private interval: any;
  private firstTime: boolean;
  private focused: boolean;

  constructor(
    private snackBar: MatSnackBar,
    private server: Server
  ) 
  {
    this.count = 0;
    this.loading = true;
    this.inAction = false;
    this.activeRequest = null;
    this.requests = null;
    this.firstTime = true;
    this.focused = false;
  }

  ngOnInit() {
    this.interval = setInterval(() => {
      if ( !this.focused || this.firstTime || this.activeRequest )
        return;
      
      this.fetchRequests(false);
    }, 3000);
  }

  ngOnDestroy()
  {
    clearInterval(this.interval);
  }

  public leaveFocus(): void
  {
    this.focused = false;
  }

  public fetchRequests(changeView: boolean = true): void
  {
    this.focused = true;
    if ( changeView )
      this.loading = true;
    this.activeRequest = null;
    this.inAction = false;
    this.server.requestAuth("post", "admin/fetch-pending-requests", {}, resp => {
      this.loading = false;
      this.firstTime = false;
      this.count = resp.count;
      if ( resp.count > 0 )
      {        
        this.requests = resp.requests;
      } else
      {
        this.requests = null;        
      };
    }, err => {
      this.loading = false;
      console.error("Could not fetch pending requests from server.");
    });
  }

  processRequest(type: number): void
  {
    var message: string = type == 1 ? "Da li ste sigurni da prihvatate ovaj zahtev?" : "Da li ste sigurni da odbijate ovaj zahtev?";
    var x = confirm(message);
    if ( x )
    {
      this.inAction = true;
      this.server.requestAuth("post", "admin/process-pending-request", {
        type: type,
        id: this.activeRequest._id
      }, resp => {
        if ( resp.performedAction )
        {
          this.fetchRequests();
        };
      }, err => {
        console.error("Could not process pending request.");
      });
    };
  }

  select(event: MatSelectChange): void
  {
    if ( event.value == -1 )
    {
      this.activeRequest = null;
    } else
    {
      var requests = this.requests;
      for (var i=0,h=requests.length; i<h; i++)
      {
        if ( event.value == requests[i]._id )
        {
          this.activeRequest = requests[i];
          break;
        };  
      };
    };
  }

}
