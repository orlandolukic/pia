import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { TrophieData } from '../modals/modal-add-new-trophie/modal-add-new-trophie.component';
import { Server } from 'src/assets/code/http-request';
import { MatInput, MatSelect, MatSelectChange, MatSnackBar } from '@angular/material';

@Component({
  selector: 'admin-game-of-the-day',
  templateUrl: './admin-game-of-the-day.component.html',
  styleUrls: ['./admin-game-of-the-day.component.css'],
})
export class AdminGameOfTheDayComponent implements OnInit, AfterViewInit 
{
  private count: number;
  private action: string;
  private gameTrophies: Array<any>;
  private game5x5: Array<any>;
  private gameAnagrams: Array<any>;
  private listOfGamesOfTheDay: Array<any>;
  private selected: boolean;
  private irregular: boolean;
  private isLoading: boolean;

  private selected5x5: any;
  private selected5x5Words: Array<string>;
  private selectedTrophie: any;
  private selectedAnagram: any;
  private selectedGame: any;
  private occupiedDates: Array<any>;

  @ViewChild("date", {static: true, read: MatInput}) childDate: MatInput;
  @ViewChild("compGame5x5", {static: true, read: MatSelect}) childGame5x5: MatSelect;
  @ViewChild("compGameTrophie", {static: true, read: MatSelect}) childGameTrophie: MatSelect;
  @ViewChild("compGameAnagram", {static: true, read: MatSelect}) childGameAnagram: MatSelect;
  @ViewChild("compAllGames", {static: true, read: MatSelect}) childAllGames: MatSelect;

  constructor(
    private server: Server,
    private snackBar: MatSnackBar
  ) 
  {
    this.count = 0;
    this.action = null;
    this.selected = false;
    this.irregular = false;
    this.isLoading = false;

    this.listOfGamesOfTheDay = null;
    this.gameTrophies = null;
    this.game5x5 = null;
    this.selected5x5 = null;
    this.selectedTrophie = null;
    this.selectedAnagram = null;
    this.selectedGame = null;
    this.selected5x5Words = null;
    this.occupiedDates = new Array<any>();
  }

  ngOnInit() {

  }

  ngAfterViewInit()
  {
    
  }

  public clearDate(): void
  {
    this.selected = false;
    this.resetFormFields();
  }

  public refreshGamesOfTheDay(): void
  {
    this.server.requestAuth("post", "admin/fetch-all-games", {}, resp => {
      this.irregular = !resp.game5x5.found || !resp.gameTrophie.found || !resp.gameAnagram.found;
      if ( !this.irregular )
      {
        this.gameTrophies = resp.gameTrophie.games;
        this.game5x5 = resp.game5x5.games;
        this.gameAnagrams = resp.gameAnagram.games;
        this.listOfGamesOfTheDay = resp.gameOfTheDay.games;
        this.occupiedDates = new Array<any>();
        var games = resp.gameOfTheDay.games;
        for ( var i=0; i<games.length; i++ )
          this.occupiedDates[i] = { 
            date: new Date(games[i].date),
            _id: games[i]._id
          };
      };
    }, err => {
      console.log("Could not fetch all games.");
    });
  }

  myFilter = (d: Date): boolean => {
    var now: Date = new Date();  
    var dDay = d.getDate();  
    var dMonth = d.getMonth();
    var dYear = d.getFullYear();

    if ( this.existsDate(dDay, dMonth, dYear) )
      return false;

    return d > now || now.getDate() == dDay && now.getMonth() == dMonth && now.getFullYear() == dYear;
  }

  public existsDate(day: number, month: number, year: number) : boolean
  {
    var arr = this.occupiedDates;
    for (var i=0; i<arr.length; i++)
    {
      var d = arr[i].date.getDate();
      var m = arr[i].date.getMonth();
      var y = arr[i].date.getFullYear();

      if ( d==day && m==month && y==year )
        return true;
    };
    return false;
  }

  /**
   * Mangages button click event.
   * 
   * @param button Button number.
   */
  public btn(button: number): void
  {
    switch(button)
    {
      // Add new game of the day.
      case 1:
        this.action = "new";
        break;

      // Change current game.
      case 2:
        this.action = "change";
         break;

      // Delete game of the day.
      case 3:
        var x = confirm("Da li ste sigurni da zelite da obrisete ovu igru dana?");
        if ( x )
        {
          this.server.requestAuth("post", "admin/remove-game-of-the-day", {
            id: this.selectedGame._id
          }, resp => {
            if ( resp.deleted )
              alert("Uspešno ste obrisali igru dana.");
            else
              alert("Dogodila se greška: " + resp.error);
            
            // Clear and refresh!
            this.selected = false;
            this.action = null;
            this.resetFormFields();
            this.refreshGamesOfTheDay();                        
          }, err => {
            console.log("Could not remove game of the day.");
          });
        }
        break;

      // Submit current action.
      case 4:
        if ( !this.passCheckBeforeSend() )
          return;

        var obj: any;
        if ( this.action == "new" )
        {
          obj = {
            action: "add",
            date: this.childDate.value,
            anagram: this.selectedAnagram,
            fxf: this.selected5x5._id,
            trophie: this.selectedTrophie._id              
          };
        } else
        {          
          obj = {
            action: "change",
            id: this.selectedGame._id,
            anagram: this.selectedAnagram,
            fxf: this.selected5x5._id,
            trophie: this.selectedTrophie._id              
          };
        };
        this.isLoading = true;
        this.server.requestAuth("post", "admin/manage-game-of-the-day", obj, resp => {          
          this.isLoading = false;          
          if ( this.action != "change" )
          {            
            this.resetFormFields();            
          } else
          {
            if ( resp.inUse )
              alert("Nije moguće promeniti igru dana. Postoje takmičari koji je već igraju.");
            this.action = null;
          };

          // Refresh!
          this.refreshGamesOfTheDay();
        }, err => {

        });        
        break;

      // Decline current action.
      case 5:
        if ( this.selected )
        {
          this.action = null;
        } else
          this.resetFormFields();
        break;
    };
  }

  public selectionChange(event: MatSelectChange, comp: number): void
  {
    var id = event.value;
    var arr = comp == 3 ? this.gameTrophies : comp == 2 ? this.game5x5 : this.gameAnagrams;
    var item: any = null;
    switch(comp)
    {
      // Anagram change.
      case 1:
        if ( id == -1 )
        {
          this.selectedAnagram = null;
          return;
        };
        this.selectedAnagram = id;
        break;

      // 5x5 change.
      case 2:
        if ( id == -1 )
        {
          this.selected5x5 = null;
          return;
        }
        var words = [];
        for (var i=0; i<arr.length; i++)
        {
          if ( arr[i]._id == id )
          {
            item = arr[i];
            break;
          };          
        }
        for ( var i=0; i<5; i++ )
          words[i] = item["w" + (i+1)];

        this.selected5x5 = item;
        this.selected5x5Words = words;
        break;

      // Trophie change.
      case 3:

        if ( id == -1 )
        {
          this.selectedTrophie = null;
          return;
        };

        for (var i=0; i<arr.length; i++)
        {
          if ( arr[i]._id == id )
          {
            item = arr[i];
            break;
          };
        };

        this.selectedTrophie = item;

        break;
    }
  }

  getTitleForAnagram(anagramID: string): string
  {
    var arr = this.gameAnagrams;
    var item: any = null;
    
    for (var i=0; i<arr.length; i++)
    {
      if ( arr[i]._id == anagramID )
      {
        item = arr[i];
        break;
      }
    };

    if ( item == null )
      return "";

    return item.title + " => " + item.result;
  }

  public passCheckBeforeSend(): boolean
  {
    if ( this.childDate.value == undefined )
    {
      this.snackBar.open("Molimo unesite datum.", null, {
        duration: 3000
      });
    } else if ( !this.selectedAnagram )
    {
      this.snackBar.open("Molimo odaberite anagram.", null, {
        duration: 3000
      });
    } else if ( !this.selected5x5 )
    {
      this.snackBar.open("Molimo odaberite igru 5x5.", null, {
        duration: 3000
      });
    } else if ( !this.selectedTrophie )
    {
      this.snackBar.open("Molimo odaberite igru pehar.", null, {
        duration: 3000
      });
    } else
      return true;

    return false;
  }

  public resetFormFields(): void
  {
    this.childDate.value = "";
    this.childGame5x5.value = "";
    this.childGameAnagram.value = "";
    this.childGameTrophie.value = "";
    this.childAllGames.value = "";
    this.selectedGame = null;
    this.selectedAnagram = null;
    this.selected5x5 = null;
    this.selected5x5Words = null;
    this.selectedTrophie = null;
    this.action = null;
  }

  public changeDate(event: MatSelectChange): void
  { 
    var id = event.value;
    if ( id == -1 )
    {
      this.resetFormFields();
      this.selected = false;
      return;
    }

    this.selected = true;
    var game: any = null;
    var games: Array<any> = this.listOfGamesOfTheDay;
    for ( var i=0; i<games.length; i++ )
    {
      if ( games[i]._id == id)
      {
        game = games[i];
        break;
      }
    };

    if ( game == null )
      return;

    this.selectedGame = game;
    this.childDate.value = game.date;
    this.selectedAnagram = game.anagramID;
    this.childGameAnagram.value = game.anagramID;
    this.selected5x5 = this.getObjectFor( "5x5", game.fxfID );
    this.childGame5x5.value = game.fxfID;
    this.selectedTrophie = this.getObjectFor( "trophie", game.trophieID );
    this.childGameTrophie.value = game.trophieID;

    var words = [];
    for ( var i=0; i<5; i++ )
      words[i] = this.selected5x5["w" + (i+1)];
    this.selected5x5Words = words;
  }

  public getObjectFor(name: string, id: string): any
  {
    var arr: Array<any>;
    var item: any = null;
    switch( name )
    {
    case "anagram":
        arr = this.gameAnagrams;
        break;  

    case "trophie":
        arr = this.gameTrophies;
        break;

    case "5x5":
        arr = this.game5x5;
        break;
    };

    for (var i=0; i<arr.length; i++)
    {
      if ( arr[i]._id == id )
      {
        item = arr[i];
        break;
      };
    }

    return item;
  }
}
