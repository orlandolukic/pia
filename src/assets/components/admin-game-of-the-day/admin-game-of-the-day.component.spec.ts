import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGameOfTheDayComponent } from './admin-game-of-the-day.component';

describe('AdminGameOfTheDayComponent', () => {
  let component: AdminGameOfTheDayComponent;
  let fixture: ComponentFixture<AdminGameOfTheDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminGameOfTheDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGameOfTheDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
