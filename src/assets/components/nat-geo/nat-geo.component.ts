import { Component, OnInit, ViewChild, AfterViewInit, DoCheck } from "@angular/core";
import { MatTable, MatTableDataSource, MatInput, MatPaginator, MatDialog } from '@angular/material';
import { NatGeoElement, translateCategoryToNative } from './util';
import { SelectionModel } from '@angular/cdk/collections';
import { Server } from 'src/assets/code/http-request';
import { ModalAddNewWordComponent } from '../modals/modal-add-new-word/modal-add-new-word.component';

@Component({
    selector: 'nat-geo',
    templateUrl: "./nat-geo.component.html",
    styleUrls: ["./nat-geo.component.css"]
})
export class NatGeoComponent implements OnInit, AfterViewInit
{
    private loading: boolean;
    private empty: boolean;
    dataSource: MatTableDataSource<NatGeoElement>;
    selection: SelectionModel<NatGeoElement>;
    columns: Array<string> = [
       "select", "indexNo", "letter", "category", "word"
    ];

    @ViewChild("table", {static: false, read: MatTable}) childTable: MatTable<any>;
    @ViewChild("search", {static: true, read: MatInput}) childSearch: MatInput;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    constructor(
        private server: Server,
        private dialog: MatDialog
    )
    {
        this.selection = new SelectionModel<NatGeoElement>(true, []);
        this.loading = true;
        this.empty = false;
    }

    ngOnInit()
    {

    }

    ngAfterViewInit()
    {
        
    }

    public fetchWords(): void
    {
        this.server.requestAuth("post", "supervisor/check-nat-geo-words", {}, resp => {
            if ( resp.count > 0 )
            {
                this.empty = false;
                this.selection.clear();                
                this.childSearch.value = "";
                var arr: Array<NatGeoElement> = new Array<NatGeoElement>();
                var obj: NatGeoElement;
                var words = resp.words;
                for (var i=0; i<resp.count; i++)
                {
                    obj = {
                        id: words[i]._id,
                        no: i+1,
                        letter: words[i].letter,
                        category: translateCategoryToNative( words[i].category ),
                        word: words[i].word
                    };
                    arr.push(obj);
                };
                
                this.dataSource = new MatTableDataSource<NatGeoElement>(arr);
                this.dataSource.paginator = this.paginator;                
            } else
            {
                this.empty = true;
            }
            this.loading = false;
        }, err => {
            console.error("Could not search for nat-geo words.")
        });
    }

    public isAllSelected(): boolean {
        if ( this.dataSource )
        {
            const numSelected = this.selection.selected.length;
            const numRows = this.dataSource.data.length;
            return numSelected == numRows;
        };
        return false;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    public masterToggle() {
        if ( !this.dataSource )
            return;
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    public applyFilter(filterValue: string): void {
        if ( this.dataSource )
            this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    public add(): void
    {
        this.dialog.open(ModalAddNewWordComponent, {
            backdropClass: "backdrop",
            disableClose: true,
            closeOnNavigation: false,
            width: "450px"
        }).afterClosed().subscribe(() => {
            this.fetchWords();
        });
    }

    public remove(): void
    {
        var x = confirm("Dat li ste sigurni da zelite da obrisete pojmove?");
        if ( x )
        {
            
            this.loading = true;
            var delby: any = [];
            var selected: Array<NatGeoElement> = this.selection.selected;
            for (var i=0; i<selected.length; i++)
            {
                delby[i] = selected[i].id;
            };

            this.server.requestAuth("post", "supervisor/delete-nat-geo-words", { by: delby }, resp => {
                if ( resp.deleted )
                {
                    this.fetchWords();
                };
            }, err => {
                console.error("Could not delete nat-geo word(s)");
            });
        };
    }
}
