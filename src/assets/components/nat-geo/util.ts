
export interface NatGeoElement
{
    id: string;
    no: number;
    letter: string;
    category: string;
    word: string;
}

export function translateCategoryToNative(category: string): string
{
    switch(category)
    {
        case "state": return "Država";
        case "city": return "Grad";
        case "lake": return "Jezero";
        case "mountain": return "Planina";
        case "river": return "Reka";
        case "animal": return "Životinja";
        case "plant": return "Biljke";
        case "band": return "Muzički bend";
    };
    return null;
}