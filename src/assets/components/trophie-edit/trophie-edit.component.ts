import { Component, OnInit, Input, AfterViewInit, OnChanges, SimpleChanges } from "@angular/core";

@Component({
    selector: "trophie-edit",
    templateUrl: "./trophie-edit.component.html",
    styleUrls: ["./trophie-edit.component.css"]
})
export class TrophieEditComponent implements OnInit, AfterViewInit, OnChanges
{
    @Input("start")
    private shouldPreview: boolean;

    @Input("trophie")
    private trophie: any;

    private loading: boolean;
    private q: Array<string>;
    private a: Array<string>;

    constructor() {
        this.loading = true;
    }

    ngOnChanges(changes: SimpleChanges)
    {
        // Trophie has changed.
        if ( changes["trophie"] && changes["trophie"].currentValue )
        {
            this.q = new Array<string>();
            this.a = new Array<string>();

            for (var i=0; i<13; i++)
            {
                this.q[i] = this.trophie["q" + (i+1)];
                this.a[i] = this.trophie["a" + (i+1)];
            };

            this.loading = false;
        };
    }

    ngOnInit()
    {
        
    }

    ngAfterViewInit()
    {
        
    }
}