import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Supervise } from 'src/assets/code/supervisor/supervise';
import { Globals } from 'src/assets/code/globals';

@Component({
  selector: 'supervisor-game-request',
  templateUrl: './supervisor-game-request.component.html',
  styleUrls: ['./supervisor-game-request.component.css']
})
export class SupervisorGameRequestComponent implements OnInit {

  @Input()
  isMultiplayer: boolean;

  @Input()
  data: any;

  constructor(
    private router: Router,
    private supervise: Supervise,
    private globals: Globals
  ) { }

  ngOnInit() {

  }

  /**
   * Joins game.
   */
  join(): void
  {
    if ( !this.isMultiplayer )
    {
      var userData: any = this.globals.getUser().getData();
      var data: any = {
        username: userData.username,
        name: userData.name,
        surname: userData.surname
      };

      var serverData = {
        for: this.data.username
      };
      
      this.supervise.inSupervision = true;
      this.supervise.sendRegularRequest("supervisor-sp-signed-in", serverData);
      this.supervise.passDataToUser(this.data.username, "sp-supervisor-signed-in", data);      
      this.supervise.singleplayer.user.username = this.data.username;
      this.router.navigate(['/supervisor/supervise/singleplayer-game']);
    }
  }

}
