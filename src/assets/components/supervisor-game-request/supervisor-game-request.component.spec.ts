import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorGameRequestComponent } from './supervisor-game-request.component';

describe('SupervisorGameRequestComponent', () => {
  let component: SupervisorGameRequestComponent;
  let fixture: ComponentFixture<SupervisorGameRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorGameRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorGameRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
