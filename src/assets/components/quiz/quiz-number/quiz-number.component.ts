import { Component, OnInit, Input, EventEmitter, Output, ComponentRef, ChangeDetectorRef } from '@angular/core';
import { MatStepper, MatButton, MatSnackBar } from '@angular/material';
import { QuizNumberStack, QuizNumberStackElement } from './quiz-number-stack';
import { GameManagement } from 'src/assets/code/quiz/game-management';
import { QuizComponent } from 'src/assets/code/quiz/quiz-component-interface';

@Component({
  selector: 'quiz-number',
  templateUrl: './quiz-number.component.html',
  styleUrls: ['./quiz-number.component.css']
})
export class QuizNumberComponent implements OnInit, QuizComponent {

  @Input()
  stepper: MatStepper;

  timeout: boolean;
  stopPressed: boolean;
  loading: boolean;
  inProcess: boolean;
  canStop: boolean;
  spin: boolean;
  dig: Array<number>;
  oth: Array<number>;
  digButtons: Array<boolean>;
  othButtons: Array<boolean>;
  des: number;
  tho: number;
  wantedNumber: number;
  exp: string;
  result: string;
  evalResult: number;
  interval: any;
  stack: QuizNumberStack;
  management: GameManagement;
  snackBar: MatSnackBar;

  constructor(
     snackBar: MatSnackBar,
     management: GameManagement,
     private detectorRef: ChangeDetectorRef
  ) 
  {
    this.stopPressed = false;
    this.management = management;
    this.snackBar = snackBar;
    this.spin = true;
    this.timeout = false;
    this.canStop = true;
    this.loading = true;
    this.inProcess = false;
    this.exp = "";
    this.result = "greška";
    this.evalResult = -1;
    this.dig = new Array<number>();
    this.oth = new Array<number>();
    this.digButtons = new Array<boolean>();
    this.othButtons = new Array<boolean>();
    for (var i=0; i<4; i++)
      this.digButtons[i] = false;
    for (var i=0; i<2; i++)
      this.othButtons[i] = false;

    this.stack = new QuizNumberStack();
  }

  ngOnInit() 
  {
    this.management.getOnLoadObservable().subscribe(() => {
      this.management.setComponentForGame("number", this);
    });
  }

  stopSpinning(): void
  {
    clearInterval(this.interval);
    this.spin = false;
    this.canStop = false;
    this.stopPressed = true;
    var numbers: Array<number> = new Array<number>();
    var f: number = 0;
    for (var i=0; i<this.dig.length; i++)
      numbers[f++] = this.dig[i];
    for (var i=0; i<this.oth.length; i++)
      numbers[f++] = this.oth[i];
    this.management.getGameWrapper().submitData(false, "sp-number-stop-spinning", {
      numbers: numbers,
      wantedNumber: this.wantedNumber
    });
    // Send data to the server.
    // Start counting.
  }

  onFinished(): void {
    
  }

  onPreview(): void {
    this.management.getGameWrapper().submitData(false, "sp-number-game-info", null);
  }

  onTimeout(): void {
    this.timeout = true;
    this.gameFinished();
  }

  public fromServer(multiplayer: boolean, request: string, data: any): void
  {
    if ( !multiplayer )
    {
      switch(request)
      {
      case "init":
          this.loading = false;
          var status: number = data.status;
          if ( status == 0 )
          {         
            this.canStop = true;
            this.spin = true;   
            this.interval = setInterval(() => {
              this.intervalCallback();
            }, 50);
          } else
          {
            this.canStop = false;
            this.spin = false;
            var f: number = 0;            
      
            for (var i=0; i<4; i++)
              this.dig[i] = data.game.partialNumbers[f++];

            for (var i=0; i<2; i++)
              this.oth[i] = data.game.partialNumbers[f++];

            this.wantedNumber = data.game.wantedNumber;            
            this.management.getTimeManager().setTickingTimeoutValue(new Date(data.timeout));
            this.detectorRef.detectChanges();
          };
          break;

      case "stopSpinning":
          this.canStop = false;
          this.management.getTimeManager().setTickingPeriod(data.timeoutInterval);                    
          break;

      case "finish":
          this.management.getTimeManager().stopInterval();
          this.management.showAdditionOfPoints("blue", data.points).then(() => {
            this.management.nextGame();
            this.stepper.next();
          });
          break;
      };
    }
  }

  public gameFinished(): void
  {
    var data: any = {
      isValidResult: this.evaluateExp(),
      result: this.evalResult
    };
    this.management.getGameWrapper().submitData(false, "sp-number-finish-game", data);
  }

  private evaluate(): void
  {
    try {
      if ( this.exp == "" )
        throw new Error();
      var num = Math.floor( eval(this.exp) );
      this.evalResult = num;
      this.result = "" + num;
    } catch(e) {
      this.evalResult = -1;
      this.result = "greška";
    };
  }

  private evaluateExp(): boolean
  {
    if ( this.exp == "" )
        return false;
    try {
      parseInt( eval(this.exp) );
    } catch(e) {
      return false;
    };
    return true;
  }

  pressed( type: string, objName: string, index: number, button: MatButton ): void
  { 
    if ( type == "number" )
    {
      try {
        if ( this.stack.top().isButton )
        {
          this.snackBar.open("Nije moguće dva puta za redom dodati broj.", null, {
            duration: 3000
          });
          return;
        };
      } catch(e) {}
      this.exp += "" + this[objName][index];    
      button.disabled = true;
      this[objName + "Buttons"][index] = true;
      this.stack.push(this[objName][index], true, button, index, objName);
    } else
    {
      this.exp += objName;
      this.stack.push(objName, false, null, 0, null);
    };
    this.evaluate();
  }

  public isReady(): boolean
  {
    return true;
    var len: number = this.digButtons.length;
    var ok: boolean = true;
    for (var i=0; i<len; i++)
    {
      ok = ok && this.digButtons[i];
    };

    len = this.othButtons.length;
    for (var i=0; i<len; i++)
    {
      ok = ok && this.othButtons[i];
    };
    return ok && this.evaluateExp();
  }

  public backspace(): void
  {
    try {
      var el: QuizNumberStackElement = this.stack.pop();
      if ( el.isButton )
      {
        el.button.disabled = false;
        this[el.array + "Buttons"][el.index] = false;
      };   
      try {  
        this.exp = this.exp.replace( new RegExp(el.value + "$"), "" ); 
      } catch(f) {
        this.exp = this.exp.replace( new RegExp(/\W$/), "" ); 
      }
    } catch(e) {
      console.error(e);
    }
    this.evaluate();
  }

  public escape(): void
  {
    this.exp = "";
    try {
      while( true )
      {
        var el: QuizNumberStackElement = this.stack.pop();
        if ( el.isButton )
        {
          this[el.array + "Buttons"][el.index] = false;
          el.button.disabled = false;
        };
      };
    } catch(e) {}

    this.evaluate();
  }

  public continue(): void
  {
    this.inProcess = true;
    this.gameFinished();
  }

  private intervalCallback(): void
  {
    this.dig[0] = Math.floor( Math.random()*8 ) + 1;
    this.dig[1] = Math.floor( Math.random()*8 ) + 1;
    this.dig[2] = Math.floor( Math.random()*8 ) + 1;
    this.dig[3] = Math.floor( Math.random()*8 ) + 1;

    var x = Math.random();
    if ( x <= 0.33 )
      x = 10;
    else if ( x > 0.33 && x < 0.66 )
      x = 15;
    else if ( x >= 0.66 )
      x = 20;
    this.oth[0] = x;

    x = Math.random();
    if ( x <= 0.25 )
      x = 25;
    else if ( x > 0.25 && x < 0.5 )
      x = 50;
    else if ( x >= 0.50 && x < 0.75 )
      x = 75;
    else if ( x >= 0.75 )
      x = 100;
    this.oth[1] = x;

    this.wantedNumber = Math.ceil( Math.random()*998 ) + 1;
  }
}
