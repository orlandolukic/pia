import { MatButton } from '@angular/material';
import { throws } from 'assert';

/**
 * Stack for buttons.
 */
export class QuizNumberStack
{
    private elements: number;
    private index: number;
    private stack: Array<QuizNumberStackElement>;

    constructor() {
        this.empty();
    }

    public empty(): void
    {
        this.index = -1;
        this.elements = 0;
        this.stack = new Array<QuizNumberStackElement>();
    }

    public push( value: string, isButton: boolean, button: MatButton, index: number, array: string ): void
    {
        this.index++;
        this.stack[this.index] = {
            value: value,
            isButton: isButton,
            button: button,
            index: index,
            array: array
        };
        this.elements++;
    }

    public top(): QuizNumberStackElement
    {
        if ( this.elements == 0 )
            throw new Error();
        return this.stack[this.index];
    }

    public pop(): QuizNumberStackElement
    {
        if ( this.elements == 0 )
            throw new Error("Stack is empty.");
        var element: QuizNumberStackElement = this.stack[this.index];
        this.index--;
        this.elements--;
        if ( this.index == -1 )
        {
            this.stack = new Array<QuizNumberStackElement>();
        };
        return element;
    }

    public size(): number
    {
        return this.elements;
    }
}

export interface QuizNumberStackElement
{
    value: string;
    isButton: boolean;
    button: MatButton;
    index: number;
    array: string;
}