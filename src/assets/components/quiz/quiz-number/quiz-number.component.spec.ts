import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizNumberComponent } from './quiz-number.component';

describe('QuizNumberComponent', () => {
  let component: QuizNumberComponent;
  let fixture: ComponentFixture<QuizNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
