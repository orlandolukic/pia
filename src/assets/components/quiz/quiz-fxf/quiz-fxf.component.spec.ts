import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizFxfComponent } from './quiz-fxf.component';

describe('QuizFxfComponent', () => {
  let component: QuizFxfComponent;
  let fixture: ComponentFixture<QuizFxfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizFxfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizFxfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
