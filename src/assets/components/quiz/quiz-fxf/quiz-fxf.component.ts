import { Component, OnInit, Input, NgZone } from '@angular/core';
import { MatStepper, MatButton, MatSnackBar } from '@angular/material';
import { QuizComponent } from 'src/assets/code/quiz/quiz-component-interface';
import { GameManagement } from 'src/assets/code/quiz/game-management';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'quiz-fxf',
  templateUrl: './quiz-fxf.component.html',
  styleUrls: ['./quiz-fxf.component.css']
})
export class QuizFxfComponent implements OnInit, QuizComponent {

  @Input("stepper")
  stepper: MatStepper;

  @Input("formGroup")
  formGroup: FormGroup;

  private isTimeout: boolean;
  private inProcess: boolean;
  private letters: Array<string>;
  private matrix: Array<Array<string>>;
  private resolved: Array<Array<boolean>>;
  private taken: any;
  private pressed: Array<number>;       // pressed[i]: 0 - none | 1 - blue | 2 - red
  private show: boolean;
  private errors: number;
  private MAXERRORS: number;

  private genCharArray(charA: string, charZ: string): Array<string> {
      var a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0), v: string;
      for (; i <= j; ++i) 
      {
        v = String.fromCharCode(i);
        switch( v )
        {
        case "W": 
        case "X":
        case "Y":
          break;

        default:
            a.push(v);
            break;
        };          
      };
      return a;
  }

  private genTakenArray(charA: string, charZ: string): any {
    var a = {}, i = charA.charCodeAt(0), j = charZ.charCodeAt(0), v: string;
    for (; i <= j; ++i) 
    {
      v = String.fromCharCode(i);
      switch( v )
      {
      case "W": 
      case "X":
      case "Y":
        break;

      default:
          a[v] = false;
          break;
      };          
    };
    a['Ć'] = false;
    a['Š'] = false;
    a['Đ'] = false;
    a['Ž'] = false;
    a['Č'] = false;    
    return a;
}

  constructor(
    private management: GameManagement,
    private snackBar: MatSnackBar,
    private ngzone: NgZone
  ) {
    this.inProcess = false;
    this.isTimeout = false;
    this.letters = this.genCharArray('A', 'Z');
    this.letters.push("Ć","Č","Š","Ž","Đ");
    this.taken = this.genTakenArray('A','Z');
    this.matrix = new Array<Array<string>>();
    this.resolved = new Array<Array<boolean>>();
    this.errors = 0;
    this.MAXERRORS = 3;
    for (var i=0; i<5; i++)
    {
      this.matrix[i] = new Array<string>();
      this.resolved[i] = new Array<boolean>();
      for (var j=0; j<5; j++)
      {
        this.matrix[i][j] = "-";
        this.resolved[i][j] = false;
      };
    }
    this.show = false;
    this.pressed = new Array<number>();
    for (var i=0; i<this.letters.length; i++)
      this.pressed[i] = 0;   
  }

  ngOnInit() {
    this.management.getOnLoadObservable().subscribe(() => {
      this.management.setComponentForGame("fxf", this);
    });  
  }

  onFinished(): void {
    
  }

  onPreview(): void {
    this.management.getGameWrapper().submitData(false, "sp-fxf-game-info", null);
    this.show = true;
  }

  onTimeout(): void {
    this.isTimeout = true;
    // Get results from the database.
    this.management.getGameWrapper().submitData(false, "sp-fxf-release-all-table", {});
  }

  public continue(): void
  {
    this.gameFinished();
  }

  public letterPressed(index: number, letter: string, button: MatButton): void
  {
    this.pressed[index] = 1;
    button.disabled = true;
    this.inProcess = true;
    this.taken[letter] = true;
    var data: any = {
      letter: letter
    };
    this.management.getGameWrapper().submitData(false, "sp-fxf-letter-pressed", data);
  }

  public gameFinished(): void
  {
    this.management.getGameWrapper().submitData(false, "sp-fxf-finish-game", null);  
  }

  public fromServer(multiplayer: boolean, request: string, data: any): void
  {
    if ( !multiplayer )
    {
      switch(request)
      {
      case "sp-fxf-game-info":        
        var game: any = data.game;
        for (var i=0; i<game.checkedLetters.length; i++)
          this.taken[game.checkedLetters[i]] = true;

        var obj: any = data.game.checked;
        for (var i=0; i<obj.length; i++)
          this.matrix[obj[i].i][obj[i].j] = obj[i].letter;

        if ( data.timeout == null )
          this.management.getTimeManager().setTickingPeriod(data.timeoutInterval);
        else
          this.management.getTimeManager().setTickingTimeoutValue(new Date(data.timeout));
        break;

      case "sp-fxf-letter-pressed":        
        this.inProcess = false;
        if ( data.letterDoesNotExist )
        {
          this.snackBar.open("Slovo '" + data.letter + "' ne postoji u ovoj slagalici.", null, {
            duration: 5000
          });    
          this.errors++;
          if ( this.errors == this.MAXERRORS )      
          {
            this.isTimeout = true;
            this.management.getTimeManager().stopInterval();
            // Get results from the database.
            this.management.getGameWrapper().submitData(false, "sp-fxf-release-all-table", {});
          };
        } else
        {
          for (var i=0; i<data.positions.length; i++)
          {
            var pos: any = data.positions[i];
            this.matrix[pos.i][pos.j] = data.letter;
          };
          this.management.showAdditionOfPoints("blue", data.points);          
        };

        if ( data.isFinished )
        {
          this.management.getTimeManager().stopInterval();
          this.isTimeout = true;
        };
        break;

      case "sp-fxf-release-all-table":
        var obj: any = data.table;
        for (var i=0; i<5; i++)
        {
          for (var j=0; j<5; j++)
          {
            if ( this.matrix[i][j] == "-" )
            {
              this.resolved[i][j] = true;
              this.matrix[i][j] = obj[i][j];
            };
          };
        }
        break;

      case "sp-fxf-finish-game":
          this.management.nextGame();
          this.stepper.next();
          break;
      }
    }
  }

}
