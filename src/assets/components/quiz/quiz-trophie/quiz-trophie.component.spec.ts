import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizTrophieComponent } from './quiz-trophie.component';

describe('QuizTrophieComponent', () => {
  let component: QuizTrophieComponent;
  let fixture: ComponentFixture<QuizTrophieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizTrophieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizTrophieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
