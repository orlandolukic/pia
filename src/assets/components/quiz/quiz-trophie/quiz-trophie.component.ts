import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatStepper, MatInput, MatSnackBar } from '@angular/material';
import { QuizComponent } from 'src/assets/code/quiz/quiz-component-interface';
import { GameManagement } from 'src/assets/code/quiz/game-management';

@Component({
  selector: 'quiz-trophie',
  templateUrl: './quiz-trophie.component.html',
  styleUrls: ['./quiz-trophie.component.css']
})
export class QuizTrophieComponent implements OnInit, QuizComponent {

  @Input("stepper")
  stepper: MatStepper;

  @ViewChild("answer", {static: true, read: MatInput})
  answer: MatInput;

  mp: boolean;
  private loading: boolean;
  private question: string;
  private canFinish: boolean;
  private q: Array<string>;
  private a: Array<string>;
  private cls: Array<number>;
  private currentQuestion: number;
  private currentLength: number;
  private trend: number;
  private correct: Array<boolean>;
  private elements: any[] = [
    { letters: 9, arr: [] },
    { letters: 8, arr: [] },
    { letters: 7, arr: [] },
    { letters: 6, arr: [] },
    { letters: 5, arr: [] },
    { letters: 4, arr: [] },
    { letters: 3, arr: [] },
    { letters: 4, arr: [] },
    { letters: 5, arr: [] },
    { letters: 6, arr: [] },
    { letters: 7, arr: [] },
    { letters: 8, arr: [] },
    { letters: 9, arr: [] },
  ];

  constructor(
    private management: GameManagement,
    private snackBar: MatSnackBar
  ) { 
    this.q = new Array<string>();
    this.a = new Array<string>();
    this.cls = new Array<number>();
    this.currentQuestion = 0;
    this.currentLength = 9;
    this.trend = -1;
    this.mp = false;
    this.canFinish = false;
    this.loading = true;
    this.correct = new Array<boolean>();

    for (var i=0; i<13; i++)
    {
        this.q[i] = null;
        this.a[i] = null;
        this.cls[i] = -1;
        this.correct[i] = false;
    };

    var j:number;
    for(var i=0; i<this.elements.length; i++)
    {
      j = this.elements[i].letters;
      for (var k=0; k<j; k++)
        this.elements[i].arr[k] = k;
    }
  }

  ngOnInit() {
    this.management.getOnLoadObservable().subscribe(() => {
      this.management.setComponentForGame("trophie", this);
      this.mp = this.management.isMultiplayer();
    });
  }

  onFinished(): void {
     
  }

  onPreview(): void {
    this.management.getGameWrapper().submitData(false, "sp-trophie-game-info", null);
  }

  onTimeout(): void {
     this.submitAnswer(true);     
  }

  /**
   * Finish all games.
   */
  public finish(): void
  {
    this.management.nextGame();
    this.stepper.next();
  }

  public submitAnswer(force: boolean = false): void
  {
    // Stop time interval.
    this.management.getTimeManager().stopInterval();

    var regexp = new RegExp("^[a-zA-ZćčšžđĆŠČŽĐ]{" + (this.currentLength) + "}$");
    if ( !force && !regexp.test(this.answer.value) )
    {
      this.snackBar.open("Reč mora imati tačno " + this.currentLength + " karaktera.", null, {
        duration: 5000
      });
      this.answer.focus();
      return;
    };

    // Proceed with submission.
    this.loading = true;
    this.management.getGameWrapper().submitData(false, "sp-trophie-submit-answer", this.answer.value);
  }

  public fromServer(multiplayer: boolean, request: string, data: any): void
  {
    if ( !multiplayer )
    {
      switch(request)
      {
      case "sp-trophie-game-info":  
        // Check if game is finished.
        if ( data.game.finished )
        {
          this.canFinish = true;
          this.loading = true;
          return;
        };

        // Set timeout.
        if ( data.started )
          this.management.getTimeManager().setTickingTimeoutValue(new Date(data.timeout));
        else 
          this.management.getTimeManager().setTickingPeriod(data.timeoutInterval);

        var arr: string[] = data.game.questions;
        for ( var i=0; i<arr.length; i++ )
        {
          this.q[i] = arr[i];
        };

        arr = data.game.answers;
        for ( var i=0; i<arr.length; i++ )
        {
          this.a[i] = arr[i];
          this.correct[i] = data.game.correct[i];
        };
        this.currentQuestion = data.game.currentQuestion;
        this.question = this.q[this.currentQuestion];
        this.loading = false;
        this.currentLength = data.game.length[this.currentQuestion];
        this.trend = this.currentQuestion >= 6 ? 1 : -1;
        break;

      case "sp-trophie-submit-answer":
        this.cls[this.currentQuestion] = 1;
        this.a[this.currentQuestion] = data.correct ? this.answer.value : data.answer;
        this.correct[this.currentQuestion] = data.correct;
        this.currentQuestion++;

        if ( data.points > 0 )
          this.management.showAdditionOfPoints("blue", data.points);

        if ( this.currentQuestion == 13 )
        {
          this.canFinish = true;
          this.loading = true;
          this.answer.value = "";
          this.question = "...";
          this.management.canLeaveFlag = true;
          return;
        };

        if ( this.currentLength == 3 && this.trend < 0 )
        {
          this.trend = 1;
        };
        if ( this.trend > 0 )
          this.currentLength++;
        else
          this.currentLength--;

        // Load new question.
        this.question = this.q[this.currentQuestion];
        this.loading = false;
        this.answer.value = "";
        setTimeout(() => {
          this.answer.focus();
        }, 100);
        this.management.getTimeManager().setTickingPeriod(data.timeoutInterval);
        break;
      }
    };
  }

}
