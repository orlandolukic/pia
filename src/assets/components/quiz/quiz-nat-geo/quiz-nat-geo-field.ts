/**
 * Input fields in NatGeo game.
 */
export class NatGeoField
{
    approved: boolean;
    bySupervisor: boolean;
    category: string;
    answered: boolean;

    constructor(category: string) {
        this.approved = false;
        this.bySupervisor = false;
        this.category = category;
        this.answered = false;
    }
}