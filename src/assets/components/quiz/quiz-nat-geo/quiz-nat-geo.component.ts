import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatStepper, MatInput } from '@angular/material';
import { QuizComponent } from 'src/assets/code/quiz/quiz-component-interface';
import { GameManagement } from 'src/assets/code/quiz/game-management';
import { NatGeoField } from './quiz-nat-geo-field';

@Component({
  selector: 'quiz-nat-geo',
  templateUrl: './quiz-nat-geo.component.html',
  styleUrls: ['./quiz-nat-geo.component.css']
})
export class QuizNatGeoComponent implements OnInit, QuizComponent {

  @Input()
  stepper: MatStepper;

  @ViewChild("city", {static: true, read: MatInput}) 
  city: MatInput;
  
  @ViewChild("state", {static: true, read: MatInput}) 
  state: MatInput;

  @ViewChild("river", {static: true, read: MatInput}) 
  river: MatInput;

  @ViewChild("mountain", {static: true, read: MatInput}) 
  mountain: MatInput;

  @ViewChild("lake", {static: true, read: MatInput}) 
  lake: MatInput;

  @ViewChild("animal", {static: true, read: MatInput}) 
  animal: MatInput;

  @ViewChild("plant", {static: true, read: MatInput}) 
  plant: MatInput;

  @ViewChild("band", {static: true, read: MatInput}) 
  band: MatInput;

  private arr: Array<any> = [
    null, null, null, null, null, null, null, null    
  ];

  private loading: boolean;
  private inProcess: boolean;
  private letter: string;
  private status: number;
  private finished: boolean[];
  private fields: NatGeoField[];

  constructor(
    private management: GameManagement
  ) {
    this.inProcess = false;
    this.letter = "...";
    this.loading = true;
    this.finished = new Array<boolean>();
    this.status = 0;
    this.fields = new Array<NatGeoField>();

    for (var i=0; i<13; i++)
      this.finished[i] = false;
    
    this.fields.push( new NatGeoField("city") );
    this.fields.push( new NatGeoField("state") );
    this.fields.push( new NatGeoField("river") );
    this.fields.push( new NatGeoField("mountain") );
    this.fields.push( new NatGeoField("lake") );
    this.fields.push( new NatGeoField("animal") );
    this.fields.push( new NatGeoField("plant") );
    this.fields.push( new NatGeoField("band") );
  }

  ngOnInit() {
    this.management.getOnLoadObservable().subscribe(() => {
      this.management.setComponentForGame("nat-geo", this);
    });
  }

  public getField(name: string): NatGeoField
  {
    for (var i=0; i<this.fields.length; i++)
      if ( this.fields[i].category == name )
        return this.fields[i];

    return null;
  }

  onFinished(): void {
   
  }
  onPreview(): void {
    this.management.getGameWrapper().submitData(false, "sp-natgeo-game-info", null);
  }

  onTimeout(): void {
    this.proceed();  
  }

  public proceed(): void
  {
    this.management.getTimeManager().stopInterval();
    this.status++;

    switch(this.status)
    {
    case 0:
      this.management.getGameWrapper().submitData(false, "sp-natgeo-finish-game", {});
      break;

    case 1:
      this.inProcess = true;
      this.loading = true;
      var data = {
        city: this.city.value,
        state: this.state.value,
        river: this.river.value,
        mountain: this.mountain.value,
        lake: this.lake.value,
        animal: this.animal.value,
        plant: this.plant.value,
        band: this.band.value,        
      };
      this.management.getGameWrapper().submitData(false, "sp-natgeo-supervise-terms", data);
      break;
    };
  }

  public continue(): void
  {
    this.proceed();  
  }

  public fromServer(multiplayer: boolean, request: string, data: any): void
  {
    if ( !multiplayer )
    {
      switch(request)
      {
      case "sp-natgeo-game-info":
        this.letter = data.game.letter;
        this.city.value = data.game.city;
        this.state.value = data.game.state;
        this.river.value = data.game.river;
        this.mountain.value = data.game.mountain;
        this.lake.value = data.game.lake;
        this.animal.value = data.game.animal;
        this.plant.value = data.game.plant;
        this.band.value = data.game.band;
        this.loading = false;
        this.status = data.game.status;
        this.handleStatus(data);      
        this.handleSupervision(data.supervision);

        break;


      case "sp-natgeo-supervise-terms":
        if ( data.points > 0 )
          this.management.showAdditionOfPoints("blue", data.points);

        if ( data.approved.length > 0 )
        {
          var arr: any[] = data.approved;
          var field: NatGeoField;
          for (var i=0; i<arr.length; i++)
          {
            field = this.getField(arr[i].key);
            field.answered = true;
            field.approved = true;
            this.setFinishedField(arr[i].key, true);
          };

          this.setFinished(-1);
        };
        
        this.status = data.status;

        if ( data.status == 3 )
        {
          this.status = -1;
          this.inProcess = false;
          this.setFinished(-1);
        };
        break;

      case "ctrl-sp-supervisor-finished":
        var arr: any[] = data.terms;  
        this.management.showAdditionOfPoints("blue", data.points);
        this.handleSupervision(data.supervision);
        this.setFinished(-1);
        this.status = -1;
        this.inProcess = false;
        this.handleStatus(null);
        break;

      case "sp-natgeo-finish-game":
        this.management.nextGame();
        this.stepper.next();        
        break;
      }
    }
  }

  public setFinished(ind: number): void
  {
    if ( ind == -1 )
    {
      for (var i=0; i<13; i++)
        this.finished[i] = true;
    } else
    {
      this.finished[ind] = true;
    };
  }

  public setFinishedField(name: string, val: boolean): void
  {
    var a: string[] = [
      "city", "state", "river", "mountain", "lake", "animal", "plant", "band"
    ];
    for (var i=0; i<a.length; i++)
    {
      if ( a[i] == name )
      {
        this.finished[i] = val;
        break;
      };
    };
  }

  public handleSupervision(supervision: any): void
  {
    if ( supervision.finished )
    {
      this.setFinished(-1);
      for (var i=0; i<supervision.results.length; i++)
      {
        for (var j=0; j<this.fields.length; j++)
        {
          if ( this.fields[j].category == supervision.results[i].category )
          {
            this.fields[j].answered = true;
            this.fields[j].bySupervisor = true;
            this.fields[j].approved = supervision.results[i].checked;
            break;
          };
        };
      };

      for (var i=0; i<supervision.dbChecked.length; i++)
      {
        for (var j=0; j<this.fields.length; j++)
        {
          if ( this.fields[j].category == supervision.dbChecked[i].category )
          {
            this.fields[j].answered = true;
            this.fields[j].bySupervisor = false;
            this.fields[j].approved = true;
            break;
          };
        };
      };

    };
  }

  public handleStatus(data: any): void
  {
    switch( this.status )
    {
    case -1:
      this.loading = true;
      this.setFinished(-1);
      break;

    case 0:
        this.management.getTimeManager().setTickingTimeoutValue(new Date(data.timeout));
        break;
        
    case 1:
      this.inProcess = true;
      this.loading = true;
      break;
    }; 
  }

  public saveInputValue(name: string, i: number): void
  {
    clearTimeout(this.arr[i]);
    if ( this[name].value != "" )
    {      
      this.arr[i] = setTimeout(() => {
        this.management.getGameWrapper().submitData(false, "sp-natgeo-save-input", {
          field: name,
          value: this[name].value
        });
      }, 2000);
    };
  }

  public declineSaveInput(name: string, i: number): void
  {
    clearTimeout(this.arr[i]);
  }
}