import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizNatGeoComponent } from './quiz-nat-geo.component';

describe('QuizNatGeoComponent', () => {
  let component: QuizNatGeoComponent;
  let fixture: ComponentFixture<QuizNatGeoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizNatGeoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizNatGeoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
