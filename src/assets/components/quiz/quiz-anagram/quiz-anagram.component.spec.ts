import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizAnagramComponent } from './quiz-anagram.component';

describe('QuizAnagramComponent', () => {
  let component: QuizAnagramComponent;
  let fixture: ComponentFixture<QuizAnagramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizAnagramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizAnagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
