import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatStepper, MatInput, MatSnackBar } from '@angular/material';
import { GameManagement } from 'src/assets/code/quiz/game-management';
import { QuizComponent } from 'src/assets/code/quiz/quiz-component-interface';

@Component({
  selector: 'quiz-anagram',
  templateUrl: './quiz-anagram.component.html',
  styleUrls: ['./quiz-anagram.component.css']
})
export class QuizAnagramComponent implements OnInit, QuizComponent {

  private question: string;
  private inProcess: boolean;

  @Input()
  stepper: MatStepper;

  @ViewChild(MatInput, {static: true, read: MatInput}) 
  childResult: MatInput;

  constructor(
    private snackBar: MatSnackBar,
    private management: GameManagement
  ) {
    this.inProcess = false;
    this.question = "";
  }

  ngOnInit() {
    this.management.getOnLoadObservable().subscribe(() => {
      this.management.setComponentForGame("anagram", this);
    });
  }

  continue(): void
  {
    var f = new RegExp("^[ ]*$");
    if ( f.test(this.childResult.value) )
    {
      this.snackBar.open("Morate uneti rešenje anagrama.", null, {
        duration: 3000
      });
      this.childResult.value = "";
      this.childResult.focus();
    } else
    {
      // Do the send process.
      this.finished();          
    };
  }

  onFinished(): void {
    
  }

  onPreview(): void {
    this.management.getGameWrapper().submitData(false, "sp-anagram-game-info", null);
  }

  onTimeout(): void {
    this.finished();    
  }

  public finished(): void
  {
    this.inProcess = true;
    this.management.stopTimeTicking();

    var data: any = {
      answer: this.childResult.value
    };
    this.management.getGameWrapper().submitData(false, "sp-anagram-finished", data);
  }

  public fromServer(multiplayer: boolean, name: string, data: any): void
  {
    if ( !multiplayer )
    {
      switch(name)
      {
      case "init":
        this.management.getTimeManager().setTickingTimeoutValue( new Date(data.timeout) );
        this.question = data.game.question;
        break;

      case "finished":
          this.management.showAdditionOfPoints("blue", data.points).then(() => {
            this.management.nextGame();
            this.stepper.next();
          });
          break;
      }
    }
  }

}
