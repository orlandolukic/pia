import { Component, Inject, Injectable, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Server } from 'src/assets/code/http-request';
import { Globals } from 'src/assets/code/globals';
import { Router } from '@angular/router';
import { LoggedInGuard } from 'src/assets/code/guards';
import { Subscription } from 'rxjs';
import { MultiplayerGame } from 'src/assets/code/multiplayer-game';
import { WaitingModalData } from 'src/assets/code/util/connection-util';
import { SingleplayerGame } from 'src/assets/code/user/singleplayer-request';
import { SupervisorData } from 'src/assets/code/util/supervisor-data';

@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: './modal-wait-for-user.html',
    styleUrls: ['./modal-wait-user.css']
})
export class ModalWaitForUser implements OnDestroy {

    private loadingRed: boolean;
    private loadingDots: string;
    private loadingSupervisor: boolean;
    private redName: string;
    private supervisorName: string;
    private supervisor: SupervisorData;

    private n: number;
    private timeoutLoading: any;
    private ready: boolean;

    private abs: Subscription;
  
    constructor(
      public dialogRef: MatDialogRef<ModalWaitForUser>,
      @Inject(MAT_DIALOG_DATA) private data: WaitingModalData,
      private server: Server,
      private globals: Globals,
      private router: Router,
      private multiplayer: MultiplayerGame,
      private singleplayer: SingleplayerGame
    ) 
      {
        this.supervisor = null;

          // Append callback.
          this.dialogRef.afterOpened().subscribe(() => {
            clearInterval(this.timeoutLoading);
            this.timeoutLoading = setInterval(() => {
                if ( this.n == 2 )
                {
                    this.n = 0;
                    this.loadingDots = ".";
                } else
                {
                    this.n++;
                    this.loadingDots += ".";
                }
            }, 800);

            // Append multiplayer listeners.
            if ( this.data.isMultiplayer )
            {
              this.multiplayer.getRegisterEventsObservable().subscribe(() => {
                this.multiplayer.addEventListener("ready-to-go", (data) => { this.waitForChange(data); });
                this.multiplayer.addEventListener("not-ready-to-go", (data) => { this.notReady(data); });
              });              
            } else
            {
              this.singleplayer.addEventListeners(() => {             
                this.singleplayer.addEventListener("ctrl-sp-supervisor-signed-in", (data: any) => { this.waitForChange(data); });
                this.singleplayer.addEventListener("ctrl-get-data-about-user", (resp: any) => {                                    
                  this.singleplayer.supervisor.setUsername(resp.user.username);
                  this.singleplayer.supervisor.setName(resp.user.name);
                  this.singleplayer.supervisor.setSurname(resp.user.surname);                  
                  this.loadingSupervisor = false;
                  this.ready = true;
                });
              });              
             
              this.singleplayer.sendRegularRequest("get-data-about-user", this.data.supervisor);              
            };
          });

          this.ready = false;
          this.loadingRed = true;
          this.loadingDots = ".";
          this.loadingSupervisor = true;
          this.n = 0;
      }
  
    /**
     * Starts game. Registers match in the database.
     */
    public startGame(): void 
    {
      if ( this.data.isMultiplayer )
      {
        this.multiplayer.setActive(true);
        this.ready = false;
        this.server.requestAuth("post", "user/start-quiz", {
          redID: this.multiplayer.getRedUsername(),
          supID: this.multiplayer.getSupervisorUsername()
        }, resp => {
          if ( resp.ok )
          {
            this.dialogRef.close();           
            this.globals.inGame = 1; 
            this.router.navigate(['/user/etf-quiz']);
          } else
            alert("Error occured while starting quiz.");
        }, err => {});
      } else
      {
        this.dialogRef.close();        
        this.singleplayer.sendRegularRequest("begin-game", {supervisor: this.singleplayer.supervisor.getUsername()});
        this.globals.inGame = 0;
        this.router.navigate(['/user/etf-quiz']);
      };
    }

    ngOnDestroy()
    {
      if ( this.data.isMultiplayer )
      {
        this.multiplayer.removeEventListener("ready-to-go", null);
        this.multiplayer.removeEventListener("not-ready-to-go", null);             
      } else
      {
        this.singleplayer.removeEventListener("ctrl-sp-supervisor-signed-in", null);   
        this.singleplayer.removeEventListener("ctrl-get-data-about-user", null);              
      }
    }

    /**
     * Declines current game.
     */
    public declineGame(): void
    {
      var x = confirm("Da li ste sigurni da zelite da ponistite igru?");
      if ( x )
      {
        this.dialogRef.close();
        if ( this.data.isMultiplayer )
        {
          this.multiplayer.removeEventListener("ready-to-go", null);
          this.multiplayer.removeEventListener("not-ready-to-go", null);
          this.multiplayer.sendEvent("decline-game-for-host", this.globals.getUser().getData().username );
          this.multiplayer.terminateGame(false);
          this.multiplayer.socketDisconnect();
        } else
        {          
          this.singleplayer.sendRegularRequest("decline-singleplayer-game-for", null);
          this.singleplayer.socketDisconnect();
        }
      }
    }

    /**
     * Callback for change on server.
     */
    public waitForChange(data: any): void
    {
      if ( this.data.isMultiplayer )
      {
        if ( data.red )
        {
            var d = data.redData;
            this.multiplayer.setRedName(d.name);
            this.multiplayer.setRedSurname(d.surname);   
            this.multiplayer.setRedUsername(d.username); 

            this.redName = this.multiplayer.getRedWholeName();                
            this.loadingRed = false;

            if ( !this.loadingSupervisor )
              this.ready = true;                   
        };

        if ( data.supervisor )
        {
            var d = data.supervisorData;
            this.multiplayer.setSupervisorName(d.name);
            this.multiplayer.setSupervisorSurname(d.surname); 
            this.multiplayer.setSupervisorUsername(d.username);

            this.supervisorName = this.multiplayer.getSupervisorWholeName(); 
            this.loadingSupervisor = false;

            if ( !this.loadingRed )
                this.ready = true; 
        };
      } else
      {
        var supervisor: SupervisorData = this.singleplayer.supervisor;
        supervisor.setUsername(data.username);
        supervisor.setName(data.name);
        supervisor.setSurname(data.surname);        
        
        this.loadingSupervisor = false;
        this.ready = true;
      }
      /*
      this.multiplayer.setRedName(data.user.name);
      this.multiplayer.setRedSurname(data.user.surname);   
      this.multiplayer.setRedUsername(data.user.username);            
      this.redName = this.multiplayer.getRedWholeName();                
      this.loadingRed = false;

      if ( !this.loadingSupervisor )
          this.ready = true;
      */
    }

    public notReady(data: any): void
    {
      if ( data.red )
      {
          this.multiplayer.setRedName(null);
          this.multiplayer.setRedSurname(null);   
          this.multiplayer.setRedUsername(null); 

          this.loadingRed = true;
          this.redName = null;        
      };

      if ( data.supervisor )
      {
          this.multiplayer.setSupervisorName(null);
          this.multiplayer.setSupervisorSurname(null); 
          this.multiplayer.setSupervisorUsername(null);

          this.loadingSupervisor = true;
          this.supervisorName = null;
      };

      this.ready = false;                   
    }
  
  }