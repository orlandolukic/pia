import { Component, OnInit } from "@angular/core";
import { MatDialogRef, MatSnackBar, MatSelect, MatOption, MatInput } from '@angular/material';
import { Server } from 'src/assets/code/http-request';

@Component({
    selector: "modal-add-new-word",
    templateUrl: "./modal-add-new-word.component.html",
    styleUrls: ["./modal-add-new-word.component.css"]
})
export class ModalAddNewWordComponent implements OnInit
{
    private adding: boolean;
    private letters: Array<string> = [
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "V", "Z", "Č", "Š", "Đ", "Ć", "Ž"
    ];

    private categories: Array<any> = [
        { native: "Država", database: "state" },
        { native: "Grad", database: "city" },
        { native: "Jezero", database: "lake" },
        { native: "Planina", database: "mountain" },
        { native: "Reka", database: "river" },
        { native: "Životinja", database: "animal" },
        { native: "Biljka", database: "plant" },
        { native: "Muzički bend", database: "band" },
    ];

    constructor(
        private dialogRef: MatDialogRef<ModalAddNewWordComponent>,
        private snackBar: MatSnackBar,
        private server: Server
    )
    {
        this.adding = false;
    }

    ngOnInit()
    {

    }

    public add(category: MatSelect, term: MatInput): void
    {
        var c: MatOption = <MatOption>category.selected;

        if ( c == undefined )
        {
            this.snackBar.open("Molimo Vas odaberite kategoriju.", null, { duration: 3000 });
            category.focus();
        } else if ( term.value.length == 0 )
        {
            this.snackBar.open("Molimo Vas unseite pojam.", null, { duration: 3000 });
            term.focus();
        } else
        {
            var letter = term.value[0].toUpperCase();
            var regexp = new RegExp("^(" + letter + "|" + letter.toLowerCase() + ")");
            if ( !regexp.test(term.value) )
            {
                this.snackBar.open("Pojam mora počinjati slovom '" + letter + "'.", null, { duration: 3000 });
                term.focus();
            } else
            {
                this.adding = true;                
                this.server.requestAuth("post", "supervisor/add-new-term", {
                    letter: letter,
                    category: c.value,
                    term: term.value
                }, resp => {
                    this.adding = false;
                    if ( resp.exists )
                    {
                        this.snackBar.open("Unešeni pojam već postoji u sistemu.", null, { duration: 3000 });
                    } else if ( resp.added )
                    {
                        this.snackBar.open("Uspešno ste uneli novi pojam", null, { duration: 3000 });                        
                        category.value = "";
                        term.value = "";
                        category.focus();                        
                    };
                }, err => {
                    console.error("Could not add new word in database.");
                });
            };
        }       
    }

    public close(): void
    {
        this.dialogRef.close();
    }
}