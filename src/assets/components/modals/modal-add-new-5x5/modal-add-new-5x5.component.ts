import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MatInput, MatDialogRef } from '@angular/material';
import { Server } from 'src/assets/code/http-request';

@Component({
    selector: "modal-add-new-5x5",
    templateUrl: "./modal-add-new-5x5.component.html",
    styleUrls: ["./modal-add-new-5x5.component.css"]
})
export class ModalAddNew5x5Component implements OnInit, AfterViewInit
{
    loaded: boolean;
    private notif: any;
    private loading: boolean;

    @ViewChild("w1", {static: false, read: MatInput}) w1: MatInput;
    @ViewChild("w2", {static: false, read: MatInput}) w2: MatInput;
    @ViewChild("w3", {static: false, read: MatInput}) w3: MatInput;
    @ViewChild("w4", {static: false, read: MatInput}) w4: MatInput;
    @ViewChild("w5", {static: false, read: MatInput}) w5: MatInput;

    constructor( 
        private dialogRef: MatDialogRef<ModalAddNew5x5Component>,
        private server: Server
    ) 
    {
        this.loaded = false;
        this.loading = false;
    }

    ngOnInit()
    {

    }

    ngAfterViewInit()
    {
        this.loaded = true;
    }

    update(): void
    {
        console.log();
    }

    public add(): void
    {
        if ( this.notif )
            clearTimeout(this.notif.timeout);

        var obj = {
            class: "success-card",
            message: "Uspešno ste dodali novu igru.",
            timeoutFunction: () => {
                this.notif = null;
            },
            timeoutInterval: 5000,
            timeout: null
        };

        var regexp = new RegExp("^[a-zA-Z]{5}$");

        if ( !regexp.test(this.w1.value) )
        {
            obj.class = "error-card";
            obj.message = "Prva rec se mora sastojati tacno od 5 slova.";
            obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval);
            this.notif = obj;
            this.w1.focus();
        } else if ( !regexp.test(this.w2.value) ) 
        {
            obj.class = "error-card";
            obj.message = "Druga rec se mora sastojati tacno od 5 slova.";
            obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval);
            this.notif = obj;
            this.w2.focus();
        } else if ( !regexp.test(this.w3.value) )
        {
            obj.class = "error-card";
            obj.message = "Treca rec se mora sastojati tacno od 5 slova.";
            obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval);
            this.notif = obj;
            this.w3.focus();
        } else if ( !regexp.test(this.w4.value) )
        {
            obj.class = "error-card";
            obj.message = "Cetvrta rec se mora sastojati tacno od 5 slova.";
            obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval);
            this.notif = obj;
            this.w4.focus();
        } else if ( !regexp.test(this.w5.value) )
        {
            obj.class = "error-card";
            obj.message = "Peta rec se mora sastojati tacno od 5 slova.";
            obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval);
            this.notif = obj;
            this.w5.focus();
        } else 
        {            
            this.notif = null;
            this.loading = true;
            var doc: any = {
                game: {
                    w1: this.w1.value,
                    w2: this.w2.value,
                    w3: this.w3.value,
                    w4: this.w4.value,
                    w5: this.w5.value
                }
            };

            // Send request to the database.
            this.server.requestAuth("post", "supervisor/add-5x5-game", doc, resp => {
                this.loading = false;
                obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval);
                this.notif = obj;
                this.w1.value = "";
                this.w2.value = "";
                this.w3.value = "";
                this.w4.value = "";
                this.w5.value = "";
                setTimeout(() => { this.w1.focus(); }, 100);
            }, err => {
                console.error("Could not add 5x5 game.");
            });
        }
    }

    public close(): void
    {
        if ( this.notif )
            clearTimeout(this.notif.timeout);

        this.dialogRef.close();
    }

    
}