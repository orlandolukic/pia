import { Component, OnInit, ViewChild } from "@angular/core";
import { MatDialogRef, MatInput } from '@angular/material';
import { Server } from 'src/assets/code/http-request';

@Component({
    selector: "modal-add-new-anagram",
    templateUrl: "./modal-add-new-anagram.html",
    styleUrls: ["./modal-add-new-anagram.css"]
})
export class ModalAddNewAnagramsComponent implements OnInit
{
    private notif: any;
    private loading: boolean;
    private added: boolean;
    private addedCnt: number;
    @ViewChild("anagramTitle", {static: false}) fieldAnagramTitle: MatInput;
    @ViewChild("anagramResult", {static: false}) fieldAnagramResult: MatInput;
    
    constructor( public dialogRef: MatDialogRef<ModalAddNewAnagramsComponent>, private server: Server ) {
        this.notif = null;
        this.loading = false;
        this.added = false;
        this.addedCnt = 0;
    }

    ngOnInit(): void {
        
    }

    public closeModal(): void
    {
        if ( this.notif )
            clearTimeout(this.notif.timeout);
        this.dialogRef.close(this.addedCnt);
    }

    public addNewAnagram(): void
    {
        if ( this.notif )
            clearTimeout(this.notif.timeout);
        var obj = {
            class: "success-card",
            message: "Uspešno ste dodali nov anagram.",
            timeoutFunction: () => {
                this.notif = null;
            },
            timeoutInterval: 5000,
            timeout: null
        };
        this.loading = true;

        if ( this.fieldAnagramTitle.value.length == 0 || this.fieldAnagramResult.value.length == 0 )
        {
            obj.class = "error-card";
            obj.message = "Postoje prazna polja.";
            obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval);
            this.notif = obj;
            this.loading = false;
            this.fieldAnagramTitle.focus();
            return;
        };

        this.server.requestAuth("post", "supervisor/add-new-anagram", {
            title: this.fieldAnagramTitle.value,
            result: this.fieldAnagramResult.value
        }, resp => {
            this.loading = false;                        
            if ( resp.exists )
            {
                obj.class = "error-card";
                obj.message = "Anagram već postoji u bazi.";                                
            } else
                this.addedCnt++;
            this.fieldAnagramTitle.value = "";
            this.fieldAnagramResult.value = "";
            this.fieldAnagramTitle.focus();
            obj.timeout = setTimeout(obj.timeoutFunction, obj.timeoutInterval)
            this.notif = obj;
        }, err => {
            this.loading = false;
            alert("Dogodila se greska prilikom upisa novog anagrama. Molimo pokusajte kasnije.");
            window.location.reload();
        });
    }
    
}