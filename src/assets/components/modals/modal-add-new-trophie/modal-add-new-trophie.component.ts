import { Component, OnInit, ViewEncapsulation, Input, ViewChild } from "@angular/core";
import { MatDialogRef, MatSnackBar, MatInput } from '@angular/material';
import { Server } from 'src/assets/code/http-request';

@Component({
    selector: "modal-add-new-trophie",
    templateUrl: "./modal-add-new-trophie.component.html",
    styleUrls: ["./modal-add-new-trophie.component.css"],
    encapsulation: ViewEncapsulation.None
})
export class ModalAddNewTrophieComponent implements OnInit
{
    private loading: boolean;
    private sections: Array<TrophieData>;
    private validator: any;
    private isValidName: boolean;

    @ViewChild("trophieName", {static: true, read: MatInput}) childTrophieName: MatInput;


    constructor( 
        private dialogRef: MatDialogRef<ModalAddNewTrophieComponent>,
        private snackBar: MatSnackBar,
        private server: Server
    )
    {
        this.loading = false;
        this.isValidName = true;

        this.sections = [
            {
                title: "Pitanje I)",
                question: "Pitanje I)",
                answer: "Odgovor I)",
                questionPlaceholder: "Unesite prvo pitanje",
                answerPlaceholder: "Unesite odgovor na prvo pitanje",
                hint: "9 slova",
                maxlength: 9,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje II)",
                question: "Pitanje II)",
                answer: "Odgovor II)",
                questionPlaceholder: "Unesite drugo pitanje",
                answerPlaceholder: "Unesite odgovor na drugo pitanje",
                hint: "8 slova",
                maxlength: 8,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje III)",
                question: "Pitanje III)",
                answer: "Odgovor III)",
                questionPlaceholder: "Unesite trece pitanje",
                answerPlaceholder: "Unesite odgovor na trece pitanje",
                hint: "7 slova",
                maxlength: 7,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje IV)",
                question: "Pitanje IV)",
                answer: "Odgovor IV)",
                questionPlaceholder: "Unesite cetvrto pitanje",
                answerPlaceholder: "Unesite odgovor na cetvrto pitanje",
                hint: "6 slova",
                maxlength: 6,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje V)",
                question: "Pitanje V)",
                answer: "Odgovor V)",
                questionPlaceholder: "Unesite peto pitanje",
                answerPlaceholder: "Unesite odgovor na peto pitanje",
                hint: "5 slova",
                maxlength: 5,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje VI)",
                question: "Pitanje VI)",
                answer: "Odgovor VI)",
                questionPlaceholder: "Unesite sesto pitanje",
                answerPlaceholder: "Unesite odgovor na sesto pitanje",
                hint: "4 slova",
                maxlength: 4,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje VII)",
                question: "Pitanje VII)",
                answer: "Odgovor VII)",
                questionPlaceholder: "Unesite sedmo pitanje",
                answerPlaceholder: "Unesite odgovor na sedmo pitanje",
                hint: "3 slova",
                maxlength: 3,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje VIII)",
                question: "Pitanje VIII)",
                answer: "Odgovor VIII)",
                questionPlaceholder: "Unesite osmo pitanje",
                answerPlaceholder: "Unesite odgovor na osmo pitanje",
                hint: "4 slova",
                maxlength: 4,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje IX)",
                question: "Pitanje IX)",
                answer: "Odgovor IX)",
                questionPlaceholder: "Unesite deveto pitanje",
                answerPlaceholder: "Unesite odgovor na deveto pitanje",
                hint: "5 slova",
                maxlength: 5,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje X)",
                question: "Pitanje X)",
                answer: "Odgovor X)",
                questionPlaceholder: "Unesite deseto pitanje",
                answerPlaceholder: "Unesite odgovor na deseto pitanje",
                hint: "6 slova",
                maxlength: 6,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje XI)",
                question: "Pitanje XI)",
                answer: "Odgovor XI)",
                questionPlaceholder: "Unesite jedanaesto pitanje",
                answerPlaceholder: "Unesite odgovor na jedanaesto pitanje",
                hint: "7 slova",
                maxlength: 7,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje XII)",
                question: "Pitanje XII)",
                answer: "Odgovor XII)",
                questionPlaceholder: "Unesite dvanaesto pitanje",
                answerPlaceholder: "Unesite odgovor na dvanaesto pitanje",
                hint: "8 slova",
                maxlength: 8,
                isValidAnswer: true,
                isValidQuestion: true
            },
            {
                title: "Pitanje XIII)",
                question: "Pitanje XIII)",
                answer: "Odgovor XIII)",
                questionPlaceholder: "Unesite trinaesto pitanje",
                answerPlaceholder: "Unesite odgovor na trinaesto pitanje",
                hint: "9 slova",
                maxlength: 9,
                isValidAnswer: true,
                isValidQuestion: true
            }
        ];

        this.validator = {

            isValidQuestion: (section: any, value: any): boolean => {
                var regexp = new RegExp("^[a-zA-ZćĆčČšŠđĐžŽ]{2}[a-zA-ZćĆčČšŠđĐžŽ ]*\\?*");
                var f = section.isValidQuestion = regexp.test(value);
                return f;
            },

            isValidAnswer: (section: any, value: any): boolean =>
            {
                var regexp = new RegExp("^[a-zA-ZćĆčČšŠđĐžŽ]{" + section.maxlength + "}$");
                if ( !regexp.test(value) )
                {
                    section.isValidAnswer = false;
                    return false;
                } else
                {
                    section.isValidAnswer = true;
                    return true;
                };
            },

            isValidTitle: (value: any): boolean =>
            {
                var regexp = new RegExp("^[a-zA-ZćĆčČšŠđĐžŽ]{2}");
                var f = this.isValidName = regexp.test(value);
                return f;
            }

        };
    }

    ngOnInit()
    {

    }

    public close(): void
    {
        this.dialogRef.close();
    }

    public add(): void
    {
        this.loading = true;
        var okQ = true;
        var okA = true;

        // Check questions.
        for (var i=0; i<this.sections.length; i++)
        {
            var inputQ: any = document.querySelectorAll("input[data-id*='input-question-" + i + "']")[0];            
            var inputA: any = document.querySelectorAll("input[data-id*='input-answer-" + i + "']")[0]; 
            okQ = this.validator.isValidQuestion(this.sections[i], inputQ.value) && okQ;
            okA = this.validator.isValidAnswer(this.sections[i], inputA.value) && okA;
        };

        if ( !okQ )
        {
            this.snackBar.open("Dogodile su se greske oko pitanja. Molimo da ih ispravite.", null, {
                duration: 3000
            });
            this.loading = false;
        } else if ( !okA )
        {
            this.snackBar.open("Dogodile su se greske oko odgovora na pitanja. Molimo da ih ispravite.", null, {
                duration: 3000
            });
            this.loading = false;
        } else
        {
            // Check if name already exists in the database.
            var name: string = this.childTrophieName.value;
            name = name.charAt(0).toUpperCase() + name.slice(1);
            this.server.requestAuth("post", "supervisor/check-if-trophie-exists", { name: name }, resp => {
                if ( !resp.found )
                {
                    // Send request to the database.            
                    var doc: any = {};
                    var inputQ: any, inputA: any;
                    for (var i=0; i<this.sections.length; i++)
                    {
                        inputQ = document.querySelector("input[data-id*='input-question-" + i + "']");            
                        inputA = document.querySelector("input[data-id*='input-answer-" + i + "']"); 
                        doc["q" + (i+1)] = inputQ.value;
                        doc["a" + (i+1)] = inputA.value;
                    };
                    doc.name = name;
                    this.server.requestAuth("post", "supervisor/add-new-trophie-game", doc, resp => {
                        if ( resp.added )
                        {
                            this.snackBar.open("Uspešno ste dodali igru pehar!", null, {
                                duration: 3000
                            });
                            this.dialogRef.close();                            
                        }
                    }, err => {
                        console.error("Could not add new trophie game.");
                    });
                } else
                {
                    this.childTrophieName.focus();
                    this.snackBar.open("Naziv pehara vec postoji u bazi.", null, {
                        duration: 3000
                    });
                    this.loading = false;
                };
            }, err => {
                console.error("Could not check if trophie exists in the database.");
            });
        }
    }
}

export interface TrophieData
{
    title: string;
    question: string;
    answer: string;
    questionPlaceholder: string;
    answerPlaceholder: string;
    hint: string;
    maxlength: number;
    isValidAnswer: boolean;
    isValidQuestion: boolean;
}