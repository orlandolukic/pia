import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpSectionSuperviseComponent } from './sp-section-supervise.component';

describe('SpSectionSuperviseComponent', () => {
  let component: SpSectionSuperviseComponent;
  let fixture: ComponentFixture<SpSectionSuperviseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpSectionSuperviseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpSectionSuperviseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
