import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { Supervise } from 'src/assets/code/supervisor/supervise';
import { Router } from '@angular/router';
import { MatSnackBar, MatInput, MatButton } from '@angular/material';
import { Globals } from 'src/assets/code/globals';
import { SuperviseUser } from 'src/assets/code/supervisor/supervise-user';
import { SuperviseElement } from 'src/assets/code/supervisor/supervise-element';

@Component({
  selector: 'app-sp-section-supervise',
  templateUrl: './sp-section-supervise.component.html',
  styleUrls: ['./sp-section-supervise.component.css']
})
export class SpSectionSuperviseComponent implements OnInit, AfterViewInit, OnDestroy {

  private loaded: boolean;
  private playing: any;
  private waiting: boolean;
  private loadedBlue: boolean;
  private isSupervision: boolean;
  private elements: Array<SuperviseElement>;
  private submitted: boolean;
  private finished: boolean;
  private canLeave: boolean;

  @ViewChild("city", {static: false, read: MatInput}) 
  city: MatInput;
  
  @ViewChild("state", {static: false, read: MatInput}) 
  state: MatInput;

  @ViewChild("river", {static: false, read: MatInput}) 
  river: MatInput;

  @ViewChild("mountain", {static: false, read: MatInput}) 
  mountain: MatInput;

  @ViewChild("lake", {static: false, read: MatInput}) 
  lake: MatInput;

  @ViewChild("animal", {static: false, read: MatInput}) 
  animal: MatInput;

  @ViewChild("plant", {static: false, read: MatInput}) 
  plant: MatInput;

  @ViewChild("band", {static: false, read: MatInput}) 
  band: MatInput;

  @ViewChild("f1a", {static: false, read: MatButton}) f1a: MatButton;
  @ViewChild("f1d", {static: false, read: MatButton}) f1d: MatButton;
  @ViewChild("f2a", {static: false, read: MatButton}) f2a: MatButton;
  @ViewChild("f2d", {static: false, read: MatButton}) f2d: MatButton;
  @ViewChild("f3a", {static: false, read: MatButton}) f3a: MatButton;
  @ViewChild("f3d", {static: false, read: MatButton}) f3d: MatButton;
  @ViewChild("f4a", {static: false, read: MatButton}) f4a: MatButton;
  @ViewChild("f4d", {static: false, read: MatButton}) f4d: MatButton;
  @ViewChild("f5a", {static: false, read: MatButton}) f5a: MatButton;
  @ViewChild("f5d", {static: false, read: MatButton}) f5d: MatButton;
  @ViewChild("f6a", {static: false, read: MatButton}) f6a: MatButton;
  @ViewChild("f6d", {static: false, read: MatButton}) f6d: MatButton;
  @ViewChild("f7a", {static: false, read: MatButton}) f7a: MatButton;
  @ViewChild("f7d", {static: false, read: MatButton}) f7d: MatButton;
  @ViewChild("f8a", {static: false, read: MatButton}) f8a: MatButton;
  @ViewChild("f8d", {static: false, read: MatButton}) f8d: MatButton;


  constructor(
    private supervise: Supervise,
    private router: Router,
    private snackBar: MatSnackBar,
    private detectorRef: ChangeDetectorRef,
    private globals: Globals,    
    private ngZone: NgZone
  ) 
  {
    this.detectorRef.reattach();
    this.globals.renameDocumentTitle("Supervizija pojmova");
    this.waiting = true;
    this.loadedBlue = false;
    this.submitted = false;
    this.isSupervision = false;
    this.loaded = false;
    this.finished = false;
    this.canLeave = false;
    this.playing = {
      dots: ".",
      interval: null,
      n: 0
    }

    this.elements = new Array<SuperviseElement>();


    // Prepare connection with sever!
    this.supervise.prepare();

    setTimeout(() => {
      // Append event listeners!
      this.supervise.addEventListeners(() => { this.appendEventListeners(); });

      // Page reloaded!
      if ( !this.supervise.inSupervision )
      {
        this.supervise.sendRegularRequest("supervisor-get-data-about-supervision", null);
        this.supervise.addEventListener("ctrl-supervisor-get-data-about-supervision", (data: any) => {
          if ( data.singleplayer.active )
          {
            this.supervise.sendEvent("supervisor-sp-get-data-about-user", data.singleplayer.user);
          };        
        });
        this.supervise.inSupervision = true;
      } else
      {
        this.supervise.sendEvent("supervisor-sp-get-data-about-user", this.supervise.singleplayer.user.username);
      };
    }, 500);
  }

  ngOnInit() {
    this.playing.interval = setInterval(() => {
      if ( this.playing.n == 2 )
      {
        this.playing.dots = ".";
        this.playing.n = 0;
      } else
      {
        this.playing.dots += ".";
        this.playing.n++;
      };
    }, 1000);
  }

  ngAfterViewInit(): void {
    // Ask what is going on with the game?!
    this.supervise.sendRegularRequest("supervisor-get-game-status", null);    
  }

  ngOnDestroy()
  {
    clearInterval(this.playing.interval);
    this.supervise.removeEventListener("ctrl-sp-begin-game", null);
    this.supervise.removeEventListener("ctrl-sp-data-about-user", null);
    this.supervise.removeEventListener("ctrl-sp-user-declined", null);
    this.supervise.cleanUp();
    this.detectorRef.detach();
  }

  public appendEventListeners(): void
  {
    this.supervise.addEventListener("ctrl-sp-begin-game", (data: any) => { this.startGame(); });
    this.supervise.addEventListener("ctrl-sp-user-declined", (data: any) => { this.declineGame(); });
    this.supervise.addEventListener("ctrl-sp-data-about-user", (data: any) => { this.prepareGame(data); });
    this.supervise.addEventListener("ctrl-get-game-status", (data: any) => { this.determineGame(data); });
    this.supervise.addEventListener("ctrl-game-change", (data: any) => { this.gameChanges(data); });
    this.supervise.addEventListener("ctrl-sp-terms-for-supervision", (data: any) => { this.startSupervision(data); })
    this.supervise.addEventListener("ctrl-supervisor-sp-finished", (data: any) => { this.endSupervision(data); });
    this.supervise.addEventListener("ctrl-supervisor-sp-game-terminated", (data: any) => { this.termination(data); });
  }

  public leave(): void
  {
    this.router.navigate(['/supervisor/dashboard']);
  }

  public termination(data: any): void
  {
    this.supervise.inSupervision = false;
    this.canLeave = true;
  }

  public startGame(): void
  {
    setTimeout(() => {
      this.supervise.game.setCurrentGame(0);
      this.waiting = false;
    }, 5000); 
    this.loadedBlue = true;   
  }

  public startSupervision(data: any): void
  {
    if ( data.length == 0 )
    {
      this.snackBar.open("Trenutno nema pojmova za proveru.", null, {
        duration: 3000
      });
      this.finished = true;
    } else
    {
      this.isSupervision = true;
      this.setSupervisionTerms(data);      
    }
  }

  public endSupervision(data: any): void
  {
    this.finished = true;
  }

  public isDisabledField(index: number): boolean
  {
    if ( this.elements.length == 0 )
      return true;
    else
      return this.elements[index].isDisabled();
  }

  public isDisabledAcceptButton(index: number): boolean
  {
    if ( this.elements.length == 0 )
      return true;
    else
      return this.elements[index].isDisabledAcceptButton();
  }

  public isDisabledDeclineButton(index: number): boolean
  {
    if ( this.elements.length == 0 )
      return true;
    else
      return this.elements[index-1].isDisabledDeclineButton();
  }

  public disAccept(num: number): boolean
  {
    if ( this.elements.length == 0 )
      return true;
    else
      return this.elements[num-1].isDisabledAcceptButton();
  }  

  public disDecline(num: number): boolean
  {
    if ( this.elements.length == 0 )
      return true;
    else
      return this.elements[num-1].isDisabledDeclineButton();
  }

  public isForSupervision(num: number): boolean
  {
    if ( this.elements.length == 0 )
      return true;
    else
      return this.elements[num-1].forSupervision;
  }

  public declineGame(): void
  {
    this.snackBar.open("Takmičar je odustao od partije.", null, {
      duration: 3000
    });
    this.supervise.inSupervision = false;
    this.router.navigate(['/supervisor/dashboard']);
  }

  public prepareGame(data: any): void
  {
    this.ngZone.run(() => {
      for( var key in data )
      {
        this.supervise.singleplayer.user[key] = data[key];  
      };
      this.loaded = true;
      this.detectorRef.reattach();
      this.detectorRef.detectChanges();
      this.detectorRef.markForCheck();
      this.init();
    });
  }

  private init(): void
  {
    var names: Array<string> = [
      "state", "city", "river", "mountain", "lake", "animal", "plant", "band"
    ];
    for (var i=0; i<8; i++)
    {
      this.elements[i] = new SuperviseElement();
      this.elements[i].setName(names[i]);
      this.elements[i].setButtons(this["f"+(i+1)+"a"], this["f"+(i+1)+"d"]);
      this.elements[i].setInput(this[names[i]]);
    };
  }

  public decide(i: number, type: boolean): void
  {
    this.elements[i-1].accepted = type;
  }

  public getUser(num?: number): SuperviseUser
  {
    if ( this.supervise.isMultiplayer )
    {

    } else
    {
      return this.supervise.singleplayer.user;
    };
  }

  /**
   * Determines game status.
   * 
   * @param data 
   */
  public determineGame(data: any): void
  {
    this.supervise.game.setCurrentGame(data.currentGame);
    if ( this.supervise.isMultiplayer )
    {

    } else
    {
      this.loadedBlue = data.started;
      this.waiting = !this.loadedBlue;
      this.isSupervision = data.supervision.inSupervision;  
      this.setSupervisionTerms(data.supervision.superviseTerms);  
      this.finished = data.supervision.finished;             
    };
  }

  public setSupervisionTerms(arr: {key:string, value:any}[]): void
  {
    this.detectorRef.reattach();
    if ( !this.detectorRef['destroyed'] )
      this.detectorRef.detectChanges(); 
    for( var i=0; i<arr.length; i++ )
    {
      for (var j=0; j<this.elements.length; j++)
      {
        if ( this.elements[j].name == arr[i].key )
        {
          this.elements[j].forSupervision = true;
          this.elements[j].accepted = false;
          this.elements[j].input.value = arr[i].value;
          break;
        };
      };
    };
    if ( !this.detectorRef['destroyed'] )
      this.detectorRef.detectChanges(); 
  }

  /**
   * Fires when game is changed.
   * 
   * @param data 
   */
  public gameChanges(data: any): void
  {
    this.supervise.game.setCurrentGame(data.gameIndex);
  }

  public submitData(): void
  {
    var x = confirm("Da li ste sigurni da potvrđujete odabir?");
    if ( x )
    {
      this.submitted = true;    
      this.finished = true;
      var subData: any[] = new Array<any>();
      var m: number = 0;
      for (var i=0; i<this.elements.length; i++)
      {
        if ( this.elements[i].forSupervision ) 
          subData[m++] = {
            category: this.elements[i].name,
            value: this.elements[i].input.value,
            checked: this.elements[i].accepted
          };
      };
      this.supervise.sendRegularRequest("supervisor-sp-finish", subData);
    }
  }

}
