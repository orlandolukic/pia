/**
 * Multiplayer request for playing quiz.
 */
import { Directive, ViewContainerRef, Component, OnDestroy, OnInit, Input, ViewChild, ComponentFactoryResolver, Injectable, Type } from '@angular/core';
import { MultiplayerRequests } from 'src/assets/code/user/multiplayer-requests';
import { Server } from 'src/assets/code/http-request';
import { Router } from '@angular/router';
import { MultiplayerGame } from 'src/assets/code/multiplayer-game';
import { Globals } from 'src/assets/code/globals';

@Directive({
  selector: '[ad-host]',
})
export class AdDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}

@Component({
    selector: "multiplayer-user-request",
    templateUrl: './multiplayer-user-request.html',
    styleUrls: ['./multiplayer-user-request.css'],    
})
export class MultiplayerUserRequestComponent implements MultiplayerComponent
{
  private isJoining: boolean;
  private isDisabled: boolean;
  @Input() data: any;
  @Input() multiplayerRequests: MultiplayerRequests;
  @Input() index: number;
  @Input() server: Server;

  constructor(
    private router: Router,
    private multiplayer: MultiplayerGame,
    private globals: Globals
  )
  {
    this.isJoining = false;
    this.isDisabled = false;
  }

  public joinMatch() : void
  {
    this.isJoining = true;
    this.multiplayerRequests.joining = true;
    this.multiplayerRequests.deactivateExcept(this.index);

    // Sign up red contestant.
    //this.multiplayer.sendEvent("hello", this.globals.getUser().getData().username );
    this.multiplayer.sendEvent("sign-in-red-contestant", { blue: this.data.username });
    this.router.navigate(['/user/etf-quiz']);

    /*
    this.server.requestAuth("post", "user/join-match", { username: this.data.username }, response => {
      
    }, err => {
      window.location.reload();
    });
    */
  }

  public deactivateComponent() : void
  {
    this.isDisabled = true;
  }
}

/**
 * Interface: MultiplayerComponent
 */
export interface MultiplayerComponent
{
  data: any;
  multiplayerRequests: MultiplayerRequests;
  index: number;
  server: Server;
  deactivateComponent() : void;
}