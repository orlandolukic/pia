import { Routes } from '@angular/router';
import { LoginComponent } from './app/login/login.component';
import { PageNotFoundComponent } from './app/page-not-found/page-not-found.component';
import { MenuUnregisteredComponent } from './app/menu-unregistered/menu-unregistered.component';
import { ForgotPasswordComponent } from './app/forgot-password/forgot-password.component';
import { MenuRegisteredComponent } from './app/menu-registered/menu-registered.component';
import { RegisterComponent } from './app/register/register.component';
import { ResultsComponent } from './app/results/results.component';
import { ResetPasswordComponent } from './app/reset-password/reset-password.component';
import { ForgotPasswordGuard, LoggedInGuard, QuizGuard, QuizGuardActivation, SupervisorGuard } from './assets/code/guards';
import { UserDashboardComponent } from './app/user-dashboard/user-dashboard.component';
import { LogoutComponent } from './app/logout/logout.component';
import { QuizComponent } from './app/quiz/quiz.component';
import { SupervisorDashboardComponent } from './app/supervisor-dashboard/supervisor-dashboard.component';
import { AdminDashboardComponent } from './app/admin-dashboard/admin-dashboard.component';
import { SpSectionSuperviseComponent } from './assets/components/supervisor/sp-section-supervise/sp-section-supervise.component';
import { SuperviseActivateGuard, SuperviseDeactivateGuard } from './assets/code/guards-supervisor';

export var routes : Routes = [
    // Main page outlet.
    { 
        path: "", 
        component: LoginComponent,
        canActivate: [ LoggedInGuard ] 
    },
    { 
        path: "forgot-password", 
        component: ForgotPasswordComponent, 
        canDeactivate: [ForgotPasswordGuard],
        canActivate: [ LoggedInGuard ] 
    },
    { 
        path: 'register', 
        component: RegisterComponent,
        canActivate: [ LoggedInGuard ] 
    },
    { 
        path: 'results', 
        component: ResultsComponent,
        canActivate: [ LoggedInGuard ]
    },
    { 
        path: 'reset-password', 
        component: 
        ResetPasswordComponent,
        canActivate: [ LoggedInGuard ]
    },
    {
        path: 'logout',
        component: LogoutComponent,
        canActivate: [ LoggedInGuard ]
    },
    { path: 'user', children: [
        { 
            path: 'dashboard', 
            component: UserDashboardComponent,
            canActivate: [ LoggedInGuard ] 
        },

        {
            path: "etf-quiz",
            component: QuizComponent,
            canActivate: [ QuizGuardActivation ],
            canDeactivate: [ QuizGuard ]
        },

        { path: "**", component: PageNotFoundComponent }
    ] },

    {
        path: "supervisor", 
        children: [
            {
                path: "dashboard",
                component: SupervisorDashboardComponent,
                canActivate: [ LoggedInGuard ]
            },

            {
                path: "supervise",
                children: [
                    {
                        path: "singleplayer-game",
                        component: SpSectionSuperviseComponent,
                        canActivate: [ SuperviseActivateGuard ],
                        canDeactivate: [ SuperviseDeactivateGuard ]
                    }
                ]
            },

            { path: "**", component: PageNotFoundComponent }
        ]
    },

    {
        path: "admin",
        children: [
            {
                path: "dashboard",
                component: AdminDashboardComponent,
                canActivate: [ LoggedInGuard ],                
            }
        ]
    },

    { path: "**", component: PageNotFoundComponent }
];